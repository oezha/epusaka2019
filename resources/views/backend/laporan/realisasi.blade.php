<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your custom  HTML goes here -->
<div class='panel panel-default'>
    <div class='panel-heading'>
      <div class='panel-title pull-left'>Realisasi</div>
      <div class='panel-title pull-right'>
      
      <a href="{{ route('import-to-transaksi') }}" class='btn btn-info btn-sm'><i class='fa fa-spinner'></i> Refresh</a>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class='panel-body'> 

    <div class="progress progress-striped active">             
    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">             
    <span class="sr-only">100%</span>             
    </div>
    </div>
        <table class='table table-striped table-bordered'>
            <thead>
                <tr>
                    <th>Kode</th>
                    <th>Uraian</th>
                    <th>Vol</th>
                    <th>Sat</th>
                    <th>Harga Satuan</th>
                    <th>Pagu</th>
                    <th>Realisasi Pengajuan</th>
                    <th>Realisasi SPM</th>
                    <th>Realisasi SP2D</th>
                </tr>
            </thead>
            <tbody>
            @foreach($row as $rows)
            <tr>
                <td>{{ $rows->kode }}</td>
                <td>{{ $rows->uraian }}</td>
                <td class='text-center'>{{ $rows->vol }}</td>
                <td class='text-center'>{{ $rows->sat }}</td>
                <td class='text-right'>{{ number_format($rows->hargasat , 0 ,  "," , ".") }}</td>
                <td class='text-right'>{{ number_format($rows->jumlah , 0 ,  "," , ".") }}</td>
                <td class='text-right'>{{ number_format($rows->realisasi_1 , 0 ,  "," , ".") }}</td>
                <td class='text-right'>{{ number_format($rows->realisasi_2 , 0 ,  "," , ".") }}</td>
                <td class='text-right'>{{ number_format($rows->realisasi_3 , 0 ,  "," , ".") }}</td>
                
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="progress">
    <div class="bar"></div >
    <div class="percent">0%</div >
</div>
<!-- ADD A PAGINATION -->
<script type="text/javascript">
 
     
    (function() {
 
    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status');
 
    $('form').ajaxForm({
        
        beforeSend: function() {
            status.empty();
            var percentVal = '0%';
            var posterValue = $('input[name=file]').fieldValue();
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        success: function() {
            var percentVal = 'Wait, Saving';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        complete: function(xhr) {
            status.html(xhr.responseText);
            alert('Uploaded Successfully');
            window.location.href = "/file-upload";
        }
    });
     
    })();
</script>
@endsection