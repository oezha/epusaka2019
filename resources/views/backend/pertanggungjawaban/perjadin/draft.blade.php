<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel panel-heading'><i class="fa fa-file-text-o"></i> Pertanggungjawaban Perjadin</div>
    <div class='panel panel-body'>      
        
        <div class='row'>
            <div class='col-xs-12'>
                <div class='nav-tabs-custom'>
                    <ul class="nav nav-tabs pull-right">
                        <li><a href="#tab_2-2" data-toggle="tab">Nominatif</a></li>
                        <li class="active"><a href="#tab_3-1" data-toggle="tab">Perjadin</a></li>
                        <li class="pull-left header"></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_3-1">
                        @if($privilage == 'user')
                        <form action="{{ route('send-pj-perjadin' , $id) }}" method="post" class='form-horizontal' id='form'>
                        @endif
                        @if($privilage == 'Bendahara')
                        <form action="{{ route('sendbend-pj-perjadin' , $id) }}" method="post" class='form-horizontal' id='form'>
                        @endif
                        {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="no_pengajuan2" class="col-sm-2 control-label">No. AJU</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="no_pengajuan2"
                                                    value="{{ $row->no_pengajuan }}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="posisi_dokumen" class="col-sm-2 control-label">Posisi Dok.</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="posisi_dokumen"
                                                    value="{{ $status->posisi_dokumen}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="keterangan" class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="keterangan"
                                                    value="{{ $status->keterangan}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama_bagian" class="col-sm-2 control-label">Bagian</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="nama_bagian"
                                                    value="{{$bagian->nama}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="no_mak" class="col-sm-2 control-label">No. Mak</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="no_mak"
                                                    value="{{$row->no_mak}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama_kegiatan" class="col-sm-3 control-label">Kegiatan</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" id="nama_kegiatan" rows="3" readonly>{{$row->nama_kegiatan}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tgl_awal" class="col-sm-3 control-label">Tgl. Kegiatan</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="tgl_awal"
                                                    value="{{ date('d M', strtotime($row->tgl_awal)) }} s.d {{ date('d M Y', strtotime($row->tgl_akhir)) }}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="title" class="col-sm-3 control-label">Daerah</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="title"
                                                    value="{{$daerah->title}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="total_realisasi" class="col-sm-3 control-label">Jumlah Pengajuan</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control text-right" id="total_realisasi"
                                                    value="{{ number_format($row->total_pengajuan , 0 , ',' , '.') }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- START DETAIL TABLE -->
                                <div class='row'>
                                
                                <div class="panel panel-default">
                                <div class="panel panel-heading"><i class="fa fa-file-text-o"></i> Detail Akun
                                <div class='panel-title pull-right'>
                                <a href="javascript:void(0);" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#form-tambah-akun"><i class="fa fa-plus"></i> Tambah Akun</a>
                                </div>


                                <div class="clearfix"></div>
                                </div>
                                    <div class="panel-body">
                                        <table class='table table-bordered' id="tableAkun">
                                            <thead>
                                                <th class='text-center'><strong>Akun</strong></th>
                                                <th class='text-center'><strong>Uraian</strong></th>
                                                <th class='text-center'><strong>Jumlah</strong></th>
                                                <th class='text-center'><strong>Pertanggungjawaban</strong></th>
                                                <th class='text-center'><strong>Pengembalian</strong></th>
                                            </thead>
                                            <tbody>
                                                @foreach($detail as $details)
                                                <tr>
                                                    <td>{{ $details->akun }}</td>
                                                    <td>{{ $details->uraian }}</td>
                                                    <td class='text-right'>{{ number_format($details->jumlah_pengajuan , 0 , ',' , '.') }}</td>
                                                    <td>
                                                    <input type="hidden" id='detail_id[{{$details->id}}]' name='detail_id[{{$details->id}}]' value='{{ $details->id }}'>
                                                    <input type="hidden" id='pagu[{{$details->id}}]' name='pagu[{{$details->id}}]' value='{{ $details->jumlah }}'>
                                                    <input type="hidden" nama='nilai_pengajuan[{{$details->id}}]' id='nilai_pengajuan[{{$details->id}}]' value='{{$details->jumlah_pengajuan}}'>
                                                    <input type='text' title="nilai" required    maxlength=15 class='form-control text-right nilai' name="pjnilai[{{$details->id}}]" id="pjnilai[{{$details->id}}]" 
                                                    value="
                                                    <?php
                                                    
                                                    if($row->status_id == 7 || $row->status_id == 8 || $row->status_id == 9 || $row->status_id == 10 || $row->status_id == 11)
                                                    {
                                                        echo number_format($details->jumlah_pertanggungjawaban , 0 , ',' , '.');
                                                    }
                                                    else
                                                    {
                                                        echo number_format($details->jumlah_pengajuan , 0 , ',' , '.');
                                                    }
                                                    
                                                    ?>"
                                                    
                                                    onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'
                                                    <?php
                                                    // if($row->metode_bayar_id == 1)
                                                    // {
                                                    //     echo 'readonly';
                                                    // }
                                                    ?>
                                                    <?php
                                                    if($row->status_id == 8 || $row->status_id == 9 || $row->status_id == 10)
                                                    {
                                                        echo 'readonly';
                                                    }
                                                    ?>
                                                    />
                                                    </td>
                                                    <td>
                                                    <input type='text' title="nilai" required    maxlength=15 class='form-control text-right sisa' name="kembali[{{$details->id}}]" id="kembali[{{$details->id}}]" value='{{ number_format(
                                                        
                                                        $rows->sisa_pagu 
                                                        , 0 , "," , ".") }}' onFocus = 'this.select()' readonly/>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                <tr>
                                                    <td colspan=2 class='text-right'><strong>TOTAL</strong></td>
                                                    <td class='text-right'>{{ number_format($row->total_pengajuan , 0 , ',' , '.') }}</td>
                                                    <td class='text-right'>
                                                    <input type='text' title="nilai" required    maxlength=15 class='form-control text-right' name="total_nilai_pj" id="total_nilai_pj"  readonly/>
                                                    </td>
                                                    <td class='text-right'>
                                                    <input type='text' title="nilai" required    maxlength=15 class='form-control text-right' name="total_nilai_kembali" id="total_nilai_kembali"  readonly/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                    
                                </div>
                                <!-- END DETAIL TABLE -->    

                                 @if(CRUDBooster::myPrivilegeName() == 'Bendahara')
                                
                                    <input type="hidden" name='_token' value='{{ csrf_token() }}'>
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            
                                        </div>
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <label for="status_id" class="col-sm-2 control-label">Status</label>
                                                <div class="col-sm-10">
                                                    <select name="status_id" id="status_id" class='form-control' require>
                                                    @foreach($status_bend as $key => $stat)
                                                    <option value="{{ $stat->id }}"
                                                    <?php if($stat->id == $row->status_id ) { echo "selected" ;} ?>
                                                    >{{ $stat->keterangan }}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class='form-group'>
                                                <label for="alasan" class="col-sm-2 control-label">Alasan</label>
                                                <div class="col-sm-10">
                                                <textarea class="form-control" rows="5" id="alasan" name='alasan'
                                                @if(empty($row->alasan))
                                                readonly
                                                @endif
                                                ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif                          
                        </div>
                        <div class="tab-pane" id="tab_2-2">
                        <table class='table table-striped table-bordered'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>NIP</th>
                                    <th>Instansi / Jabatan</th>
                                    <th>Golongan</th>
                                    <th>Lama (Hari)</th>
                                    <th>Pesawat</th>
                                    <th>Taksi Provinsi</th>
                                    <th>Taksi Kabupaten</th>
                                    <th>Uang Harian</th>
                                    <th>Penginapan</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(Count($result))
                                @foreach($result as $key => $row)
                                @php
                                    $totalpesawat   = $totalpesawat + $row->tiket_pesawat;
                                    $totaltaksiprov = $totaltaksiprov + $row->taksi_provinsi;
                                    $totaltaksikab  = $totaltaksikab + $row->taksi_kabupaten;
                                    $totaluh        = $totaluh + $row->uang_harian;
                                    $totalinap      = $totalinap + $row->penginapan;
                                    $total          = $totalinap + $totalpesawat + $totaltaksikab + $totaltaksiprov + $totaluh;
                                @endphp
                                <tr>
                                <td>{{$key + 1}}</td>
                                    <td>{{$row->nama_peserta}}</td>
                                    <td>{{$row->nip}}</td>
                                    <td>{{$row->instansi}}</td>
                                    <td>{{$row->golongan}}</td>
                                    <td>{{$row->lama}}</td>
                                    <td class='text-right'>{{ number_format($row->tiket_pesawat , 0 , ',' , '.' ) }}</td>
                                    <td class='text-right'>{{ number_format($row->taksi_provinsi , 0 , ',' , '.' ) }}</td>
                                    <td class='text-right'>{{ number_format($row->taksi_kabupaten , 0 , ',' , '.' ) }}</td>
                                    <td class='text-right'>{{ number_format($row->uang_harian , 0 , ',' , '.' ) }}</td>
                                    <td class='text-right'>{{ number_format($row->penginapan , 0 , ',' , '.' ) }}</td>
                                    <td class='text-right'>
                                        <label for="total">{{ number_format($row->tiket_pesawat + $row->taksi_provinsi + $row->taksi_kabupaten + $row->uang_harian + $row->penginapan , 0 , ',' , '.' ) }}</label>
                                    </td>
                                    
                                </tr>
                                @endforeach
                                <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class='text-right'><strong>{{ number_format($totalpesawat , 0 , ',' , '.')}}</strong></td>
                                <td class='text-right'><strong>{{ number_format($totaltaksiprov , 0 , ',' , '.')}}</strong></td>
                                <td class='text-right'><strong>{{ number_format($totaltaksikab , 0 , ',' , '.')}}</strong></td>
                                <td class='text-right'><strong>{{ number_format($totaluh , 0 , ',' , '.')}}</strong></td>
                                <td class='text-right'><strong>{{ number_format($totalinap , 0 , ',' , '.')}}</strong></td>
                                <td class='text-right'><strong>{{ number_format($total , 0 , ',' , '.')}}</strong></td>
                                
                                </tr>
                            @else
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                   
                                </tr>
                            @endif
                            
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        


         <div class="text-center">
         <a href="{{ url()->previous() }}" class='btn btn-default float-left'><i class='fa fa-arrow-left'></i> Back</a>
            
            @if($status->id == 5 || $status->id == 7 || $status->id == 11)
            <button type='submit' value='Kirim' class='btn btn-success float-right'>
                <i class='fa fa-send'> Kirim</i>
            </button>
            @endif
            @if(CRUDBooster::myPrivilegeName() == 'Bendahara')
            @if($status->id == 8 || $status->id == 9)
            <button type='submit' value='Kirim' class='btn btn-success float-right'>
                <i class='fa fa-send'> Kirim</i>
            </button>
            @endif
            @endif
         </div>
         </form>
                
            
         
        <!-- etc .... -->
        </div
      
    </div>
  </div>
  <div id="form-tambah-akun" class="modal fade">
  <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" action="{{ route('import-excel-keg' ,  $id) }}" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah Akun</h4>
                </div>
                <div class="modal-body">
                    <div class='form-group header-group-0 ' id='form-group-hotel_id' style="">
                    <label class='control-label col-sm-2'>Hotel
                                    <span class='text-danger' title='This field is required'>*</span>
                            </label>

                    <div class="col-sm-10">
                        <select style='width:100%' class='form-control' id="hotel_id"
                                required    name="hotel_id"  >
                                
                        </select>
                        <div class="text-danger">
                        
                        </div><!--end-text-danger-->
                        <p class='help-block'></p>
                    </div>
                </div>
                    {{ csrf_field() }}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batal </button>
                    <button type="submit" class="btn bg-light-blue btn-sm"><i class="fa fa-plus"></i> Upload</button>
                </div>
            </form>
        </div>
    </div>
  </div>
@endsection