<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tanda Terima Pertanggungjawaban</title>
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


</head>
<body>
    
    <div class="w3-row w3-tiny">
        <div class="col-md-12" style="text-align: center;">
            <u>TANDA TERIMA PENYERAHAN PERTANGGUNGJAWABAN</u>
        </div>
        <br>
        <div class="row" style="text-align: right; margin-right: 200px;">
            <div class="col-md-6">
                TA : {{ CRUDBooster::myYear() }}
            </div>
            <div class="col-md-6">
                Nomor Berkas : {{ $no_aju }}
            </div>
        </div>
    </div>
    
    <div class="w3-row w3-tiny">
        <div class="col-md-12">
            <table class="table" style="padding: 10px !important; margin: 10px !important;">
                <tr>
                    <td>Sudah Terima Dari</td>
                    <td>:</td>
                    <td>{{ $bagian_name }}</td>
                </tr>
                <tr>
                    <td>Kegiatan</td>
                    <td>:</td>
                    <td>{{ $nama_keg }}</td>
                </tr>
                <tr>
                    <td>Tanggal</td>
                    <td>:</td>
                    <td>{{date('d M', strtotime($tanggal_awal))}} s.d {{date('d M y', strtotime($tanggal_akhir))}}</td>
                </tr>
                <tr>
                    <td>Lokasi Kegiatan</td>
                    <td>:</td>
                    <td>{{ $provinsi }}</td>
                </tr>
                <tr>
                    <td>Jenis Kegiatan</td>
                    <td>:</td>
                    <td>Kegiatan</td>
                </tr>
                <tr>
                    <td>MAK</td>
                    <td>:</td>
                    <td>{{ $mak }}</td>
                </tr>
                <tr>
                    <td>Rincian</td>
                    <td>:</td>
                    <td>{{ $metode }}</td>
                </tr>
            </table>
        </div>
    </div>

    

    <div  class="w3-row w3-tiny">
    <h5><u>I  PENGEMBALIAN</u></h5>
        <div class="col-md-12">
        <table border="1" cellpadding="2" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="w3-center" >No</th>
                        <th class="w3-center" >MAK</th>
                        <th class="w3-center" >RINCIAN</th>
                        <th class="w3-center">UANG MUKA (Rp.)</th>
                        <th class="w3-center">DIPERTANGGUNG <br> JAWABKAN (Rp.)</th>
                        <th class="w3-center">PENGEMBALIAN (Rp.)</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $x = 0;
                $total = 0;
                $total_aju = 0;
                $total_kembali = 0;
                ?>
                @foreach($detail as $rows)
                <?php $x++;
                $total = $total + ($rows->jumlah_pengajuan - $rows->jumlah_pertanggungjawaban);
                $total_aju = $total_aju + $rows->jumlah_pengajuan;
                $total_kembali = $total_kembali + $rows->jumlah_pertanggungjawaban;
                 ?>
                    <tr>
                        <td style="width:5%;">{{ $x }}</td>
                        <td style="width:10%;">{{ $rows->akun }}</td>
                        <td style="width:40%;">{{ $rows->uraian }}</td>
                        <td class="w3-right-align" style="width:10%;">{{ number_format($rows->jumlah_pengajuan , 0 , ',' , '.') }}</td>
                        <td class="w3-right-align" style="width:15%;">{{ number_format($rows->jumlah_pertanggungjawaban , 0 , ',' , '.') }}</td>
                        <td class="w3-right-align" style="width:10%;">{{ number_format($rows->jumlah_pengajuan - $rows->jumlah_pertanggungjawaban , 0 , ',' , '.')  }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan = '3' class="w3-right-align" >
                    <b>TOTAL</b>
                    </td>
                    <td class="w3-right-align"><b>{{ number_format($total_aju , 0 , ',' ,'.') }}</b></td>
                    <td class="w3-right-align"><b>{{ number_format($total_kembali , 0 , ',' ,'.') }}</b></td>
                    <td class="w3-right-align"><b>{{ number_format($total , 0 , ',' ,'.') }}</b></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="w3-row w3-tiny">
    <h5><u>II DATA PENDUKUNG</u></h5>
        <div class="col-md-12">
        <table border="1" cellpadding="5" cellspacing="0" width="50%">
                <tr>
                    <th style="text-align: center;padding: 2px;">No</th>
                    <th style="text-align: center;padding: 10px;width: 450px;">RINCIAN</th>
                    <th style="text-align: center;padding: 10px;">ADA</th>
                    <th style="text-align: center;padding: 10px;">TIDAK</th>
                </tr>
                <tr>
                    <td style="text-align: center;">1</td>
                    <td>Hardcopy Nominatif</td>
                    <td style="text-align: center;"></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="text-align: center;">2</td>
                    <td>Absensi</td>
                    <td style="text-align: center;"></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="text-align: center;">3</td>
                    <td>Kuitansi rill</td>
                    <td style="text-align: center;"></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="text-align: center;">4</td>
                    <td>Bill Penginapan</td>
                    <td style="text-align: center;"></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="text-align: center;">5</td>
                    <td>SPD</td>
                    <td style="text-align: center;"></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="w3-row w3-tiny">
        <div class="col-md-12">
            <h5>Jakarta,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ date('d M Y', strtotime($tanggal)) }}</h5> 
            <!-- <br> -->
            <h5>Mengetahui,</h5>
            <table border="0" cellpadding="5" cellspacing="0" width="100%">
                <thead >
                    <tr>
                        <td>
                            Pejabat Pembuat Komitmen
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>{{ $jabatan_pimpinan }}</td>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan=5>&nbsp;</td>
                    </tr>
                    <tr>
                    <td colspan=5>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>{{ $nama_ppk }}
                        <br>
                        NIP.{{ $nip_ppk }}
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>{{ $nama_pimpinan }}
                        <br />NIP.{{ $nip_pimpinan }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <hr>

    <div class="w3-row w3-tiny">
        <div class="col-md-12">
            <table border="0" cellpadding="5" cellspacing="0" width="100%">
                <thead >
                    <tr>
                        <td>
                            Diterima : <br> Bendahara Pengeluaran
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        
                    </tr>
                </thead>
                <tbody>
                    <tr><td>&nbsp;</td></tr>
                    <tr><td>&nbsp;</td></tr>                        
                    <tr>
                        <td>{{ $nama_bend }}
                        <br>
                        NIP. {{ $nip_bend }}
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>