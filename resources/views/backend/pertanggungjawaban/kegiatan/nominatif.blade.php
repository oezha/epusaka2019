
<div class='panel panel-default'>
<div class='panel-heading'>
      <div class='panel-title pull-left'>List Nominatif</div>
      <div class='panel-title pull-right'>
      <a href="javascript:void(0)" class='btn btn-danger btn-sm' onclick='{{ CRUDBooster::deleteConfirm("/admin/keg/$id/nominatif/reset") }}'><i class='fa fa-trash'></i> Hapus Semua</a>
      <a href="javascript:void(0)" class='btn btn-primary btn-sm' data-toggle="modal" data-target="#import-nominatif"><i class='fa fa-upload'></i> Upload</a>
      <a href="{{ route('download-excel-keg' , $id) }}" class='btn btn-info btn-sm'><i class='fa fa-download'></i> Download Excel</a>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class='panel-body'>  
        <table class='table table-striped table-bordered'>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIP</th>
                    <th>Instansi / Jabatan</th>
                    <th>Golongan</th>
                    <th>Lama (Hari)</th>
                    <th>Pesawat</th>
                    <th>Taksi Provinsi</th>
                    <th>Taksi Kabupaten</th>
                    <th>Uang Harian</th>
                    <th>Penginapan</th>
                    <th>Total</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @if(Count($result))
                @foreach($result as $key => $row)
                @php
                    $totalpesawat   = $totalpesawat + $row->tiket_pesawat;
                    $totaltaksiprov = $totaltaksiprov + $row->taksi_provinsi;
                    $totaltaksikab  = $totaltaksikab + $row->taksi_kabupaten;
                    $totaluh        = $totaluh + $row->uang_harian;
                    $totalinap      = $totalinap + $row->penginapan;
                    $total          = $totalinap + $totalpesawat + $totaltaksikab + $totaltaksiprov + $totaluh;
                @endphp
                <tr>
                <td>{{$key + 1}}</td>
                    <td>{{$row->nama_peserta}}</td>
                    <td>{{$row->nip}}</td>
                    <td>{{$row->instansi}}</td>
                    <td>{{$row->golongan}}</td>
                    <td>{{$row->lama}}</td>
                    <td class='text-right'>{{ number_format($row->tiket_pesawat , 0 , ',' , '.' ) }}</td>
                    <td class='text-right'>{{ number_format($row->taksi_provinsi , 0 , ',' , '.' ) }}</td>
                    <td class='text-right'>{{ number_format($row->taksi_kabupaten , 0 , ',' , '.' ) }}</td>
                    <td class='text-right'>{{ number_format($row->uang_harian , 0 , ',' , '.' ) }}</td>
                    <td class='text-right'>{{ number_format($row->penginapan , 0 , ',' , '.' ) }}</td>
                    <td class='text-right'>
                        <label for="total">{{ number_format($row->tiket_pesawat + $row->taksi_provinsi + $row->taksi_kabupaten + $row->uang_harian + $row->penginapan , 0 , ',' , '.' ) }}</label>
                    </td>
                    
                </tr>
                @endforeach
                <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td class='text-right'><strong>{{ number_format($totalpesawat , 0 , ',' , '.')}}</strong></td>
                <td class='text-right'><strong>{{ number_format($totaltaksiprov , 0 , ',' , '.')}}</strong></td>
                <td class='text-right'><strong>{{ number_format($totaltaksikab , 0 , ',' , '.')}}</strong></td>
                <td class='text-right'><strong>{{ number_format($totaluh , 0 , ',' , '.')}}</strong></td>
                <td class='text-right'><strong>{{ number_format($totalinap , 0 , ',' , '.')}}</strong></td>
                <td class='text-right'><strong>{{ number_format($total , 0 , ',' , '.')}}</strong></td>
                
                </tr>
            @else
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    
                </tr>
            @endif
            
            </tbody>
        </table>
    </div>
</div>

    
