<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <p><a title='Main Module' href='http://127.0.0.1:8000/admin/detail_rpd'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; Back To List Data Detail RPD</a></p>
  <div class='panel panel-default'>
    <div class='panel-heading'>Edit Form</div>
    <form method='post' action="{{ CRUDBooster::mainpath('edit-save/' . $row->id) }}">
    <div class='panel-body'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" >
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Kegiatan
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="Kegiatan"
                required    maxlength=255 class='form-control'
                name="no_mak_sys" id="no_mak_sys" value='{{$row->no_mak_sys}}'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Uraian
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="Uraian"
                required    maxlength=255 class='form-control'
                name="no_mak_sys" id="no_mak_sys" value='{{$row->uraian}}' readonly='true'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Pagu
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="pagu"
                required    maxlength=255 class='form-control text-right'
                name="pagu" id="pagu" value='{{ number_format($row->jumlah , 0 , "," , ".")}}' readonly='true'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Januari
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="januari"
                required    maxlength=15 class='form-control text-right'
                name="jan" id="jan" value='{{$row->jan}}' onFocus = 'this.select()'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Februari
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="Februari"
                required    maxlength=15 class='form-control text-right'
                name="feb" id="feb" value='{{$row->feb}}' onFocus = 'this.select()'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Maret
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="Maret"
                required    maxlength=15 class='form-control text-right'
                name="mar" id="mar" value='{{$row->mar}}' onFocus = 'this.select()'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            April
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="April"
                required    maxlength=15 class='form-control text-right'
                name="apr" id="apr" value='{{$row->apr}}' onFocus = 'this.select()'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Mei
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="Mei"
                required    maxlength=15 class='form-control text-right'
                name="mei" id="mei" value='{{$row->mei}}' onFocus = 'this.select()'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Juni
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="Juni"
                required    maxlength=15 class='form-control text-right'
                name="jun" id="jun" value='{{$row->jun}}' onFocus = 'this.select()'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Juli
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="Juli"
                required    maxlength=15 class='form-control text-right'
                name="jul" id="jul" value='{{$row->jul}}' onFocus = 'this.select()'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Agustus
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="Agustus"
                required    maxlength=15 class='form-control text-right'
                name="aug" id="aug" value='{{$row->aug}}' onFocus = 'this.select()'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            September
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="September"
                required    maxlength=15 class='form-control text-right'
                name="sep" id="sep" value='{{$row->sep}}' onFocus = 'this.select()'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Oktober
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="Oktober"
                required    maxlength=15 class='form-control text-right'
                name="oct" id="oct" value='{{$row->oct}}' onFocus = 'this.select()'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            November
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="November"
                required    maxlength=15 class='form-control text-right'
                name="nov" id="nov" value='{{$row->nov}}' onFocus = 'this.select()'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Desember
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="Desember"
                required    maxlength=15 class='form-control text-right'
                name="dec" id="dec" value='{{$row->dec}}' onFocus = 'this.select()'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-no_mak_sys' style="">
            <label class='control-label col-sm-2 text-right'>
            Total
            <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <input type='text' title="Total"
                required    maxlength=15 class='form-control text-right'
                name="total" id="total" value='0' readonly='true'/>

                <div class="text-danger"></div>
            <p class='help-block'></p>

            </div>
        </div>
        

         
        <!-- etc .... -->
        
      
    </div>
    <div class='panel-footer text-center'>
    <a href='http://127.0.0.1:8000/admin/detail_rpd?' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> Back</a>
      <input type='submit' name='submit' class='btn btn-primary' value='Save changes'/>
    </div>
    </form>
  </div>
@endsection