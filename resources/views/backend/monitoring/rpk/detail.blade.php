<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <p><a title='Main Module' href='http://127.0.0.1:8000/admin/detail_rpd'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; Back To List </a></p>
  <div class='panel panel-default'>
    <div class='panel-heading'>Detail Form</div>
   
        <div class='panel-body'>
          <?php foreach($forms as $index=>$form):?>
                    {{$form['value']}}
            <?php endforeach;?>
       </div>
  </div>
@endsection