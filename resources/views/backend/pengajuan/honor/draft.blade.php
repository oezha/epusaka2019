<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'><i class="fa fa-file-text-o"></i> Draft Pengajuan Honor</div>
    <div class='panel-body'>      
        
        <div class='row'>
            <div class='col-xs-12'>
                <div class='nav-tabs-custom'>
                    <ul class="nav nav-tabs pull-right">
                        <li><a href="#tab_2-2" data-toggle="tab">Nominatif</a></li>
                        <li class="active"><a href="#tab_3-1" data-toggle="tab">Kegiatan</a></li>
                        <li class="pull-left header"></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_3-1">
                            <form class="form-horizontal">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="no_pengajuan2" class="col-sm-2 control-label">No. AJU</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="no_pengajuan2"
                                                    value="{{ $row->no_pengajuan }}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="posisi_dokumen" class="col-sm-2 control-label">Posisi Dok.</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="posisi_dokumen"
                                                    value="{{ $status->posisi_dokumen}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="keterangan" class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="keterangan"
                                                    value="{{ $status->keterangan}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama_bagian" class="col-sm-2 control-label">Bagian</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="nama_bagian"
                                                    value="{{$bagian->nama}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="no_mak" class="col-sm-2 control-label">No. Mak</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="no_mak"
                                                    value="{{$row->no_mak}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama_kegiatan" class="col-sm-3 control-label">Kegiatan</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" id="nama_kegiatan" rows="3" readonly>{{$row->nama_kegiatan}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tgl_awal" class="col-sm-3 control-label">Tgl. Kegiatan</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="tgl_awal"
                                                    value="{{ date('d M', strtotime($row->tgl_awal)) }} s.d {{ date('d M Y', strtotime($row->tgl_akhir)) }}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="total_realisasi" class="col-sm-3 control-label">Jumlah Pengajuan</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control text-right" id="total_realisasi"
                                                    value="{{ number_format($row->total_pengajuan , 0 , ',' , '.') }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- START DETAIL TABLE -->
                                <div class='row'>
                                    <table class='table table-bordered' id="tableAkun">
                                        <thead>
                                            <th class='text-center'><strong>Akun</strong></th>
                                            <th class='text-center'><strong>Uraian</strong></th>
                                            <th class='text-center'><strong>Jumlah</strong></th>
                                        </thead>
                                        <tbody>
                                            @foreach($detail as $details)
                                            
                                            <tr>
                                                <td>{{ $details->akun }}</td>
                                                <td>{{ $details->uraian }}</td>
                                                <td class='text-right'>{{ number_format($details->jumlah_pengajuan , 0 , ',' , '.') }}</td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan=2 class='text-right'><strong>TOTAL</strong></td>
                                                <td class='text-right'>{{ number_format($row->total_pengajuan , 0 , ',' , '.') }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DETAIL TABLE -->
                                </form>
                                @if(CRUDBooster::myPrivilegeName() == 'Bendahara')
                                    <form action="{{ route('sendBend-hn' , $id) }}" method="post" class='form-horizontal' id='form'>
                                        <input type="hidden" name='_token' value='{{ csrf_token() }}'>
                                        <div class='row'>
                                            <div class='col-md-6'>
                                                <div class='form-group'>
                                                    <label for="metode_bayar" class="col-sm-2 control-label">Metode Bayar</label>
                                                    <div class="col-sm-10">
                                                        <select name="metode_bayar_id" id="metode_bayar_id" class='form-control' require>
                                                        @foreach($metode_bayar as $key => $metode)
                                                        <option
                                                        <?php if($metode->id == $row->metode_bayar_id ) { echo "selected" ;} ?>
                                                        value="{{ $metode->id }}">{{ $metode->metode_bayar}}
                                                            </option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-md-6'>
                                                <div class='form-group'>
                                                    <label for="status_id" class="col-sm-2 control-label">Status</label>
                                                    <div class="col-sm-10">
                                                        <select name="status_id" id="status_id" class='form-control' require>
                                                        @foreach($status_bend as $key => $stat)
                                                        <option value="{{ $stat->id }}"
                                                        <?php if($stat->id == $row->status_id ) { echo "selected" ;} ?>
                                                        >{{ $stat->keterangan }}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            <div class='form-group'>
                                                <label for="alasan" class="col-sm-2 control-label">Alasan</label>
                                                <div class="col-sm-10">
                                                <textarea class="form-control" rows="5" id="alasan" name='alasan'
                                                @if(empty($row->alasan))
                                                readonly
                                                @endif
                                                ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            
                        </div>
                        <div class="tab-pane" id="tab_2-2">
                        <div class="panel panel-default">
                        <div class='panel-heading'>
                            <div class='panel-title pull-left'>
                            List Penerima Honor
                            </div>
                            <div class='panel-title pull-right'>
                            <a href="javascript:void(0)" class='btn btn-danger btn-sm' onclick='{{ CRUDBooster::deleteConfirm("/admin/hn/$id/reset") }}'><i class='fa fa-trash'></i> Hapus Semua</a>
                            <a href="javascript:void(0)" class='btn btn-primary btn-sm' data-toggle="modal" data-target="#import-nominatif"><i class='fa fa-upload'></i> Upload</a>
                            <a href="{{ route('download-excel-honor' , $id) }}" class='btn btn-info btn-sm'><i class='fa fa-download'></i> Download Excel Kosong</a>
                            <a href="{{ route('hn-list-penerima' , $id) }}" class='btn btn-success btn-sm'><i class='fa fa-print'></i> Cetak </a>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                        </div>
                        <table class='table table-striped table-bordered'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    
                                    <th>Instansi / Jabatan</th>
                                    <th>NPWP</th>
                                    
                                    <th>Jumlah Honor</th>
                                    <th>Jumlah Potongan</th>
                                    <th>Jumlah Terima</th>
                                    
                                    
                                </tr>
                            </thead>
                            <tbody>
                            @if(Count($result) > 0)
                                @foreach($result as $key => $rows)
                                <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $rows->nama_penerima }}</td>
                               
                                <td>{{ $rows->instansi }}</td>
                                <td>{{ $rows->npwp }}</td>
                                
                                <td class='text-right'>{{ number_format($rows->jumlah_honor , 0 , "," , ".")}}</td>
                                <td class='text-right'>{{ number_format($rows->jumlah_potongan , 0 , "," , ".")}}</td>
                                <td class='text-right'>{{ number_format($rows->jumlah_terima , 0 , "," , ".")}}</td>
                                
                                </tr>
                                @endforeach
                            @else
                            
                            @endif
                            
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        


         <div class="text-center">
         <a href="{{ url()->previous() }}" class='btn btn-default float-left'><i class='fa fa-arrow-left'></i> Back</a>
         <input type="hidden" name='previous' id='previous' value="{{ URL::previous() }}">
            @if(CRUDBooster::myPrivilegeName() == 'user')
                <a href="{{ route('senddraft-hn' , $id)}}" class='btn btn-success float-right'><i class='fa fa-send'></i> Kirim</a>
            @endif
            @if(CRUDBooster::myPrivilegeName() == 'Bendahara' && $row->status_id != 1)
            
            <button type='submit' value='Kirim' class='btn btn-info float-right'>
                <i class='fa fa-send'> Kirim</i>
            </button>
            
          
            @endif
         </div>
         </form>
                
            
         
        <!-- etc .... -->
        </div>
      
    </div>
  </div>
  <div class="modal fade" id="import-nominatif">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" action="{{ route('import-excel-honor' ,  $id) }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload Nominatif</h4>
                </div>
                <div class="modal-body">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="import_file" class="col-sm-3 control-label">FILE</label>
                        <div class="col-sm-9 {{ $errors->has('import_file') ? 'has-error' : '' }}">
                            <input type="file" name="import_file" class="form-control" id="import_file" value="{{ old('import_file') }}" required>
                            @if($errors->has('import_file'))
                                <span class="help-block">
                                    {{ $errors->first('import_file') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batal </button>
                    <button type="submit" class="btn bg-light-blue btn-sm"><i class="fa fa-plus"></i> Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection