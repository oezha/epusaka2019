<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <p><a title='Return' href='http://127.0.0.1:8000/admin/keg'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; Back To List Data Kegiatan</a></p>
  <div class='panel panel-default'>
    <div class='panel-heading'>Add Form</div>
    <div class='panel-body'>
    <form method='post' action='{{CRUDBooster::mainpath("edit-save")}}/{{$id}}'>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="box-body" id="parent-form-area">
        <div class='form-group header-group-0 ' id='form-group-bagian_id' style="">
            <label class='control-label col-sm-2'>Bagian
                    <span class='text-danger' title='This field is required'>*</span>
            </label>
            <div class="col-sm-10">
                <!-- <select style='width:100%' class='form-control' id="bagian_id"  required    name="bagian_id"  >
                            <option value=''>** Please select a Bagian</option>
                                <option selected value='1'></option>                    
                </select> -->
                <input type="text" name="bagian_name" id="bagian_name" required readonly class='form-control'>
                <div class="text-danger">
                    
                </div><!--end-text-danger-->
                <p class='help-block'></p>
            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-rkakl_id' style="">
            <label class='control-label col-sm-2'>Nama Kegiatan
                            <span class='text-danger' title='This field is required'>*</span>
                    </label>
    
            <div class="col-sm-10">
                <select class='form-control' id="rkakl_id" data-value='' required    name="rkakl_id">
                    <option value=''>** Please select a Nama Kegiatan</option>
                    @foreach($no_mak as $key => $nomak)
                    <option value="{{$nomak['id']}}"
                    <?php
                    if($master->rkakl_id == $nomak['id'])
                        echo 'Selected';
                    ?>
                    
                    >{{$nomak['no_mak']}} - {{$nomak['uraian']}}</option>
                    @endforeach
                            
                            </select>
                <div class="text-danger"></div>
                <p class='help-block'></p>
            </div>
        </div>
        <div class='form-group form-datepicker header-group-0 ' id='form-group-tgl_awal' style="">
                <label class='control-label col-sm-2'>Tgl Awal
                                <span class='text-danger' title='This field is required'>*</span>
                        </label>

                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                        <input type='text' title="Tgl Awal" readonly
                            required    class='form-control notfocus input_date' name="tgl_awal" id="tgl_awal"
                            value='{{ $master->tgl_awal }}' />
                    </div>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
        </div>
        <div class='form-group form-datepicker header-group-0 ' id='form-group-tgl_akhir'
                style="">
                <label class='control-label col-sm-2'>Tgl Akhir
                                <span class='text-danger' title='This field is required'>*</span>
                        </label>

                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                        <input type='text' title="Tgl Akhir" readonly
                            required    class='form-control notfocus input_date' name="tgl_akhir" id="tgl_akhir"
                            value='{{ $master->tgl_akhir }}'/>
                    </div>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
        </div>
    </div>
    
    
    <div class='panel-footer text-center'>
    <a href='http://127.0.0.1:8000/admin/keg' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> Back</a>
      <input type='submit' class='btn btn-success' value='Save changes'/>
    </div>
    </form>
  </div>
@endsection