<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>Add Form</div>
    <div class='panel-body'>
      <form method='post' action="{{ route( 'simpan-hn-nom' , $id ) }}">
        <input type="hidden" name='_token' id='_token' value = '{{ csrf_token() }}'>
        <input type='hidden' name='id' id='id' value='{{ $id }}' />
        <div class='form-group'>
          <label>Nama Pegawai</label>
          <select name="nama_peserta" id="nama_peserta" class='form-control' require>
            <option value="">** Pilih Nama Pegawai</option>
            @foreach($pegawai as $peg)
                <option value="{{ $peg->id }}">{{$peg->nama}}</option>
            @endforeach
          
          </select>
        </div>
        <div class='form-group'>
          <label>NIP</label>
          <input type='text' name='nip' id='nip' required class='form-control' readonly/>
        </div>
        <div class='form-group'>
          <label>Instansi</label>
          <input type='text' name='instansi' id='instansi' required class='form-control' readonly/>
        </div>
        <div class='form-group'>
          <label>Golongan</label>
          <input type='text' name='golongan' id='golongan' required class='form-control' readonly/>
        </div>
        <table class='table table-striped table-bordered'>
            <thead>
            <th class='text-center'>Uraian</th>
            <th class='text-center'>Jumlah</th>
            <th class='text-center'>Total</th>
            </thead>
            <tbody>
            <!-- Jumlah Honor -->
            <tr>
                    <td>Jumlah Honor</td>
                    <td>
                    <input type='text' title="jumlah_honor" required    maxlength=15 class='form-control text-right' name="jumlah_honor" id="jumlah_honor" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                        <label for=""></label>
                    </td> 
                    <td>
                    <input type='text' title="nilai" required readonly    maxlength=15 class='form-control text-right' name="nilai" id="nilai" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td>
            </tr>
            <tr>
                    <td>Jumlah Potongan</td>
                    <td>
                    <input type='text' title="jumlah_potongan" required    maxlength=15 class='form-control text-right' name="jumlah_potongan" id="jumlah_potongan" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                        <label for=""></label>
                    </td> 
                    <td>
                    <input type='text' title="potongan" required readonly    maxlength=15 class='form-control text-right' name="potongan" id="potongan" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td>
            </tr>
            <tr>
                    <td>Jumlah DiTerima</td>
                    <td>
                   
                    </td> 
                    <td>
                    <input type='text' title="terima" required readonly    maxlength=15 class='form-control text-right' name="terima" id="terima" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td>
            </tr>
                <tr></tr>
            </tbody>

        </table>
      
    </div>
    <div class='panel-footer text-center'>
      <input type='submit' class='btn btn-primary' value='Save changes'/>
    </div>
    </form>
  </div>
@endsection