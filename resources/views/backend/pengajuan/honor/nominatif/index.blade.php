<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your custom  HTML goes here -->

<div class='panel panel-default'>

    <div class='panel-heading'>
    <div class='panel-title pull-left'>
    List Penerima Honor
    </div>
    <div class='panel-title pull-right'>
    <a href="javascript:void(0)" class='btn btn-danger btn-sm' onclick='{{ CRUDBooster::deleteConfirm("/admin/hn/$id/reset") }}'><i class='fa fa-trash'></i> Hapus Semua</a>
    <a href="javascript:void(0)" class='btn btn-primary btn-sm' data-toggle="modal" data-target="#import-nominatif"><i class='fa fa-upload'></i> Upload</a>
    <a href="{{ route('download-excel-honor' , $id) }}" class='btn btn-info btn-sm'><i class='fa fa-download'></i> Download Excel Kosong</a>
    </div>
    <div class="clearfix"></div>
    </div>
    
    <div class='panel-body'>
      <table class='table table-striped table-bordered'>
        <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              
              <th>Instansi / Jabatan</th>
              <th>NPWP</th>
              
              <th>Jumlah Honor</th>
              <th>Jumlah Potongan</th>
              <th>Jumlah Terima</th>
              
              <th></th>
            </tr>
        </thead>
        <tbody>
        @if(Count($result))
          @foreach($result as $key => $row)
          <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $row->nama_penerima }}</td>
            
            <td>{{ $row->instansi }}</td>
            <td>{{ $row->npwp }}</td>
            
            <td class='text-right'>{{ number_format($row->jumlah_honor , 0 , "," , ".")}}</td>
            <td class='text-right'>{{ number_format($row->jumlah_potongan , 0 , "," , ".")}}</td>
            <td class='text-right'>{{ number_format($row->jumlah_terima , 0 , "," , ".")}}</td>
            <td>

              
              <a href="javascript:void(0)" class='btn btn-danger btn-sm' onclick='{{ CRUDBooster::deleteConfirm("nominatif-hn-index") }}'><i class='fa fa-trash'></i></a>

            </td>
          </tr>
          @endforeach
        @else
        <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>
              <a href='/admin/hn/{{ $id }}/nom/add' class="btn btn-primary btn-sm" title='Tambah Pelaksana Pusat'><i class="fa fa-plus"></i></a>
                <a href="/admin/hn/{{ $id }}/nom/addguest" class="btn btn-warning btn-sm" title='Tambah Pelaksana Lainnya'><i class="fa fa-plus"></i></a></td>
          </tr>
        @endif
        <tr>
                  <td colspan=12 class='text-center'>
                  <a href='#' class='btn btn-default btn-sm'><i class='fa fa-chevron-circle-left'></i> Back</a>
                  <a href='{{ route("draft-hn" , $id) }}' class='btn btn-success btn-sm'>Selanjutnya <i class='fa fa-arrow-right'></i></a>
            
                  </td>
        </tr>
        </tbody>
      </table>
    </div>
</div>
<div class="modal fade" id="import-nominatif">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" action="{{ route('import-excel-honor' ,  $id) }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload Nominatif</h4>
                </div>
                <div class="modal-body">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="import_file" class="col-sm-3 control-label">FILE</label>
                        <div class="col-sm-9 {{ $errors->has('import_file') ? 'has-error' : '' }}">
                            <input type="file" name="import_file" class="form-control" id="import_file" value="{{ old('import_file') }}" required>
                            @if($errors->has('import_file'))
                                <span class="help-block">
                                    {{ $errors->first('import_file') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batal </button>
                    <button type="submit" class="btn bg-light-blue btn-sm"><i class="fa fa-plus"></i> Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection