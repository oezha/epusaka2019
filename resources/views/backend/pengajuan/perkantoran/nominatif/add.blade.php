<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>Add Form</div>
    <div class='panel-body'>
      <form method='post' action="{{ route( 'simpan-nom-perjadin' , $id ) }}">
        <input type="hidden" name='_token' id='_token' value = '{{ csrf_token() }}'>
        <input type='hidden' name='id' id='id' value='{{ $id }}' />
        <div class='form-group'>
          <label>Nama Pegawai</label>
          <select name="nama_peserta" id="nama_peserta" class='form-control' require>
            <option value="">** Pilih Nama Pegawai</option>
            @foreach($pegawai as $peg)
                <option value="{{ $peg->id }}">{{$peg->nama}}</option>
            @endforeach
          
          </select>
        </div>
        <div class='form-group'>
          <label>NIP</label>
          <input type='text' name='nip' id='nip' required class='form-control' readonly/>
        </div>
        <div class='form-group'>
          <label>Instansi</label>
          <input type='text' name='instansi' id='instansi' required class='form-control' readonly/>
        </div>
        <div class='form-group'>
          <label>Golongan</label>
          <input type='text' name='golongan' id='golongan' required class='form-control' readonly/>
        </div>
        <table class='table table-striped table-bordered'>
            <thead>
            <th class='text-center'>Uraian</th>
            <th class='text-center' colspan='4'>Jumlah</th>
            <th class='text-center'>Total</th>
            </thead>
            <tbody>
            <!-- Uang harian -->
                <tr>
                    <td>Uang harian</td>
                    <td style='width:px'>
                    <input type='text' title="nilai" required    maxlength=15 class='form-control text-right' name="uh" id="uh" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                        <label for=""></label>
                    </td> 
                    <td style='width:3px'>
                        <label for="">X</label>
                    </td>
                    <td>   
                    <input type='text' title="nilai" required    maxlength=2 class='form-control text-right' name="lamauh" id="lamauh" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td>
                    <td style='width:3px'><label for="">Hari</label></td>
                    <td>
                    <input type='text' title="nilai" required readonly    maxlength=15 class='form-control text-right' name="totaluh" id="totaluh" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td>
                </tr>
                <!-- Taksi Provinsi -->
                <tr>
                    <td>Taksi Provinsi</td>
                    <td colspan='4'>
                        <input type='text' title="nilai"  required
                        maxlength=15 class='form-control text-right' name="taksi_prov" id="taksi_prov" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td> 
                        
                    
                    
                    <td>
                    <input type='text' title="nilai" required readonly   maxlength=15 class='form-control text-right' name="total_taksi_prov" id="total_taksi_prov" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td>
                </tr>
                <!-- Taksi Kabupaten -->
                <tr>
                    <td >Taksi Kabupaten</td>
                    <td colspan='4'>
                        <input type='text' title="nilai"  required
                        maxlength=15 class='form-control text-right' name="taksi_kab" id="taksi_kab" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td> 
                    <td >
                    <input type='text' title="nilai" required readonly    maxlength=15 class='form-control text-right' name="total_taksi_kab" id="total_taksi_kab" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td>
                </tr>
                <!-- Penginapan -->
                <tr>
                    <td >Penginapan</td>
                    <td>
                        <input type='text' title="nilai"  required
                        maxlength=15 class='form-control text-right' name="penginapan" id="penginapan" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td> 
                        
                    <td style='width:3px'>
                        <label for="">X</label>
                    </td>
                    <td>
                    <input type='text' title="nilai"  required
                        maxlength=2 class='form-control text-right' name="lama_penginapan" id="lama_penginapan" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td>
                    <td style='width:3px'><label for="">Hari</label></td>
                    <td >
                    <input type='text' title="nilai" required readonly    maxlength=15 class='form-control text-right' name="total_penginapan" id="total_penginapan" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td>
                </tr>
                <!-- Pesawat -->
                <tr>
                    <td >Tiket Pesawat</td>
                    <td colspan='4'>
                        <input type='text' title="nilai"  required
                        maxlength=15 class='form-control text-right' name="tiket_pesawat" id="tiket_pesawat" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td>                     
                    <td >
                    <input type='text' title="nilai" required readonly    maxlength=15 class='form-control text-right' name="total_tiket_pesawat" id="total_tiket_pesawat" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td>
                </tr>
                <tr>
                    <td colspan='5' class='text-right'>
                    <strong>TOTAL</strong>
                    </td>
                    
                    <td>
                    <input type="text" name='total' id='total' readonly class='form-control text-right' value=0>
                    </td>
                </tr>
            </tbody>

        </table>

         
        <!-- etc .... -->
        
      
    </div>
    <div class='panel-footer text-center'>
      <input type='submit' class='btn btn-primary' value='Save changes'/>
    </div>
    </form>
  </div>
@endsection