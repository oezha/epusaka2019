<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your custom  HTML goes here -->
<table class='table table-striped table-bordered'>
  <thead>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>NIP</th>
        <th>Instansi / Jabatan</th>
        <th>Golongan</th>
        <th>Lama (Hari)</th>
        <th>Pesawat</th>
        <th>Taksi Provinsi</th>
        <th>Taksi Kabupaten</th>
        <th>Uang Harian</th>
        <th>Penginapan</th>
        <th>Total</th>
        <th></th>
       </tr>
  </thead>
  <tbody>
  @if(Count($result))
    @foreach($result as $key => $row)
    @php
        $totalpesawat   = $totalpesawat + $row->tiket_pesawat;
        $totaltaksiprov = $totaltaksiprov + $row->taksi_provinsi;
        $totaltaksikab  = $totaltaksikab + $row->taksi_kabupaten;
        $totaluh        = $totaluh + $row->uang_harian;
        $totalinap      = $totalinap + $row->penginapan;
        $total          = $totalinap + $totalpesawat + $totaltaksikab + $totaltaksiprov + $totaluh;
    @endphp
      <tr>
      <td>{{$key + 1}}</td>
        <td>{{$row->nama_peserta}}</td>
        <td>{{$row->nip}}</td>
        <td>{{$row->instansi}}</td>
        <td>{{$row->golongan}}</td>
        <td>{{$row->lama}}</td>
        <td class='text-right'>{{ number_format($row->tiket_pesawat , 0 , ',' , '.' ) }}</td>
        <td class='text-right'>{{ number_format($row->taksi_provinsi , 0 , ',' , '.' ) }}</td>
        <td class='text-right'>{{ number_format($row->taksi_kabupaten , 0 , ',' , '.' ) }}</td>
        <td class='text-right'>{{ number_format($row->uang_harian , 0 , ',' , '.' ) }}</td>
        <td class='text-right'>{{ number_format($row->penginapan , 0 , ',' , '.' ) }}</td>
        <td class='text-right'>
            <label for="total">{{ number_format($row->tiket_pesawat + $row->taksi_provinsi + $row->taksi_kabupaten + $row->uang_harian + $row->penginapan , 0 , ',' , '.' ) }}</label>
        </td>
        <td>

         
          <a href="javascript:void(0)" class='btn btn-danger btn-sm' onclick='swal({   
				title: "Are you sure ?",   
				text: "You will not be able to recover this record data!",   
				type: "warning",   
				showCancelButton: true,   
				confirmButtonColor: "#ff0000",   
				confirmButtonText: "Yes!",  
				cancelButtonText: "No",  
				closeOnConfirm: false }, 
				function(){  location.href="http://127.0.0.1:8000/nominatif/peg/{{$row->id}}/destroy" });'><i class='fa fa-trash'></i></a>
          <!-- To make sure we have read access, wee need to validate the privilege -->
          <!-- @if(CRUDBooster::isUpdate() && $button_edit)
          <a class='btn btn-success btn-sm' href='{{CRUDBooster::mainpath("edit/$row->id")}}'>Edit</a>
          @endif
          
          @if(CRUDBooster::isDelete() && $button_edit)
          <a class='btn btn-success btn-sm' href='{{CRUDBooster::mainpath("delete/$row->id")}}'>Delete</a>
          @endif -->

        </td>
       </tr>
    @endforeach
    <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td class='text-right'><strong>{{ number_format($totalpesawat , 0 , ',' , '.')}}</strong></td>
    <td class='text-right'><strong>{{ number_format($totaltaksiprov , 0 , ',' , '.')}}</strong></td>
    <td class='text-right'><strong>{{ number_format($totaltaksikab , 0 , ',' , '.')}}</strong></td>
    <td class='text-right'><strong>{{ number_format($totaluh , 0 , ',' , '.')}}</strong></td>
    <td class='text-right'><strong>{{ number_format($totalinap , 0 , ',' , '.')}}</strong></td>
    <td class='text-right'><strong>{{ number_format($total , 0 , ',' , '.')}}</strong></td>
    <td>
    <a href='/admin/perjadin/{{ $id }}/nom/add' class="btn btn-primary btn-sm" title='Tambah Pelaksana Pusat'><i class="fa fa-plus"></i></a>
    <a href="/admin/perjadin/{{ $id }}/nom/addguest" class="btn btn-warning btn-sm" title='Tambah Pelaksana Lainnya'><i class="fa fa-plus"></i></a></td>
    </td>
    </tr>
@else
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>
        <a href='/admin/perjadin/{{ $id }}/nom/add' class="btn btn-primary btn-sm" title='Tambah Pelaksana Pusat'><i class="fa fa-plus"></i></a>
          <a href="/admin/perjadin/{{ $id }}/nom/addguest" class="btn btn-warning btn-sm" title='Tambah Pelaksana Lainnya'><i class="fa fa-plus"></i></a></td>
    </tr>
@endif
<tr>
            <td colspan=12 class='text-center'>
            <a href='#' class='btn btn-default btn-sm'><i class='fa fa-chevron-circle-left'></i> Back</a>
            <a href='{{ route("draft-perjadin" , $id) }}' class='btn btn-success btn-sm'>Selanjutnya <i class='fa fa-arrow-right'></i></a>
      
            </td>
</tr>
  </tbody>
</table>

<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection