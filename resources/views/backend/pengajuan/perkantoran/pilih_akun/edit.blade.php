@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class="box box-default">
  <form action="{{ route('simpan-perkantoran' , $id) }}" method="post">
  
  <input name="_token" value="{{ csrf_token() }}" type="hidden">
            <div class="box-body table-responsive no-padding">
              <table class='table table-bordered' id="tableAkun">
                <tr>
                  
                  <td class='text-center'><strong>Kode</strong></td>
                  <td class='text-center'><strong>Uraian</strong></td>
                  <td class='text-center'><strong>Vol</strong></td>
                  <td class='text-center'><strong>Sat</strong></td>
                  <td class='text-center'><strong>Harga Satuan</strong></td>
                  <td class='text-center'><strong>Pagu</strong></td>
                  <td class='text-center'><strong>Sisa Pagu</strong></td>
                  <td class='text-center'><strong>Vol Pengajuan</strong></td>
                  <td class='text-center'><strong>Jumlah Pengajuan</strong></td>
                  <td class='text-center'><strong>Sisa Anggaran</strong></td>
                </tr>
                @foreach($row as $key =>  $rows )
                  <tr>
                    
                    <td class ='text-center'>{{ $rows->kode }}</td>
                    <td>{{ $rows->uraian }}</td>
                    <td class ='text-center'>{{ $rows->vol }}</td>
                    <td class ='text-center'>{{ $rows->sat }}</td>
                    <td class='text-right'>
                      {{ number_format($rows->hargasat , 0 , ',' , '.') }}
                      <input type="hidden" name='hargasat[{{ $key }}]' id='hargasat[{{ $key }}]' value='{{ $rows->hargasat }}'>
                    </td>
                    <td class='text-right'>{{ number_format($rows->jumlah , 0 , ',' , '.') }}</td>
                    <td class='text-right'>
                    {{ number_format($rows->sisa_pagu , 0 , ',' , '.') }}
                    <input type="hidden" name='sisa_pagu[{{ $key }}]' id='sisa_pagu[{{ $key }}]' value='{{ $rows->sisa_pagu }}'>
                    </td>
                    @if($rows->level == 0 && $rows->hargasat != 0)
                    <td class='text-right'>
                      <input type='text' title="vol_pengajuan" required    maxlength=3 class='form-control text-right hitung' name="vol_pengajuan[{{$key}}]" id="vol_pengajuan[{{$key}}]" value='0' onFocus='this.select()' onkeypress='return isNumber(event);' onkeyup='return hitung(this)'/>
                    </td>
                    <td class='text-right'>
                    <input type="hidden" value="{{ $rows->id }}" name="id[{{$key}}]" id="id[{{$key}}]" >
                    <input type='text' title="nilai" required    maxlength=15 class='form-control text-right nilai' name="nilai[{{$key}}]" id="nilai[{{$key}}]" value='0' onFocus='this.select()' onkeypress='return isNumber(event)' onkeyup='return addcoma(this)'/>
                    </td>
                    <td class='text-right'>
                    <input type='text' title="nilai"
                      required    maxlength=15 class='form-control text-right sisa'
                      name="sisa_anggaran[{{$key}}]" id="sisa_anggaran[{{$key}}]" value='{{ number_format($rows->sisa_pagu , 0 , "," , ".") }}' onFocus = 'this.select()' readonly/>
                    </td>
                    @else
                    <td></td>
                    <td></td>
                    @endif
                  </tr>
                  
                @endforeach
                <tr>
                    <td colspan=8 class='text-right'>
                    <strong>
                    TOTAL
                    </strong>
                    </td>
                    <td>
                      <input type="text" name="total_pengajuan" id="total_pengajuan" class="form-control text-right" value="0" readonly onkeyup="getSisa();">
                    </td>
                    <td>
                    <input type="text" name="total_sisa" id="total_sisa" class="form-control text-right" value="0" readonly>
                    </td>
                  </tr>
              </table>
            </div>
            <div class='panel-footer text-center'>
    <a href='#' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> Back</a>
      <input type='submit' name='submit' class='btn btn-primary' value='Save changes'/>
    </div>
    </form>
  </div>
@endsection