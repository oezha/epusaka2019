<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>Add Form</div>
    <div class='panel-body'>
      <form method='post' action='{{CRUDBooster::mainpath("add-save")}}'>
        <!-- Start Form -->
        <div class='form-group form-datepicker header-group-0 ' id='form-group-tgl_awal' style="">
                <label class='control-label col-sm-2'>Tanggal
                                <span class='text-danger' title='This field is required'>*</span>
                        </label>

                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                        <input type='text' title="Tanggal" readonly
                            required    class='form-control notfocus input_date' name="tgl_pengajuan" id="tgl_pengajuan"
                            value=''/>
                    </div>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-rkakl_id' style="">
            <label class='control-label col-sm-2'>No Mak
                            <span class='text-danger' title='This field is required'>*</span>
                    </label>

            <div class="col-sm-10">
            <select class='form-control' id="rkakl_id" data-value='' required name="rkakl_id">
                    <option value=''>** Please select a Nama Kegiatan</option>
                    @foreach($no_mak as $key => $value)
                    <option value="{{ $value->rkakl_id }}">{{ $value->no_mak_sys }} - {{ $value->uraian }}</option>
                    @endforeach
                    </select>
                <div class="text-danger">
                    
                </div><!--end-text-danger-->
                <p class='help-block'></p>

            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-keterangan' style="">
                <label class='control-label col-sm-2'>Keterangan
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    <input type="text" name="keterangan" id="keterangan" title="No Surat Tugas" required class="form-control">
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
        </div>
        <!-- End Form -->
      
    </div>
    <div class='panel-footer  text-center'>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <a href='http://127.0.0.1:8000/admin/perkantoran' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> Back</a>
      <input type='submit' class='btn btn-primary' value='Save changes'/>
    </div>
    </form>
  </div>
@endsection