<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>Edit Form</div>
    <div class='panel-body'>
    <form method='post' action='{{CRUDBooster::mainpath("edit-save")}}/{{$id}}'>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="box-body" id="parent-form-area">
        <div class='form-group form-datepicker header-group-0 ' id='form-group-tgl_pengajuan' style="">
                <label class='control-label col-sm-2'>Tanggal pengajuan
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                        <input type='text' title="Tgl Surat Tugas" required class='form-control notfocus input_date' name="tgl_pengajuan" id="tgl_pengajuan" value='{{$perkantoran->tgl_pengajuan}}'/>
                    </div>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>   
            <div class='form-group form-datepicker header-group-0 ' id='form-group-kode_7' style="">
                <label class='control-label col-sm-2'>No Mak
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    
                    
                    <select class='form-control' id="rkakl_id" data-value='' required name="rkakl_id">
                    <option value=''>** Please select a Nama Kegiatan</option>
                    @foreach($no_mak as $key => $value)
                    <option value="{{ $value->rkakl_id }}">{{ $value->no_mak_sys }} - {{ $value->uraian }}</option>
                    @endforeach
                    
                        
                    </select>

                     <!-- select class='form-control' id="kode_7" data-value='' required    name="kode_7">
                    <option value=''>** Please select a Nama Kegiatan</option>
                            </select> -->

                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>
            <div class='form-group form-datepicker header-group-0 ' id='form-group-uraian'
                style="">
                <label class='control-label col-sm-2'>Uraian
                    <span class='text-danger' title='This field is required'>*</span>  

                </label>
                <div class="col-sm-10">
                    <input type='text' title="Uraian" required class='form-control' name="uraian" id="uraian" value='{{$perkantoran->uraian}}'/>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>
        </div>
        </div>
    </div>
    
    
    <div class='panel-footer text-center'>
    <a href='http://127.0.0.1:8000/admin/perkantoran' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> Back</a>
      <input type='submit' class='btn btn-success' value='Save changes'/>
    </div>
    </form>
  </div>
@endsection