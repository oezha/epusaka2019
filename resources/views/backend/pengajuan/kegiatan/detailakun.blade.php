@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->


  <div class="box box-default">

            <div class="box-body table-responsive no-padding">
              <table class='table table-bordered' id="tableAkun">
                <tr>
                  <td class='text-center'> <strong> Akun</strong></td>
                  <td class='text-center'> <strong> Uraian</strong></td>
                  <td class='text-center'> <strong> Jumlah Pengajuan</strong></td>
                  <td></td>
            
                </tr>
                @if(Count($row))
                @foreach($row as  $rows )
                  <tr>
                    <td>{{ $rows->akun }}</td>
                    <td>{{ $rows->uraian }}</td>
                    <td class="text-right">{{ number_format($rows->jumlah , 0 ,  "," , ".") }}</td>
                    <td>
                      <a href = "{{ route('delete-detail-akun-keg' , $id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                @endforeach
                
                <tr>
                   
                </tr>
              </table>
              @else
                <tr>
                  <td class='text-center' colspan=3><i class="fa fa-exclamation-triangle fa-2x"></i>
                            <h4 class="no-margins">Tidak ada akun yang teregistrasi!!</h4>
                  <a href='/admin/keg/{{$id}}/pa' 
                               id='btn_add_new_data' class="btn btn-sm btn-success" title="Add Data">
                                <i class="fa fa-plus-circle"></i> Add Data
                            </a>
                  </td>
                </tr>
                <tr>
                   
                   </tr>
                 </table>
                @endif
            </div>
            
            <div class='panel-footer text-center'>
            @if(Count($row))
    <a href='#' class='btn btn-default'><i class='fa fa-chevron-circle-left'></i> Back</a>
    <a href='{{ route("nominatif-index" , $id) }}' class='btn btn-success'>Selanjutnya<i class='fa fa-arrow-right'></i></a>
      
      @endif
    </div>
    
  </div>
@endsection