<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'><i class="fa fa-file-text-o"></i> Draft Pengajuan Kegiatan</div>
    <div class='panel-body'>      
        
        <div class='row'>
            <div class='col-xs-12'>
                <div class='nav-tabs-custom'>
                    <ul class="nav nav-tabs pull-right">
                        <li><a href="#tab_2-2" data-toggle="tab">Nominatif</a></li>
                        <li class="active"><a href="#tab_3-1" data-toggle="tab">Kegiatan</a></li>
                        <li class="pull-left header"></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_3-1">
                            <form class="form-horizontal">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="no_pengajuan2" class="col-sm-2 control-label">No. AJU</label>
                                            <div class="col-sm-10">
                                            
                                                <input type="text" class="form-control" id="no_pengajuan2"
                                                    value="{{ $row->no_pengajuan }}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="posisi_dokumen" class="col-sm-2 control-label">Posisi Dok.</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="posisi_dokumen"
                                                    value="{{ $status->posisi_dokumen}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="keterangan" class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="keterangan"
                                                    value="{{ $status->keterangan}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama_bagian" class="col-sm-2 control-label">Bagian</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="nama_bagian"
                                                    value="{{$bagian->nama}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="no_mak" class="col-sm-2 control-label">No. Mak</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="no_mak"
                                                    value="{{$row->no_mak}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama_kegiatan" class="col-sm-3 control-label">Kegiatan</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" id="nama_kegiatan" rows="3" readonly>{{$row->uraian}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tgl_awal" class="col-sm-3 control-label">Tgl. Kegiatan</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="tgl_awal"
                                                    value="{{ date('d M', strtotime($row->tgl_awal)) }} s.d {{ date('d M Y', strtotime($row->tgl_akhir)) }}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="title" class="col-sm-3 control-label">Daerah</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="title"
                                                    value="{{$daerah->title}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="total_realisasi" class="col-sm-3 control-label">Jumlah Pengajuan</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control text-right" id="total_realisasi"
                                                    value="{{ number_format($row->total_pengajuan , 0 , ',' , '.') }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- START DETAIL TABLE -->
                                <div class='row'>
                                    <table class='table table-bordered' id="tableAkun">
                                        <thead>
                                            <th class='text-center'><strong>Akun</strong></th>
                                            <th class='text-center'><strong>Uraian</strong></th>
                                            <th class='text-center'><strong>Alokasi</strong></th>
                                            <th class='text-center'><strong>Sisa Alokasi</strong></th>
                                            <th class='text-center'><strong>Jumlah Pengajuan</strong></th>
                                            <th class='text-center'><strong>Sisa Anggaran</strong></th>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $total_pengajuan = 0;$total_alokasi =0;$total_sisa = 0;$total_sisa_anggaran = 0;
                                        ?>
                                            @foreach($detail as $details)
                                            <tr>
                                                <td>{{ $details->akun }}</td>
                                                <td>{{ $details->uraian }}</td>
                                                <td class='text-right'>{{ number_format($details->jumlah , 0 , ',' , '.') }}</td>
                                                <td class='text-right'>{{ number_format($details->sisa_pagu_sblm_pengajuan , 0 , ',' , '.') }}</td>
                                                <td class='text-right'>{{ number_format($details->jumlah_pengajuan , 0 , ',' , '.') }}</td>
                                                <td class='text-right'>{{ number_format($details->sisa_pagu_sblm_pengajuan - $details->jumlah_pengajuan , 0 , ',' , '.') }}</td>
                                            </tr>
                                            <?php
                                                $total_pengajuan = $total_pengajuan + $details->jumlah_pengajuan;
                                                $total_alokasi = $total_alokasi + $details->jumlah;
                                                $total_sisa = $total_sisa + $details->sisa_pagu_sblm_pengajuan;
                                                $total_sisa_anggaran = $total_sisa_anggaran + ( $details->sisa_pagu_sblm_pengajuan - $details->jumlah_pengajuan);
                                            ?>
                                            @endforeach
                                            <tr>
                                                <td colspan=2 class='text-right'><strong>TOTAL</strong></td>
                                                <td class='text-right'>{{ number_format($total_alokasi, 0 , ',' , '.') }}</td>
                                                <td class='text-right'>{{ number_format($total_sisa, 0 , ',' , '.') }}</td>
                                                <td class='text-right'>{{ number_format($total_pengajuan, 0 , ',' , '.') }}</td>
                                                <td class='text-right'>{{ number_format($total_sisa_anggaran, 0 , ',' , '.') }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DETAIL TABLE -->
                                </form>
                                @if(CRUDBooster::myPrivilegeName() == 'Bendahara')
                                    <form action="{{ route('sendBend-keg' , $id) }}" method="post" class='form-horizontal' id='form'>
                                    <input type="hidden" name='_token' value='{{ csrf_token() }}'>
                                    
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <label for="metode_bayar" class="col-sm-2 control-label">Metode Bayar</label>
                                                <div class="col-sm-10">
                                                    <select name="metode_bayar_id" id="metode_bayar_id" class='form-control' require>
                                                    @foreach($metode_bayar as $key => $metode)
                                                    <option
                                                    <?php if($metode->id == $row->metode_bayar_id ) { echo "selected" ;} ?>
                                                     value="{{ $metode->id }}">{{ $metode->metode_bayar}}
                                                        </option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <label for="status_id" class="col-sm-2 control-label">Status</label>
                                                <div class="col-sm-10">
                                                    <select name="status_id" id="status_id" class='form-control' require>
                                                    @foreach($status_bend as $key => $stat)
                                                    <option value="{{ $stat->id }}"
                                                    <?php if($stat->id == $row->status_id ) { echo "selected" ;} ?>
                                                    >{{ $stat->keterangan }}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class='form-group'>
                                                <label for="alasan" class="col-sm-2 control-label">Alasan</label>
                                                <div class="col-sm-10">
                                                <textarea class="form-control" rows="5" id="alasan" name='alasan'
                                                @if(empty($row->alasan))
                                                readonly
                                                @endif
                                                ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            
                        </div>
                        <div class="tab-pane" id="tab_2-2">
                        <table class='table table-striped table-bordered'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>NIP</th>
                                    <th>Instansi / Jabatan</th>
                                    <th>Golongan</th>
                                    <th>Lama (Hari)</th>
                                    <th>Pesawat</th>
                                    <th>Taksi Provinsi</th>
                                    <th>Taksi Kabupaten</th>
                                    <th>Uang Harian</th>
                                    <th>Penginapan</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(Count($result))
                                @foreach($result as $key => $row)
                                @php
                                    $totalpesawat   = $totalpesawat + $row->tiket_pesawat;
                                    $totaltaksiprov = $totaltaksiprov + $row->taksi_provinsi;
                                    $totaltaksikab  = $totaltaksikab + $row->taksi_kabupaten;
                                    $totaluh        = $totaluh + $row->uang_harian;
                                    $totalinap      = $totalinap + $row->penginapan;
                                    $total          = $totalinap + $totalpesawat + $totaltaksikab + $totaltaksiprov + $totaluh;
                                @endphp
                                <tr>
                                <td>{{$key + 1}}</td>
                                    <td>{{$row->nama_peserta}}</td>
                                    <td>{{$row->nip}}</td>
                                    <td>{{$row->instansi}}</td>
                                    <td>{{$row->golongan}}</td>
                                    <td>{{$row->lama}}</td>
                                    <td class='text-right'>{{ number_format($row->tiket_pesawat , 0 , ',' , '.' ) }}</td>
                                    <td class='text-right'>{{ number_format($row->taksi_provinsi , 0 , ',' , '.' ) }}</td>
                                    <td class='text-right'>{{ number_format($row->taksi_kabupaten , 0 , ',' , '.' ) }}</td>
                                    <td class='text-right'>{{ number_format($row->uang_harian , 0 , ',' , '.' ) }}</td>
                                    <td class='text-right'>{{ number_format($row->penginapan , 0 , ',' , '.' ) }}</td>
                                    <td class='text-right'>
                                        <label for="total">{{ number_format($row->tiket_pesawat + $row->taksi_provinsi + $row->taksi_kabupaten + $row->uang_harian + $row->penginapan , 0 , ',' , '.' ) }}</label>
                                    </td>
                                    
                                </tr>
                                @endforeach
                                <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class='text-right'><strong>{{ number_format($totalpesawat , 0 , ',' , '.')}}</strong></td>
                                <td class='text-right'><strong>{{ number_format($totaltaksiprov , 0 , ',' , '.')}}</strong></td>
                                <td class='text-right'><strong>{{ number_format($totaltaksikab , 0 , ',' , '.')}}</strong></td>
                                <td class='text-right'><strong>{{ number_format($totaluh , 0 , ',' , '.')}}</strong></td>
                                <td class='text-right'><strong>{{ number_format($totalinap , 0 , ',' , '.')}}</strong></td>
                                <td class='text-right'><strong>{{ number_format($total , 0 , ',' , '.')}}</strong></td>
                                
                                </tr>
                            @else
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                   
                                </tr>
                            @endif
                            
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        


         <div class="text-center">
            <a href="{{ url()->previous() }}" class='btn btn-default float-left'><i class='fa fa-arrow-left'></i> Back</a>
            <input type="hidden" name='previous' id='previous' value="{{ URL::previous() }}">
            @if(CRUDBooster::myPrivilegeName() == 'user' && $status->id == 1)
                <a href="{{ route('senddraft-keg' , $id)}}" class='btn btn-success float-right'><i class='fa fa-send'></i> Kirim</a>
            @endif
            @if(CRUDBooster::myPrivilegeName() == 'Bendahara' )
            <button type='submit' value='Kirim' class='btn btn-info float-right'>
                <i class='fa fa-send'> Kirim</i>
            </button>
            
          
            @endif
         </div>
         </form>
                
            
         
        <!-- etc .... -->
        </div
      
    </div>
  </div>
@endsection