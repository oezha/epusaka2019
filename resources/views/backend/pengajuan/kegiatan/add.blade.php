<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <p><a title='Return' href='{{ CRUDBooster::mainpath() }}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; Back To List Data Kegiatan</a></p>
  <div class='panel panel-default'>
    <div class='panel-heading'>Add Form</div>
    <div class='panel-body'>
    <form method='post' action='{{CRUDBooster::mainpath("add-save")}}'>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="box-body" id="parent-form-area">
        <div class='form-group header-group-0 ' id='form-group-bagian_id' style="">
            <label class='control-label col-sm-2'>Bagian
                    <span class='text-danger' title='This field is required'>*</span>
            </label>
            <div class="col-sm-10">

                <input type="text" name="nama_bagian" id="nama_bagian" required readonly class='form-control' value='{{ $current_bagian->nama }}'>
                <div class="text-danger">
                    
                </div>
                <p class='help-block'></p>
            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-rkakl_id' style="">
            <label class='control-label col-sm-2'>Nama Kegiatan
                            <span class='text-danger' title='This field is required'>*</span>
                    </label>

            <div class="col-sm-10">
                <select class='form-control' id="rkakl_id" data-value='' required    name="rkakl_id">
                    <option value=''>** Please select a Nama Kegiatan</option>
                    @foreach($nama_kegiatan as $keg)
                    <option value='{{ $keg->kode_7 }}'>{{ $keg->no_mak_7 }} | {{ $keg->uraian_kegiatan }}</option>
                    @endforeach
                            </select>
                <div class="text-danger"></div>
                <p class='help-block'></p>
            </div>
        </div>
            <div class='form-group header-group-0 ' id='form-group-jenis_belanja' style="">
                <label class='control-label col-sm-2'>Jenis Belanja
                                <span class='text-danger' title='This field is required'>*</span>
                        </label>

                <div class="col-sm-10">
                    <select class='form-control' id="jenis_belanja" data-value='' required    name="jenis_belanja">
                        <option value=''>** Please select a Jenis Belanja</option>
                        
                                </select>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>    
            <div class='form-group form-datepicker header-group-0 ' id='form-group-tgl_awal' style="">
                <label class='control-label col-sm-2'>Tgl Awal
                                <span class='text-danger' title='This field is required'>*</span>
                        </label>

                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                        <input type='text' title="Tgl Awal" readonly
                            required    class='form-control notfocus input_date' name="tgl_awal" id="tgl_awal"
                            value=''/>
                    </div>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>
            <div class='form-group form-datepicker header-group-0 ' id='form-group-tgl_akhir'
                style="">
                <label class='control-label col-sm-2'>Tgl Akhir
                                <span class='text-danger' title='This field is required'>*</span>
                        </label>

                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                        <input type='text' title="Tgl Akhir" readonly
                            required    class='form-control notfocus input_date' name="tgl_akhir" id="tgl_akhir"
                            value=''/>
                    </div>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>
            <div class='form-group header-group-0 ' id='form-group-hotel_id' style="">
                <label class='control-label col-sm-2'>Hotel
                                <span class='text-danger' title='This field is required'>*</span>
                        </label>

                <div class="col-sm-10">
                    <select style='width:100%' class='form-control' id="hotel_id"
                            required    name="hotel_id"  >
                            <option value="">**Please Select Hotel</option>
                            @foreach($hotel as $hotels)
                                <option value="{{$hotels->id}}">{{ $hotels->nama_hotel }}</option>
                            @endforeach
                    </select>
                    <div class="text-danger">
                    
                    </div><!--end-text-danger-->
                    <p class='help-block'></p>
                </div>
            </div>

            <div class='form-group header-group-0 ' id='form-group-provinsi_id' style="">
                <label class='control-label col-sm-2'>Provinsi
                                <span class='text-danger' title='This field is required'>*</span>
                        </label>

                <div class="col-sm-10">
                    <select style='width:100%' class='form-control' id="provinsi_id"
                            required    name="provinsi_id"  >
                    <option value="">** Please Select Provinsi</option>
                    @foreach($provinsi as $provinsis)
                    <option value="{{ $provinsis->id }}">{{ $provinsis->title }}</option>
                    @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class='panel-footer text-center'>
    <a href='{{ CRUDBooster::mainpath() }}' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> Back</a>
    <input type="hidden" name="bagian_id" id="bagian_id" required readonly class='form-control' value='{{ $current_bagian->id }}'>
      <input type='submit' class='btn btn-success' value='Save changes'/>
    </div>
    </form>
  </div>
@endsection