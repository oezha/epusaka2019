<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
<!--   <p><a title='Return' href='http://127.0.0.1:8000/admin/perjadin'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; Back To List Data Kegiatan</a></p> -->
  <div class='panel panel-default'>
    <div class='panel-heading'>Add Form</div>
    <div class='panel-body'>
    <form method='post' action='{{CRUDBooster::mainpath("add-save")}}'>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="box-body" id="parent-form-area">
        <div class='form-group header-group-0 ' id='form-group-bagian_id' style="">
            <label class='control-label col-sm-2'>Bagian
                    <span class='text-danger' title='This field is required'>*</span>
            </label>
            <div class="col-sm-10">
                <input type="hidden" name="bagian_id" id="bagian_id" required readonly class='form-control'>
                <input type="text" name="bagian_name" id="bagian_name" required readonly class='form-control' value = '{{ $bagian_name }}'>
                <div class="text-danger">
                </div><!--end-text-danger-->
                <p class='help-block'></p>
            </div>
        </div>
        
        <div class='form-group header-group-0 ' id='form-group-kode_7' style="">
            <label class='control-label col-sm-2'>Nama Kegiatan 
                <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                    <select class='form-control' id="id_rkakl" data-value='' required    name="id_rkakl">
                        <option value=''>** Please select a Nama Kegiatan</option>
                       @foreach($nomak as $key => $value)
                       <option value="{{ $value['id'] }}">{{ $value['no_mak'] }}-{{ $value['uraian'] }}</option>
                       @endforeach
                    </select>
                <div class="text-danger"></div>
                <p class='help-block'></p>
            </div>
            <div class='form-group header-group-0 ' id='form-group-no_surat_tugas' style="">
                <label class='control-label col-sm-2'>No Surat Tugas
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    <input type="text" name="no_surat_tugas" id="no_surat_tugas" title="No Surat Tugas" required class="form-control">
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>
            <div class='form-group form-datepicker header-group-0 ' id='form-group-tgl_surat_tugas' style="">
                <label class='control-label col-sm-2'>Tanggal Surat Tugas
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                        <input type='text' title="Tgl Surat Tugas" 
                            required    class='form-control notfocus input_date' name="tgl_surat_tugas" id="tgl_surat_tugas"
                            value=''/>
                    </div>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>
            <div class='form-group form-datepicker header-group-0 ' id='form-group-file' style="">
                <label class='control-label col-sm-2'>File
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    
                    <input type="file" name="file" id="file" required="" class='form-control'>
                    
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>    
            <div class='form-group form-datepicker header-group-0 ' id='form-group-tgl_awal' style="">
                <label class='control-label col-sm-2'>Tgl Awal
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                        <input type='text' title="Tgl Awal" 
                            required    class='form-control notfocus input_date' name="tgl_awal" id="tgl_awal"
                            value=''/>
                    </div>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>
            <div class='form-group form-datepicker header-group-0 ' id='form-group-tgl_akhir'
                style="">
                <label class='control-label col-sm-2'>Tgl Akhir
                                <span class='text-danger' title='This field is required'>*</span>
                        </label>

                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                        <input type='text' title="Tgl Akhir" 
                            required    class='form-control notfocus input_date' name="tgl_akhir" id="tgl_akhir"
                            value=''/>
                    </div>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>

            <div class='form-group header-group-0 ' id='form-group-proviinsi_id' style="">
                <label class='control-label col-sm-2'>Provinsi
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    <select class='form-control' id="provinsi_id" data-value='' required    name="provinsi_id">
                        <option value="">** Please Select Provinsi</option>
                        @foreach($provinsi as $provinsis)
                        <option value="{{ $provinsis->id }}">{{ $provinsis->title }}</option>
                        @endforeach
                    </select>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>

            <div class='form-group form-datepicker header-group-0 ' id='form-group-kabkota_id' style="">
                <label class='control-label col-sm-2'>KabKota
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    
                    <select class='form-control' id="kabkota_id" data-value='' required    name="kabkota_id">
                        <option value="">** Please Select Kabkota</option>
                        @foreach($kabkota as $kabkotas)
                        <option value="{{ $kabkotas->id }}">{{ $kabkotas->nama }}</option>
                        @endforeach
                    </select>
                    
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>


        </div>
    </div>
    
    
    <div class='panel-footer text-center'>
    <a href='http://127.0.0.1:8000/admin/perjadin29' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> Back</a>
      <input type='submit' class='btn btn-success' value='Save changes'/>
    </div>
    </form>
  </div>
@endsection