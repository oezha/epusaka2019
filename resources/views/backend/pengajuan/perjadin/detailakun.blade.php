@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->


  <div class="box box-default">

            <div class="box-body table-responsive no-padding">
              <table class='table table-bordered' id="tableAkun">
                <tr>
                  <td class='text-center'> <strong> Akun</strong></td>
                  <td class='text-center'> <strong> Uraian</strong></td>
                  <td class='text-center'> <strong> Jumlah Pengajuan</strong></td>
                  <td></td>
            
                </tr>
                @if(Count($row))
                @foreach($row as  $rows )
                  <tr>
                    <td>{{ $rows->akun }}</td>
                    <td>{{ $rows->uraian }}</td>
                    <td class="text-right">{{ number_format($rows->jumlah , 0 ,  "," , ".") }}</td>
                    <td>
                      <a href = "{{ route('delete-detail-akun-perjadin' , $id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                      <a href = "javascript:void(0);" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye"></i></a>
                    </td>
                  </tr>
                @endforeach
                
                <tr>
                   
                </tr>
              </table>
              @else
                <tr>
                  <td class='text-center' colspan=3><i class="fa fa-exclamation-triangle fa-2x"></i>
                            <h4 class="no-margins">Tidak ada akun yang teregistrasi!!</h4>
                            
                  <a href='/admin/perjadin/{{$id}}/pa' 
                               id='btn_add_new_data' class="btn btn-sm btn-success" title="Add Data">
                                <i class="fa fa-plus-circle"></i> Add Data
                            </a>
                  </td>
                </tr>
                <tr>
                   
                   </tr>
                 </table>
                @endif
            </div>
            
            <div class='panel-footer text-center'>
            @if(Count($row))
    <a href='#' class='btn btn-default'><i class='fa fa-chevron-circle-left'></i> Back</a>
    <a href='{{ route("nominatif-index-perjadin" , $id) }}' class='btn btn-success'>Selanjutnya<i class='fa fa-arrow-right'></i></a>
      
      @endif
    </div>
    
  </div>

  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Akun</h4>
      </div>
      <div class="modal-body">
      <table class='table table-bordered' id="tableAkun">
        <thead>
        <tr>
                  <td class='text-center'><strong>Kode</strong></td>
                  <td class='text-center'><strong>Uraian</strong></td>
                  <td class='text-center'><strong>Vol</strong></td>
                  <td class='text-center'><strong>Sat</strong></td>
                  <td class='text-center'><strong>Harga Satuan</strong></td>
                  <td class='text-center'><strong>Pagu</strong></td>
                  <td class='text-center'><strong>Sisa Pagu</strong></td>
                  <td class='text-center'><strong>Jumlah Pengajuan</strong></td>
                  <td class='text-center'><strong>Sisa Anggaran</strong></td>
                </tr>
        </thead>
        <tbody>
        <?php
        
        $total_pengajuan = 0;

        ?>



          @foreach($detail_akun as $key => $value)
          <tr>
            <td>{{ $value->kode }}</td>
            <td>{{ $value->uraian }}</td>
            <td>{{ $value->vol }}</td>
            <td>{{ $value->sat }}</td>
            <td class='text-right'>{{ number_format($value->hargasat , 0 , "." , ",") }}</td>
            <td class='text-right'>{{ number_format($value->jumlah , 0 , "." , ",") }}</td>
            <td class='text-right'>{{ number_format($value->sisa_pagu_sblm_pengajuan , 0 , "." , ",") }}</td>
            <td class='text-right'>{{ number_format($value->jumlah_pengajuan , 0 , "." , ",") }}</td>
            <td class='text-right'>{{ number_format($value->sisa_pagu_sblm_pengajuan - $value->jumlah_pengajuan , 0 , "." , ",") }}</td>
          </tr>

          <?php
        
        $total_pengajuan = $total_pengajuan + $value->jumlah_pengajuan;

        ?>




          @endforeach
          <tr>
            <td colspan='7'><strong>TOTAL</strong></td>
            
            <td class='text-right'>{{ number_format($total_pengajuan, 0 , "," , ".") }}</td>
            <td></td>
          </tr>
        </tbody>
      </table>
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection