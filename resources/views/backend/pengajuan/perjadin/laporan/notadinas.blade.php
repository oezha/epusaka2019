<html>

<title>NOTA DINAS</title>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style type="text/css">
hr
{
	border:0;
	border-top:1px solid black !important;
	margin:20px 0;
}
.w3-table
{
	font-size: 10px !important;
}
.w3-table td:{
	padding-top:0px;
}
.table { 
border-spacing: 0;
border-collapse: collapse;
}
/*.w3-table td
{
	text-align: left !important;
}*/
</style>

</head>
<body>
<br>
<br>
<br>
<br>
<div class="w3-center w3-tiny">
	<br>
	<br>
	<h6><b><u>NOTA DINAS</u></b></h6>
</div>

<div class="w3-tiny">

<table class="w3-tiny" border="0" width="100%">
	<tr>
		<td width="5%">
			<label>Nomor</label>
		</td>
		<td><label>:</label> {{ $no_pengajuan }}</td>
		<td class="w3-right-align">
		<label>{{ date('d M Y', strtotime($tanggalprint)) }}</label>
		</td>
	</tr>
	<tr>
		<td width="5%">
			<label>Lampiran</label>
		</td>
		<td colspan="2"><label>: 1 (satu) Berkas</label></td>
	</tr>
	<tr>
		<td width="5%" style="vertical-align: top;">
			<label>Hal</label>
		</td>
		<td colspan="2">
		
			<label>: Permohonan Pelaksanaan Perjalanan Dinas Dalam Negeri</label><br>
			
			<b>&nbsp;&nbsp;{{$nama_kegiatan}}</b><br>
		</td>
	</tr>
</table>
<br>
<div class="w3-tiny">
	<label>Yang Terhormat,</label>
</div>
<div class="w3-tiny">
	<label>Direktur Produksi dan Distribusi Kefarmasian</label>
</div>
<div class="w3-tiny">
	<label>di-</label>
</div>
<div class="w3-tiny">
	&nbsp;&nbsp;&nbsp;<label>Jakarta</label>
</div>
<div class="w3-tiny">

	<p align="justify">
		Sehubungan {{ $catatan }} dilaksankannya Perjalanan Dinas Dalam Negeri (MAK {{$no_mak}}) tanggal {{ date('d', strtotime($tgl_awal)) }} s.d {{ date('d M Y', strtotime($tgl_akhir)) }} di {{ $provinsi }} ,
		maka kami mengajukan rencana pembiayaan untuk Perjalanan Dinas tersebut sebagai berikut:
	</p>

</div>
<div class="w3-tiny">
	A. Permintaan Sekarang
	<table border="1" cellpadding="5" cellspacing="0" width="100%">
		<thead>
			<tr class="w3-center">
				<th>Akun</th>
				<th>Rincian</th>
				<th>Sisa Pagu</th>
				<th>Jumlah Penarikan</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$total = 0;
		?>
			@foreach($detail as $key => $data)
			<tr>
				<td class='w3-center' width='auto'>{{$data->akun}}</td>
				<td class='w3-left-align' width='auto'>{{$data->uraian}}</td>
				<td class='w3-right-align' width='auto'>{{number_format($data->sisa_pagu_sblm_pengajuan,0,',','.') }}</td>
				<td class='w3-right-align' width='auto'>{{ number_format($data->jumlah_pengajuan,0,',','.')}}</td>
			</tr>
			<?php
			$total = $total + $data->jumlah_pengajuan;
			?>
			@endforeach
			<tr>
				<td colspan="3" class="w3-right-align">
				<b>TOTAL</b>
				</td>
				<td class="w3-right-align">
				<b>{{ number_format($total , 0 , ',' , '.') }}</b>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="w3-tiny">
B. Petugas yang ditunjuk untuk melaksanakan Perjalanan Dinas terlampir.<br>
Demikian Permohonan ini kami sampaikan, atas perhatian ibu kami ucapkan termakasih
</div>

<div class="w3-responsive w3-tiny">
	<table class="w3-table">
		<thead>
			<tr>
				<td>
					Setuju dibebankan pada mata anggaran berkenaan,
					<br>
					<strong>Pejabat Pembuat Komitmen</strong></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>
					<br>
					<strong>{{ $jabatan_pimpinan }}</strong></td>
			</tr>
		</thead>
		<tbody>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>						
			<tr>
				<td>&nbsp;&nbsp;<b>{{ $ppk_nama }}</b>
				<br>
				&nbsp;&nbsp;NIP. {{ $ppk_nip }}
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td><b>{{ $pimpinan }}</b>
					<br>
				NIP.{{ $nip_pimpinan }}</td>
			</tr>
			<tr>
				<td></td>
			</tr>
		</tbody>
	</table>
</div>
	
</div>

</body>
</html> 
