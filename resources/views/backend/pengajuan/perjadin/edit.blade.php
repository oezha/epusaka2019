<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>Edit Form</div>
    <div class='panel-body'>
    <form method='post' action='{{CRUDBooster::mainpath("edit-save")}}/{{$id}}'>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="box-body" id="parent-form-area">
        <div class='form-group header-group-0 ' id='form-group-bagian_id' style="">
            <label class='control-label col-sm-2'>Bagian
                    <span class='text-danger' title='This field is required'>*</span>
            </label>
            <div class="col-sm-10">
                <input type="text" name="bagian_id" id="bagian_id" required readonly class='form-control' value="{{$perjadin->bagian_id}}">
                <div class="text-danger">
                    
                </div><!--end-text-danger-->
                <p class='help-block'></p>
            </div>
        </div>
        <div class='form-group header-group-0 ' id='form-group-kode_7' style="">
            <label class='control-label col-sm-2'>Nama Kegiatan
                    <span class='text-danger' title='This field is required'>*</span>
            </label>

            <div class="col-sm-10">
                <select class='form-control' id="id_rkakl" data-value='' required name="id_rkakl" disabled>
                    <option value='{{ $perjadin->no_mak }}'>{{ $perjadin->nama_kegiatan }}</option>
                </select>
                <div class="text-danger"></div>
                <p class='help-block'></p>
            </div>
            <div class='form-group header-group-0 ' id='form-group-no_surat_tugas' style="">
                <label class='control-label col-sm-2'>No Surat Tugas
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    <input type="text" name="no_surat_tugas" id="no_surat_tugas" title="No Surat Tugas" required class="form-control" value="{{$perjadin->no_surat_tugas}}" >
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>
            <div class='form-group form-datepicker header-group-0 ' id='form-group-tgl_surat_tugas' style="">
                <label class='control-label col-sm-2'>Tanggal Surat Tugas
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                        <input type='text' title="Tgl Surat Tugas" 
                            required    class='form-control notfocus input_date' name="tgl_surat_tugas" id="tgl_surat_tugas"
                            value='{{$perjadin->tgl_surat_tugas}}'/>
                    </div>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div> 
            <div class='form-group form-datepicker header-group-0 ' id='form-group-file' style="">
                <label class='control-label col-sm-2'>File
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    <input type="file" name="file" id="file" required="" class="form-control" value="{{$perjadin->file}}">{{$perjadin->file}}
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div> 
            <div class='form-group form-datepicker header-group-0 ' id='form-group-tgl_awal' style="">
                <label class='control-label col-sm-2'>Tgl Awal
                        <span class='text-danger' title='This field is required'>*</span>
                </label>

                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                        <input type='text' title="Tgl Awal" readonly
                            required    class='form-control notfocus input_date' name="tgl_awal" id="tgl_awal"
                            value='{{ $perjadin->tgl_awal }}'/>
                    </div>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>
            <div class='form-group form-datepicker header-group-0 ' id='form-group-tgl_akhir'
                style="">
                <label class='control-label col-sm-2'>Tgl Akhir
                    <span class='text-danger' title='This field is required'>*</span>
                </label>

                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                        <input type='text' title="Tgl Akhir" readonly
                            required    class='form-control notfocus input_date' name="tgl_akhir" id="tgl_akhir"
                            value='{{ $perjadin->tgl_akhir }}'/>
                    </div>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>           

            <div class='form-group form-datepicker header-group-0 ' id='form-group-provinsi_id'
                style="">
                <label class='control-label col-sm-2'>Provinsi
                    <span class='text-danger' title='This field is required'>*</span>
                </label>

                <div class="col-sm-10">
                    <select style='width:100%' class='form-control' id="provinsi_id"
                            required    name="provinsi_id" disabled >
                    <option value="{{$provinsi->id}}">{{ $provinsi->title }}</option>
                   
                    </select>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>
            <div class='form-group form-datepicker header-group-0 ' id='form-group-kabkota_id' style="">
                <label class='control-label col-sm-2'>KabKota
                    <span class='text-danger' title='This field is required'>*</span>
                </label>

                <div class="col-sm-10">
                    <select class='form-control' id="kabkota_id" data-value='' required    name="kabkota_id" disabled>
                        <option value="{{ $kabkota->id }}">{{ $kabkota->nama }}</option>   
                    </select>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>

            <div class='form-group header-group-0 ' id='form-group-lama' style="" >
                <label class='control-label col-sm-2'>lama
                    <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">
                    <input type="text" name="lama" id="lama" title="lama" required class="form-control" disabled value="{{$perjadin->lama}}">
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>
        </div>
        </div>
    </div>
    
    
    <div class='panel-footer text-center'>
    <a href='http://127.0.0.1:8000/admin/perjadin29' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> Back</a>
      <input type='submit' class='btn btn-success' value='Save changes'/>
    </div>
    </form>
  </div>
@endsection