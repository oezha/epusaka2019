@extends('crudbooster::admin_template')
@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        Import RKAKL
    </div>
    <form method='POST' id='form' name='form' action='{{ route("UploadRkakl") }}' enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="panel-body">
    <div class='form-group header-group-0 ' id='form-group-bagian_id' style="">
            <label class='control-label col-sm-2'>Keterangan
                    <span class='text-danger' title='This field is required'>*</span>
            </label>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-md-4">
                    <input type="radio" name="uploadke" id="uploadke" value='0' required> POK AWAL
                    </div>
                    <div class="col-md-4">
                    <input type="radio" name="uploadke" id="uploadke" value='1' required> Revisi
                    </div>
                </div>
                
                
                   
                <div class="text-danger">
                    
                </div>
                <p class='help-block'></p>
            </div>
        
    </div>
    <div class='form-group header-group-0 ' id='form-group-revisike' style="display:none">
            <label class='control-label col-sm-2'>Revisi Ke
                    <span class='text-danger' title='This field is required'>*</span>
            </label>
            <div class="col-sm-10">
               <input type="number" name="revisike" id="revisike" value="{{ $revisike }}">    
                <div class="text-danger">
                    
                </div>
                <p class='help-block'></p>
            </div>
        
    </div>
    <div class='form-group header-group-0 ' id='form-group-bagian_id' style="">
            <label class='control-label col-sm-2'>File Excel
                    <span class='text-danger' title='This field is required'>*</span>
            </label>
            <div class="col-sm-10">

            <input name="file" id="file" type="file" class="form-control" required><br/>
                   
                <div class="text-danger">
                    
                </div>
                <p class='help-block'></p>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <input type="submit" value="Upload" class="btn btn-success">
        <a href="{{ CRUDBooster::mainpath() }}" class='btn btn-default'>Back</a>
    </div>
    </form>
</div>

@endsection