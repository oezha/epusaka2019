@extends('crudbooster::admin_template')
@section('content')
<ul class="nav nav-tabs success">
  <li class="active"><a data-toggle="tab" href="#1">Data RKAKL</a></li>
  <li><a data-toggle="tab" href="#2">Analisis Semula Menjadi</a></li>
  <li><a data-toggle="tab" href="#3">Analisis Pagu Minus</a></li>
</ul>

<div class="tab-content">
  <div id="1" class="tab-pane fade in active">
  @include('backend.referensi.rkakl.form_data_rkakl')
  </div>
  <div id="2" class="tab-pane fade">
  <br>
    @include('backend.referensi.rkakl.analisis_sm')
  </div>
  <div id="3" class="tab-pane fade">
    <br>
    @include('backend.referensi.rkakl.analisa_pagu_minus')
  </div>
</div>
@endsection