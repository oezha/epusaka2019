<div class="panel panel-success" id='form-master'>
    <div class="panel-heading">
        <h4>Semula Menjadi</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body" id='keterangan1'>
                        <div class='form-group header-group-0 row' id='form-group-revisike'>
                            <label class='control-label col-md-2 small'>Ket. Data</label>
                            <div class="col-md-4">
                                <input type="number" name="semula" id="semula" class="form-control text-center" min=0  max="{{ $menjadi - 1 }}" value="{{ $semula }}" onKeyDown="return false">    
                                    <div class="text-danger">
                                        
                                    </div>
                                    <p class='help-block'></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <a href="javascript:void(0)" class="btn btn-md btn-primary" onclick = "return generate_sm();">
                            <i class='fa fa-refresh'></i> Generate
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body" id='keterangan2'>
                        <div class='form-group header-group-0 row' id='form-group-revisike'>
                            <label class='control-label col-md-2 small'>Ket. Data</label>
                            <div class="col-md-4">
                                <input type="number" name="nilai_menjadi" id="nilai_menjadi" class="form-control text-center" min=0  max="{{ $menjadi }}" value="{{ $menjadi }}" readonly>    
                                    <div class="text-danger">
                                        
                                    </div>
                                    <p class='help-block'></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Semula
                    </div>
                    <div class="panel-body table-responsive small">
                        <table class="table table-hover table-striped table-bordered" id="table_semula">
                            <thead>
                                <tr>
                                    <th class="text-center">Kode</th>
                                    <th class="text-center">Uraian</th>
                                    <th class="text-center">Vol</th>
                                    <th class="text-center">Sat</th>
                                    <th class="text-center">HargaSat</th>
                                    <th class="text-center">Jumlah</th>
                                    <th class="text-center">kdBlokir</th>
                                    <th class="text-center">Sdana</th>
                                </tr>
                            </thead>
                            <tbody id = "table_semula_body">
                           
                            </tbody> 
                        </table>
                    </div>
                </div>

               
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Menjadi
                    </div>
                    <div class="panel-body table-responsive small">
                        <table class="table table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">Kode</th>
                                    <th class="text-center">Uraian</th>
                                    <th class="text-center">Vol</th>
                                    <th class="text-center">Sat</th>
                                    <th class="text-center">HargaSat</th>
                                    <th class="text-center">Jumlah</th>
                                    <th class="text-center">kdBlokir</th>
                                    <th class="text-center">Sdana</th>
                                </tr>
                            </thead>
                            <tbody id="table_menjadi_body">
                            
                            </tbody> 
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>

