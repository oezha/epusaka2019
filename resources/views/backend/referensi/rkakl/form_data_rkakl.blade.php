<br>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Data Upload Rkakl</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Uraian</th>
                    <th class="text-center">Vol</th>
                    <th class="text-center">Sat</th>
                    <th class="text-center">HargaSat</th>
                    <th class="text-center">Jumlah</th>
                    <th class="text-center">kdBlokir</th>
                    <th class="text-center">Sdana</th>
                </tr>
            </thead>
            <tbody>
            @foreach($upload_rkakl as $key => $value)
                <tr>
                    <td>{{ $value->kode }}</td>
                    <td>{{ $value->uraian }}</td>
                    <td>{{ $value->vol }}</td>
                    <td>{{ $value->sat }}</td>
                    <td class="text-right">{{ number_format($value->hargasat , 0 , "," , ".") }}</td>
                    <td class="text-right">{{ number_format($value->jumlah , 0 , "," , ".") }}</td>
                    <td>{{ $value->kdblokir }}</td>
                    <td>{{ $value->sdana }}</td>
                </tr>
            @endforeach
            </tbody>
            
        </table>
    </div>
    <div class="panel-footer">
    <p>{!! urldecode(str_replace("/?","?",$upload_rkakl->appends(Request::all())->render())) !!}</p>
    </div>
</div>