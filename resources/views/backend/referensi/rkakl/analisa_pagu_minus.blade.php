<div class="panel panel-success">
    <div class="panel-heading">
        
        <div class='panel-title pull-left'><h4>Pagu Minus</h4></div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">
                <div class='panel panel-default'>
                    <div class="panel-body">
                            <div class='form-group header-group-0' id='form-group-revisike'>
                                <label class='control-label col-sm-2 text-right'>Data Rkakl Ke :</label>
                                <div class="col-md-2">
                                    <input type="number" name="semula" id="semula" class="form-control text-center" min='{{ $menjadi }}'  max="{{ $menjadi }}" value="{{ $menjadi }}" onKeyDown="return false">    
                                        <div class="text-danger">
                                            
                                        </div>
                                        <p class='help-block'></p>
                                </div>
                                <a href="javascript:void(0)" class="btn btn-primary btn-md"> <i class="fa fa-spinner"></i> Reload </a>
                            </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-striped table-bordered" id="table_semula">
                    <thead>
                        <tr>
                            <th class="text-center">Kode</th>
                            <th class="text-center">Uraian</th>
                            <th class="text-center">Vol</th>
                            <th class="text-center">Sat</th>
                            <th class="text-center">HargaSat</th>
                            <th class="text-center">Jumlah</th>
                            <th class="text-center">kdBlokir</th>
                            <th class="text-center">Sdana</th>
                        </tr>
                    </thead>
                    <tbody id = "table_pagu">
                    
                    </tbody> 
                </table>
            </div>
        </div>

        
    </div>
    
</div>