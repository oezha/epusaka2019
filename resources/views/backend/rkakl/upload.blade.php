@extends('crudbooster::admin_template')
@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">
        <h5>Upload File Rkakl</h5>
    </div>
    <form method='post' action="/admin/rkakl/upload" enctype="multipart/form-data" class='form-horizontal'>
    {{ csrf_field() }}
        <div class="panel-body">
        <div class='form-group header-group-0 ' id='form-group-bagian_id' style="">
                <label class='control-label col-sm-2'>File Excel
                        <span class='text-danger' title='This field is required'>*</span>
                </label>
                <div class="col-sm-10">

                    <input type="file" name="file" id="file" class='form-control'>
                    <div class="text-danger">
                        
                    </div>
                    <p class='help-block'></p>
                </div>
            </div>
        </div>
        <div class="panel-footer text-center">
            <input type="submit" value="Upload" class='btn btn-primary'>
        </div>
        
    </form>
</div>


@endsection()