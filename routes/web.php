<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin');
});

Route::get('/admin' , 'DashboardsController@getindex')->name('DashboardsControllerGetIndex');

Route::group(['prefix' => '/rkakl'], function(){
    Route::get('/load-rkakl' , 'RkaklController@load_rkakl')->name('load-rkakl');
});

    Route::get('admin/m_rpk/memuat_output' , 'RpkController@memuat_output');
    Route::get('admin/m_rpk/memuat_suboutput/{id?}' , 'RpkController@memuat_suboutput')->name('memuat_suboutput');
    Route::get('admin/m_rpk/memuat_komponen/{id?}' , 'RpkController@memuat_komponen')->name('memuat_komponen');
    Route::get('admin/m_rpk/memuat_uraian/{id?}' , 'RpkController@memuat_uraian')->name('memuat_uraian');
    Route::get('admin/m_rpk/memuat_data/{id}' , 'RpkController@memuat_data')->name('memuat_data');

    // START KEGIATAN //
    Route::get('admin/keg/memuat_nomak' , 'AdminKegController@memuat_nomak');
    Route::get('admin/keg/memuat_jenis_belanja/{id}' , 'KegiatanController@memuat_jenis_belanja');
    Route::get('/admin/keg/{id}/pa' , 'AdminPilihAkunController@pengajuan_kegiatan')->name('keg-pa');
    Route::post('/admin/keg/{id}/pa' , 'PaController@store')->name('simpan-pa');
    Route::get('/admin/keg/{id}/dakun' , 'AdminKegController@detail_akun');
    Route::get('/admin/keg/{id}/dakun/destroy' , 'dakunController@destroy')->name('delete-detail-akun-keg');
    Route::get('/admin/keg/{id}/nominatif' , 'AdminNomController@getIndex')->name('nominatif-index');
    Route::get('/admin/keg/{id}/nom/add' , 'AdminNomController@getAdd')->name('nominatif-add');
    Route::get('/admin/keg/{id}/nom/addguest' , 'AdminNomController@AddGuest')->name('nominatif-addGuest');
    Route::get('/admin/keg/{id}/draft' , 'AdminKegController@getDraft')->name('draft-keg');
    Route::get('/admin/keg/{id}/delete' , 'AdminKegController@getDelete')->name('delete-keg');
    Route::get('/admin/pjkeg/{id}/draft' , 'AdminPjKegController@getDraft')->name('pjdraft-keg');
    Route::get('/nominatif/peg/{id}' , 'AdminNomController@getDataPegawai')->name('getpegawai');
    Route::get('/nominatif/tamu/{id}' , 'AdminNomController@getDataTamu')->name('getTamu');
    Route::post('/nominatif/peg/{id}/add' , 'NominatifController@store')->name('simpan-nom');
    Route::post('/nominatif/tamu/{id}/add' , 'NominatifController@store_guest')->name('simpan-guest');
    Route::get('/nominatif/peg/{id}/destroy' , 'NominatifController@destroy')->name('delete-nominatif');
    Route::get('/admin/keg/{id}/edit' , 'AdminKegController@getEdit')->name('edit-data-keg');
    Route::get('/admin/keg/{id}/sendDraft' , 'AdminKegController@senddraft')->name('senddraft-keg');
    Route::post('/admin/keg/{id}/sendBend' , 'AdminKegController@sendBend')->name('sendBend-keg');
    Route::get('/admin/keg/{id}/notadinas' , 'AdminKegController@print_notadinas');
    Route::get('/admin/keg/getbagian', 'AdminKegController@getbagian')->name('getbagian');
    Route::post('/admin/keg/{id}/editsave', 'KegiatanController@edit_save')->name('edit-save-keg');
    Route::get('/admin/keg/{id}/nominatif/reset' , 'NominatifController@reset_kegiatan')->name('reset-nom-keg');
    //excel kegiatan
    Route::get('/admin/keg/{id}/download' , 'NominatifController@download_excel_kegiatan')->name('download-excel-keg');
    Route::post('/admin/keg/{id}/import' , 'NominatifController@import_excel_kegiatan')->name('import-excel-keg');

    // pertanggungjawaban
    Route::get('/admin/pjkeg/{id}/draft' , 'AdminPjkegController@getDraft')->name('draft-pjkeg');
    Route::post('/admin/pjkeg/{id}/send' , 'AdminPjkegController@send')->name('send-pj');
    Route::post('/admin/pjkeg/{id}/sendbend' , 'AdminPjkegController@sendbend')->name('sendbend-pj');
    Route::get('/admin/pjkeg/{id}/print_tandaterima' , 'AdminPjkegController@print_tanda_terima')->name('print-tanda-terima-keg');



    // END KEGIATAN //
    
    // START HONOR //
    Route::post('/admin/hn/{id}/import' , 'PnhnController@import_excel')->name('import-excel-honor');
    Route::get('/admin/hn/getnomak' , 'AdminHonorController@getnomak')->name('getnomak-honor');
    Route::get('/admin/hn/fillnamakegiatan/{id}' , 'AdminHonorController@fillnamakegiatan')->name('fillnamakegiatan-honor');
    Route::get('/admin/hn/{id}/dakun' , 'AdminHonorController@detail_akun');
    Route::get('/admin/hn/{id}/pa' , 'AdminPilihAkunController@pengajuan_honor')->name('hn-pa');
    Route::post('/admin/hn/{id}/pa' , 'PaController@store_honor')->name('simpan-hn-pa');
    Route::get('/admin/hn/{id}/delete' , 'AdminHonorController@getDelete')->name('delete-honor');
    Route::get('/admin/hn/{id}/dakun/destroy' , 'dakunController@destroy_hn')->name('delete-detail-akun-hn');
    Route::get('/admin/hn/{id}/nominatif' , 'AdminPnhnController@list_nominatif_honor')->name('nominatif-hn-index');
    Route::get('/admin/hn/{id}/nom/add' , 'AdminPnhnController@getAdd')->name('nominatif-add');
    Route::get('/admin/hn/{id}/nom/addguest' , 'AdminPnhnController@AddGuest')->name('nominatif-addGuest');
    Route::post('/admin/hn/nom/{id}/add' , 'PnhnController@store')->name('simpan-hn-nom');
    Route::get('/admin/hn/{id}/destroy' , 'PnhnController@destroy')->name('delete-hn-nominatif');
    Route::get('/admin/hn/{id}/edit' , 'AdminHonorController@getEdit');
    Route::get('/admin/hn/{id}/draft' , 'AdminHonorController@getDraft')->name('draft-hn');
    Route::get('/admin/hn/{id}/sendDraft' , 'AdminHonorController@senddraft')->name('senddraft-hn');
    Route::post('/admin/hn/{id}/sendBend' , 'AdminHonorController@sendBend')->name('sendBend-hn');
    Route::get('/admin/hn/{id}/download_nom' , 'AdminPnhnController@download_honor')->name('download-excel-honor');
    Route::get('/admin/hn/{id}/reset' , 'PnhnController@Hapus_Semua')->name('reset-penerima');
    Route::get('/admin/hn/{id}/notadinas' , 'AdminHonorController@print_notadinas')->name('hn-print-notadinas');
    Route::get('/admin/hn/{id}/list_penerima' , 'AdminHonorController@print_list_penerima')->name('hn-list-penerima');
    // END HONOR //

    // START PERJADIN //
    Route::get('/admin/perjadin/{id}/sendDraft' , 'AdminPerjadin29Controller@senddraft')->name('senddraft-perjadin');
    Route::post('/admin/perjadin/{id}/sendBend' , 'AdminPerjadin29Controller@sendBend')->name('sendBend-perjadin');
    Route::get('/admin/perjadin/{id}/notadinas' , 'AdminPerjadin29Controller@print_notadinas');
    Route::get('/admin/perjadin/getbagian', 'AdminPerjadin29Controller@getbagian')->name('getbagian');
    Route::get('/admin/perjadin/getkegiatan', 'AdminPerjadin29Controller@getkegiatan')->name('getkegiatan');
    Route::get('/admin/perjadin/memuat_nomak' , 'PerjadinController@memuat_nomak');
    Route::get('/admin/perjadin/{id}/dakun' , 'AdminPerjadin29Controller@detail_akun');
    Route::get('/admin/perjadin/{id}/delete' , 'AdminPerjadin29Controller@getDelete')->name('delete-perjadin');
    Route::get('/admin/perjadin/{id}/dakun/destroy' , 'dakunController@destroy_perjadin')->name('delete-detail-akun-perjadin');
    Route::get('/admin/perjadin/{id}/pa' , 'AdminPilihAkunController@pengajuan_perjadin')->name('perjadin-pa');
    Route::post('/admin/perjadin/{id}/pa' , 'PaController@store_perjadin')->name('simpan-perjadin');
    Route::get('/admin/perjadin/{id}/nominatif' , 'AdminNomController@getIndexPerjadin')->name('nominatif-index-perjadin');
    Route::get('/admin/perjadin/{id}/nom/add' , 'AdminNomController@getAddPerjadin')->name('nominatif-add-perjadin');
    Route::post('/nominatif/perjadin/{id}/add' , 'NominatifController@store_perjadin')->name('simpan-nom-perjadin');
    Route::get('/admin/perjadin/{id}/draft' , 'AdminPerjadin29Controller@getDraft')->name('draft-perjadin');
    Route::get('/admin/perjadin/{id}/nom/addguest' , 'AdminNomController@AddGuestPerjadin')->name('nominatif-addGuest-perjadin');
    Route::get('/admin/perjadin/{id}/editdata' , 'AdminPerjadin29Controller@getEdit')->name('edit-data-perjadin');
    Route::get('/admin/perjadin/{id}/getkabkota' , 'AdminPerjadin29Controller@getkabkota')->name('perjadin-getkabkota');
    Route::get('/admin/perjadin/{id}/destroy' , 'NominatifController@destroy_perjadin')->name('delete-perjadin-nominatif');
    Route::get('/admin/perjadin/{id}/download' , 'AdminNomController@download_perjadin')->name('download-excel-perjadin');
    Route::post('/admin/perjadin/{id}/import' , 'NominatifController@import_perjadin')->name('import-excel-perjadin');
    Route::get('/admin/perjadin/{id}/reset' , 'AdminNomController@reset_nominatid_perjadin')->name('reset-nom-perjadin');
    Route::get('/admin/perjadin/{id}/download_nom' , 'AdminNomController@download_nominatif')->name('download-nom-perjadin');
    Route::get('/admin/perjadin/{id}/sendDraft' , 'AdminPerjadin29Controller@senddraft')->name('senddraft-perjadin');
    Route::get('/admin/perjadin/{id}/tandaterima' , 'AdminNomController@download_tandaterima')->name('tandaterima-excel-perjadin');


    // Pertanggungjawaban perjadin
    
    Route::get('/admin/pjperjadin35/{id}/draft' , 'AdminPjperjadin35Controller@getDraft')->name('draft-pjperjadin');
    Route::post('/admin/pjperjadin35/{id}/send' , 'AdminPjperjadin35Controller@send')->name('send-pj-perjadin');
    Route::post('/admin/pjperjadin35/{id}/sendbend' , 'AdminPjperjadin35Controller@sendbend')->name('sendbend-pj-perjadin');
    Route::get('/admin/pjperjadin35/{id}/print_tandaterima' , 'AdminPjperjadin35Controller@print_tanda_terima')->name('print-tanda-terima');

    // END

	// END PERJADIN //
    
	//  PERKANTORAN

    Route::get('/admin/perkantoran/getbagian', 'AdminPerkantoranController@getbagian')->name('getbagian');
    Route::get('/admin/perkantoran/getkegiatan', 'AdminPerkantoranController@getkegiatan')->name('getkegiatan');
    Route::get('/admin/perkantoran/geturaian', 'AdminPerkantoranController@geturaian')->name('geturaian');
    Route::get('/admin/perkantoran/{id}/dakun' , 'AdminPerkantoranController@detail_akun');
    Route::get('/admin/perkantoran/{id}/pa' , 'AdminPilihAkunController@pengajuan_perkantoran')->name('perkantoran-pa');
    Route::post('/admin/perkantoran/{id}/pa' , 'PaController@store_perkantoran')->name('simpan-perkantoran');
    Route::get('/admin/perkantoran/{id}/editdata' , 'AdminPerkantoranController@getEdit')->name('edit-data-perkantoran');
    Route::get('/admin/perkantoran/memuat_nomak_perkantoran' , 'AdminPerkantoranController@memuat_nomak_perkantoran');
    Route::get('/admin/perkantoran/{id}/draft' , 'AdminPerkantoranController@getDraft')->name('draft-perkantoran');
    Route::get('/admin/perkantoran/{id}/sendDraft' , 'AdminPerkantoranController@senddraft')->name('senddraft-perkantoran');
    Route::get('/admin/perkantoran/{id}/dakun/destroy' , 'dakunController@destroy_perkantoran')->name('delete-detail-akun-perkantoran');
    Route::get('/admin/perkantoran/{id}/notadinas' , 'AdminPerkantoranController@print_notadinas');
    Route::get('/admin/perkantoran/{id}/edit' , 'AdminPerkantoranController@getEdit');

    
    

    Route::get('/admin/update_m_rpk' , 'RkaklController@update_kode_7_m_rpk');
    Route::get('/admin/refresh_rkakl_id_perkantoran' , 'AdminPerkantoranController@refresh_rkakl_id_perkantoran');
    Route::get('/admin/cek_detail_rpd' , 'RkaklController@cek_detail_rpd');

    Route::post('/admin/perkantoran/{id}/sendBend' , 'AdminPerkantoranController@sendBend')->name('sendBend-perkantoran');


    // Pertanggungjawaban perkantoran
    
    Route::get('/admin/pjperkantoran/{id}/draft' , 'AdminPjperkantoranController@getDraft')->name('draft-pjperkantoran');
    Route::post('/admin/pjperkantoran/{id}/send' , 'AdminPjperkantoranController@send')->name('send-pj-perkantoran');
    Route::post('/admin/pjperkantoran/{id}/sendbend' , 'AdminPjperkantoranController@sendbend')->name('sendbend-pj-perkantoran');

    // END

    // END PERKANTORAN

    Route::get('/admin/gen_to_transaksi/{ket}' , 'DashboardsController@gen_to_transaksi');
    Route::get('/admin/hitung_realisasi/{satkerid}' , 'DashboardsController@hitung_realisasi');
    Route::get('/admin/hitung_perbulan' , 'DashboardsController@hitung_perbulan');
    Route::get('/admin/hitung_jenis_belanja/{satkerid}' , 'DashboardsController@hitung_jenis_belanja');
    Route::get('/admin/hitung_rm_pnbp/{satkerid}' , 'DashboardsController@hitung_rm_pnbp');

    Route::get('/admin/laporan/realisasi' , 'AdminRkaklController@laporan_realisasi')->name('laporan-realisasi');
    Route::get('/admin/laporan/realisasi/import_to_transaksi' , 'AdminRkaklController@import_to_transaksi')->name('import-to-transaksi');

    //============//
    Route::get('/admin/uploadfile' , 'SAController@uploadfile')->name('SAUploadFile');

    // Update Module RKAKL
    Route::post('/admin/rkakl_upload/import' , 'AdminRkaklUploadController@fileUploadPost')->name('UploadRkakl');
    Route::get('/admin/rkakl_upload/semula/{uploadke}' , 'AdminRkaklUploadController@get_semula')->name('getRkaklSemula');
    Route::get('/admin/rkakl_upload/menjadi/{uploadke}' , 'AdminRkaklUploadController@get_menjadi')->name('getRkaklmenjadi');
    Route::get('/admin/rkakl_upload/{semula}/semula_menjadi/{menjadi}' , 'AdminRkaklUploadController@semula_menjadi')->name('semula-menjadi');
    Route::get('/admin/rkakl_upload/paguminus/{UploadKe}' , 'AdminRkaklUploadController@pagu_minus')->name('paguminus');
    Route::get('/admin/rkakl_upload/realisasi' , 'AdminRkaklUploadController@transfer_transaksi')->name('transfer_transaksi');
    Route::get('/admin/rkakl_upload/posting' , 'AdminRkaklUploadController@posting_transaksi')->name('posting_transfer');
    
