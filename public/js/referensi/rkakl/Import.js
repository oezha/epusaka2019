
$(document).ready(function()
{
       var bar = $('.bar');
       var percent = $('.percent');

        // $('form').ajaxForm({
        //     beforeSend: function() {
        //         var percentVal = '0%';
        //         bar.width(percentVal)
        //         percent.html(percentVal);
        //     },
        //     uploadProgress: function(event, position, total, percentComplete) {
        //         var percentVal = percentComplete + '%';
        //         bar.width(percentVal)
        //         percent.html(percentVal);
        //     },
        //     complete: function(xhr) {
        //         alert('File Uploaded Successfully');
        //         window.location.href = "/admin/rkakl_upload";
        //     }
        // });

        $("input[type='radio']").change(function(){
            console.log(this.value);
            if($(this).val()==1)
            {
               $("#form-group-revisike").show();
               $("#revisike").prop('required' , true);
            }
            else
            {
                $("#form-group-revisike").hide(); 
                $("#revisike").prop('required' , false);
            }
         
         });

         $("#semula").on('change' , function(){
            
            var b = parseInt(this.value) + 1;
            var a = document.getElementById('nilai_menjadi');
            a.value = '';
            a.value = b;
         });

}); 


function generate_sm(){

    
    // $('#loading-modal').show();
    var semula = document.getElementById('semula');
    var menjadi = document.getElementById('nilai_menjadi');

    var tabel = $('#table_semula_body');
    var tabel2 = $('#table_menjadi_body');

    tabel.empty();
    tabel2.empty();
    $html = "<tr><td colspan=8 class ='text-center'><strong>Loading Data</strong></td></tr>";
    tabel.append($html);
    tabel2.append($html);
    

    var url = "/admin/rkakl_upload/semula/" + semula.value;
    $.get(url , function(result){
     var tabel = $('#table_semula_body');
     tabel.empty();
    
     if(result.upload.length != 0){
        $.each(result.upload, function (key, entry) {
            if(entry.analisa_no_mak_sys == '1'){
                $html = '<tr class = "danger">';    
            }else{
                $html= "<tr>";
            }
            $html = $html + "<td>";
            if(entry.kode != null){
                $html = $html + entry.kode;
            }
            else{
                $html = $html + "";
            };
            $html = $html + "</td>";
            $html = $html + "<td>"+ entry.uraian +"</td>";
            $html = $html + "<td>"+ entry.vol +"</td>";
            $html = $html + "<td>";
            if(entry.sat != null){$html = $html + entry.sat;}else{ $html = $html + "";};
            $html = $html + "</td>";
            $html = $html + "<td>"+ entry.hargasat +"</td>";
            $html = $html + "<td>"+ entry.jumlah +"</td>";
            $html = $html + "<td>";
            if(entry.kdblokir != null){$html = $html + entry.kdblokir;}else{ $html = $html + "";};
            $html = $html + "</td>";
            $html = $html + "<td>";
            if(entry.sdana != null){$html = $html + entry.sdana;}else{ $html = $html + "";};
            $html = $html + "</td>";
            $html = $html + "</tr>";    
        
            tabel.append($html);
        
        });
       
     }else{
        $html = "<tr><td colspan=8 class ='text-center'><strong>Tidak Ada Data</strong></td></tr>";
        table.append($html);
        
     }
        
    });

    var url2 = "/admin/rkakl_upload/menjadi/" + menjadi.value;
    $.get(url2 , function(result2){
     var tabel2 = $('#table_menjadi_body');
     tabel2.empty();
     
     if(result2.upload.length != 0){
        $.each(result2.upload, function (key2, entry2) {
            
            $html= "<tr>";
            if(entry2.analisa_uraian == '2'){
                $html = '<tr class = "warning">';    
            }

            if(entry2.analisa_no_mak_sys == '3'){
                $html = '<tr class = "success">';    
            }

            $html = $html + "<td>";
            if(entry2.kode != null){
                $html = $html + entry2.kode;
            }
            else{
                $html = $html + "";
            };
            $html = $html + "</td>";
            $html = $html + "<td>"+ entry2.uraian +"</td>";
            $html = $html + "<td>"+ entry2.vol +"</td>";
            $html = $html + "<td>";
            if(entry2.sat != null){$html = $html + entry2.sat;}else{ $html = $html + "";};
            $html = $html + "</td>";
            $html = $html + "<td>"+ entry2.hargasat +"</td>";
            $html = $html + "<td>"+ entry2.jumlah +"</td>";
            $html = $html + "<td>";
            if(entry2.kdblokir != null){$html = $html + entry2.kdblokir;}else{ $html = $html + "";};
            $html = $html + "</td>";
            $html = $html + "<td>";
            if(entry2.sdana != null){$html = $html + entry2.sdana;}else{ $html = $html + "";};
            $html = $html + "</td>";
            $html = $html + "</tr>";    
        
            tabel2.append($html);
        
        });
        
     }else{
         $html = "<tr><td colspan=8 class ='text-center'><strong>Tidak Ada Data</strong></td></tr>";
         tabel2.append($html);
        
     }
       
    });

    
};
