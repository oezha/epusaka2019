$(document).ready(function(){
    $('#nama_peserta').on('change' , function(){
        var nilai = this.value;   
        
        var url =  "/nominatif/peg/" + nilai;
        $.ajax({
            type: "Get",
            url:  url,
            dataType: 'json',
            success: function(result) {
               document.getElementById('nip').value = result.nip;
               document.getElementById('instansi').value = result.jabatan;
               document.getElementById('golongan').value = result.gol;

               document.getElementById('uh').select();
            },
            error: function(xhr, status, error) {
                console.log(error);
            }
        });
    });
    $('#nama_tamu').on('change' , function(){
        var nilai = this.value;   
        
        var url =  "/nominatif/tamu/" + nilai;
        $.ajax({
            type: "Get",
            url:  url,
            dataType: 'json',
            success: function(result) {
               document.getElementById('nip').value = result.nip;
               document.getElementById('instansi').value = result.instansi;
               document.getElementById('jabatan').value = result.jabatan;

               document.getElementById('uh').select();
            },
            error: function(xhr, status, error) {
                console.log(error);
            }
        });
    });
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
};

function addcoma(obj)
{
    if (/\D/g.test(obj.value))
    {
        obj.value = obj.value.replace(/\D/g, '');
    }
    var angka = document.getElementById(obj.id);

    $(angka).val(angka.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
    hitung();
}

function hitung()
{
    var jumlahhonor = document.getElementById('jumlah_honor');
    var nilai = document.getElementById('nilai');
    var jumlahpotongan = document.getElementById('jumlah_potongan');
    var potongan = document.getElementById('potongan');
    var terima = document.getElementById('terima');
    // console.log(jumlahhonor.value);
    if(jumlahhonor.value != 0)
    {
        document.getElementById('nilai').value = jumlahhonor.value;
    }

    if(jumlahpotongan.value != 0)
    {
        document.getElementById('potongan').value = jumlahpotongan.value;
    }

    

    // var uh = document.getElementById('uh');
    // var lama = document.getElementById('lamauh');
    // var total = document.getElementById('totaluh');
    // var taxiprov = document.getElementById('taksi_prov');
    // var taxikab = document.getElementById('taksi_kab');
    // var inap = document.getElementById('penginapan');
    // var lamainap = document.getElementById('lama_penginapan');
    // var pesawat = document.getElementById('tiket_pesawat');
    
    // var nilai_uh = uh.value;
    
    
    var nilai_total = parseFloat(nilai.value.replace(/\D/g, '')) - parseFloat(potongan.value.replace(/\D/g, ''));
    // var nilai_penginapan = parseFloat(inap.value.replace(/\D/g, '')) * parseFloat(lamainap.value.replace(/\D/g, ''));

    $('#terima').val(nilai_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
    // $('#total_taksi_prov').val(taxiprov.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
    // $('#total_taksi_kab').val(taxikab.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
    // $('#total_penginapan').val(nilai_penginapan.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
    // $('#total_tiket_pesawat').val(pesawat.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
    


    // var totaluh = document.getElementById('totaluh');
    // var tottaksiprov = document.getElementById('total_taksi_prov');
    // var tottaksikab = document.getElementById('total_taksi_kab');
    // var totpenginapan = document.getElementById('total_penginapan');
    // var totpesawat = document.getElementById('total_tiket_pesawat');

    // var totalsemua = parseFloat(totaluh.value.replace(/\D/g, '')) + parseFloat(tottaksiprov.value.replace(/\D/g, '')) + parseFloat(tottaksikab.value.replace(/\D/g, '')) + parseFloat(totpenginapan.value.replace(/\D/g, '')) + parseFloat(totpesawat.value.replace(/\D/g, ''));
    // $('#total').val(totalsemua.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
}
