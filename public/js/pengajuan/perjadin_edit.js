$(document).ready(function(){

    getlama();
    // getNoMak();
    // getbagian();

    $('#rpk_id').on('change' , function(){
        getJenisBelanja(this.value);
    });

    $('#status_id').on('change' , function(){
        console.log(this.value);
        if(this.value == 6)
        {
            document.getElementById('alasan').removeAttribute('readonly');
            document.getElementById("alasan").focus() ;
        }
        
    });



    $("#bagian_id").prop("disabled", true);

    var lang = 'en';
        $(function () {
            $('.input_date').datepicker({
                format: 'yyyy-mm-dd',
                                language: lang
            });

            $('.open-datetimepicker').click(function () {
                $(this).next('.input_date').datepicker('show');
            });

        });
});


function getlama()
{
 $(document).ready(function() {
    $('#tgl_awal, #tgl_akhir').on('change textInput input', function () {
        if ( ($("#tgl_awal").val() != "") && ($("#tgl_akhir").val() != "")) {
            var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
            var firstDate = new Date($("#tgl_awal").val());
            var secondDate = new Date($("#tgl_akhir").val());
            var diffDays = Math.round(Math.round((secondDate.getTime() - firstDate.getTime()) / (oneDay))); 
            $("#lama").val(diffDays);
             
        }
    });
});
}

function getNoMak()
{
   var url = "/admin/perjadin/memuat_nomak";
   $.get(url , function(result){
        
        $.each(result, function (key, entry) {
            var kd6 = $('#kode_7');
            kd6.append("<option value="+ entry.kode_7 +">" + entry.no_mak_7 + " - " + entry.uraian_kegiatan + "</option");
            
          })
   });
}

function getbagian()
{
    var url = "/admin/perjadin/getbagian";
    $.get(url , function(result){
        
        console.log(result);
        document.getElementById("bagian_id").value=result;
            
          
   });
}