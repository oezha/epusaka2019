$(document).ready(function(){
    
    getNoMak();

    $("#bagian_id").prop("disabled", true);

    var lang = 'en';
        $(function () {
            $('.input_date').datepicker({
                format: 'yyyy-mm-dd',
                                language: lang
            });

            $('.open-datetimepicker').click(function () {
                $(this).next('.input_date').datepicker('show');
            });

        });

});

function getData(id)
{
    var url = "/admin/keg/"+ id + "/editdata";
   $.get(url , function(result){
        console.log(result);
        $.each(result, function (key, entry) {
            var jenisbalanja = $('#jenis_belanja');
            jenisbalanja.append("<option value="+ entry.no_urut +">" + entry.kode + " - " + entry.uraian + "</option");
            
          })
   });
}

function getNoMak()
{
   var url = "/admin/keg/memuat_nomak";
   $.get(url , function(result){
        
        $.each(result, function (key, entry) {
            var kd6 = $('#kode_7');
            kd6.append("<option value="+ entry.kode_7 +">" + entry.no_mak_7 + " - " + entry.uraian_kegiatan + "</option");
            
          })
   });
}