$(document).ready(function(){
    getNoMak();

    // getbagian();
    // blok_ls();

    $('#rkakl_id').on('change' , function(){
        
        getJenisBelanja(this.value);
    });

    $('#status_id').on('change' , function(){
        console.log(this.value);
        if(this.value == 6)
        {
            document.getElementById('alasan').removeAttribute('readonly');
            document.getElementById("alasan").focus() ;
        }
        
    });

    var lang = 'en';
        $(function () {
            $('.input_date').datepicker({
                format: 'yyyy-mm-dd',
                                language: lang
            });

            $('.open-datetimepicker').click(function () {
                $(this).next('.input_date').datepicker('show');
            });

        });
});

function getNoMak()
{
   var url = "/admin/keg/memuat_nomak";
   $.get(url , function(result){
    var kd6 = $('#rkakl_id');
    var jenisbalanja = $('#jenis_belanja'); 
    kd6.empty();
    jenisbalanja.empty();
    kd6.append("<option value=''>** Silahkan Pilih Kegiatan **</option>");
    jenisbalanja.append("<option value=''>** Silahkan Pilih Jenis Belanja **</option>");
        $.each(result, function (key, entry) {
            
            kd6.append("<option value="+ entry.kode_7 +">" + entry.no_mak_7 + " - " + entry.uraian_kegiatan + "</option");
            
          })
   });
}

function getJenisBelanja(id)
{
    var url = "/admin/keg/memuat_jenis_belanja/" + id;
    console.log(id);
   $.get(url , function(result){
    var jenisbalanja = $('#jenis_belanja');    
    jenisbalanja.empty();
    jenisbalanja.append("<option value=''>** Silahkan Pilih Jenis Belanja **</option>");
        $.each(result, function (key, entry) {
            
            jenisbalanja.append("<option value="+ entry.no_urut +">" + entry.kode + " - " + entry.uraian + "</option");
            
          })
   });
}

function getbagian()
{
    var url = "/admin/keg/getbagian";
    $.get(url , function(result){
        document.getElementById('bagian_id').value= result;
    });
}

// function blok_ls()
// {
//     var metode = document.getElementById('metode_bayar_id');
//     console.log(metode);
//     if(metode.value == 1)
//     {
//         document.getElementById('metode_bayar_id').setAttribute('disabled' , true);
//         document.getElementById('status_id').setAttribute('disabled' , true);
//     }
// }