$(document).ready(function(){

    $('#vol_pengajuan').on('keypress' , function(){
        console.log(this.value);
    });
    
    getSisa();
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
    
};

function addcoma(obj)
{
    if (/\D/g.test(obj.value))
        {
            obj.value = obj.value.replace(/\D/g, '');
        }
        var angka = document.getElementById(obj.id);
        var index = obj.name;
        index = index.replace("nilai[" , '');
        index = index.replace(']' , '');
        
        var sisapagu = document.getElementById('sisa_pagu[' +index+ ']');
        var sisaanggaran = parseFloat(sisapagu.value) - parseFloat(obj.value);
        
        if(parseFloat(obj.value) > parseFloat(sisapagu.value))
        {
            sisaanggaran = 0;
            alert('Nilai Pengajuan Melebihi Sisa Pagu!!');
            obj.value = 0;
        }
        document.getElementById('sisa_anggaran['+index+']').value = sisaanggaran.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        
        $(angka).val(angka.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
        getSisa();
        // hitung(obj);
        
};

function getSisa()
{
    
   
   var total = 0;
   $(".sisa").each(function(){
       var nilai = this.value.replace(/\D/g, '')
        total = parseFloat(total) + parseFloat(nilai);
   });

   var total_nilai = 0;
   $(".nilai").each(function(){
    var nilai = this.value.replace(/\D/g, '')
    total_nilai = parseFloat(total_nilai) + parseFloat(nilai);
   });


   document.getElementById('total_sisa').value = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
   document.getElementById('total_pengajuan').value = total_nilai.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");

    
}

function hitung(obj)
{
    
    var id  = obj.id;
    id = id.replace("vol_pengajuan[" , '');
    id = id.replace("]" , '');
    id = id.replace("nilai[" , '');
    id = id.replace("]" , '');

    var hargasat    = document.getElementById('hargasat['+id+']').value;
    var vol         = document.getElementById('vol_pengajuan['+id+']').value;
    var jml = 0;
    if(vol > 0 && vol != '')
    {
        jml      = parseFloat(hargasat) * parseFloat(vol);
        
    } 
    

    document.getElementById('nilai['+id+']').value = jml.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
    getSisa();

    
    
}