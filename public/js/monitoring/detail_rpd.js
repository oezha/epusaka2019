$(document).ready(function(){
    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46) {
            return true;
        } else if ( key < 48 || key > 57 ) {
            return false;
        } else {
            return true;
        }
    };

    $('[id^=bulan]').keypress(validateNumber);

    $('#jan').keyup(function(){
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }

        var getJan = document.getElementById('jan');
        var getFeb = document.getElementById('feb');
        var getMar = document.getElementById('mar');
        var getApr = document.getElementById('apr');
        var getMei = document.getElementById('mei');
        var getJun = document.getElementById('jun');
        var getJul = document.getElementById('jul');
        var getAug = document.getElementById('aug');
        var getSep = document.getElementById('sep');
        var getOct = document.getElementById('oct');
        var getNov = document.getElementById('nov');
        var getDec = document.getElementById('dec');
        var getPagu = document.getElementById('pagu');

        var total = 0;
        var total = parseFloat(getJan.value.replace(/\D/g, '')) + parseFloat(getFeb.value.replace(/\D/g, '')) + + parseFloat(getMar.value.replace(/\D/g, ''))
                    + parseFloat(getApr.value.replace(/\D/g, '')) + parseFloat(getMei.value.replace(/\D/g, '')) + + parseFloat(getJun.value.replace(/\D/g, ''));
                    + parseFloat(getJul.value.replace(/\D/g, '')) + parseFloat(getAug.value.replace(/\D/g, '')) + + parseFloat(getSep.value.replace(/\D/g, ''));
                    + parseFloat(getOct.value.replace(/\D/g, '')) + parseFloat(getNov.value.replace(/\D/g, '')) + + parseFloat(getDec.value.replace(/\D/g, ''));
        
                    if(total > parseFloat(getPagu.value.replace(/\D/g, '')))
                    {
                        alert('Jumlah Melebihi Pagu');
                        this.value = 0;
                        
                        this.focus();
                        var total = parseFloat(getJan.value.replace(/\D/g, '')) + parseFloat(getFeb.value.replace(/\D/g, '')) + + parseFloat(getMar.value.replace(/\D/g, ''))
                    + parseFloat(getApr.value.replace(/\D/g, '')) + parseFloat(getMei.value.replace(/\D/g, '')) + + parseFloat(getJun.value.replace(/\D/g, ''));
                    + parseFloat(getJul.value.replace(/\D/g, '')) + parseFloat(getAug.value.replace(/\D/g, '')) + + parseFloat(getSep.value.replace(/\D/g, ''));
                    + parseFloat(getOct.value.replace(/\D/g, '')) + parseFloat(getNov.value.replace(/\D/g, '')) + + parseFloat(getDec.value.replace(/\D/g, ''));
                       
                    }
  
        $('#jan').val($(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
        $('#total').val(total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
        

    });
    $('#feb').keyup(function(){
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }

        var getJan = document.getElementById('jan');
        var getFeb = document.getElementById('feb');
        var getMar = document.getElementById('mar');
        var getApr = document.getElementById('apr');
        var getMei = document.getElementById('mei');
        var getJun = document.getElementById('jun');
        var getJul = document.getElementById('jul');
        var getAug = document.getElementById('aug');
        var getSep = document.getElementById('sep');
        var getOct = document.getElementById('oct');
        var getNov = document.getElementById('nov');
        var getDec = document.getElementById('dec');
        var getPagu = document.getElementById('pagu');


        var total = parseFloat(getJan.value.replace(/\D/g, '')) + parseFloat(getFeb.value.replace(/\D/g, '')) + + parseFloat(getMar.value.replace(/\D/g, ''))
                    + parseFloat(getApr.value.replace(/\D/g, '')) + parseFloat(getMei.value.replace(/\D/g, '')) + + parseFloat(getJun.value.replace(/\D/g, ''));
                    + parseFloat(getJul.value.replace(/\D/g, '')) + parseFloat(getAug.value.replace(/\D/g, '')) + + parseFloat(getSep.value.replace(/\D/g, ''));
                    + parseFloat(getOct.value.replace(/\D/g, '')) + parseFloat(getNov.value.replace(/\D/g, '')) + + parseFloat(getDec.value.replace(/\D/g, ''));
        
                    if(total > parseFloat(getPagu.value.replace(/\D/g, '')))
                    {
                        alert('Jumlah Melebihi Pagu');
                        this.value = 0;
                        this.focus();
                    }

        $('#feb').val($(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));

    });
    $('#mar').keyup(function(){
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }

        var hargasat = parseFloat($(this).val().replace(/\./g, ''));

        $('#mar').val($(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));

    });
    $('#apr').keyup(function(){
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }

        var hargasat = parseFloat($(this).val().replace(/\./g, ''));

        $('#apr').val($(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));

    });
    $('#mei').keyup(function(){
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }

        var hargasat = parseFloat($(this).val().replace(/\./g, ''));

        $('#mei').val($(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));

    });
    $('#jun').keyup(function(){
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }

        var hargasat = parseFloat($(this).val().replace(/\./g, ''));

        $('##jun').val($(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));

    });
    $('#jul').keyup(function(){
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }

        var hargasat = parseFloat($(this).val().replace(/\./g, ''));

        $('#jul').val($(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));

    });
    $('#aug').keyup(function(){
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }

        var hargasat = parseFloat($(this).val().replace(/\./g, ''));

        $('#aug').val($(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));

    });
    $('#sep').keyup(function(){
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }

        var hargasat = parseFloat($(this).val().replace(/\./g, ''));

        $('#sep').val($(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));

    });
    $('#oct').keyup(function(){
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }

        var hargasat = parseFloat($(this).val().replace(/\./g, ''));

        $('#oct').val($(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));

    });
    $('#nov').keyup(function(){
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }

        var hargasat = parseFloat($(this).val().replace(/\./g, ''));

        $('#nov').val($(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));

    });
    $('#dec').keyup(function(){
        if (/\D/g.test(this.value))
        {
            this.value = this.value.replace(/\D/g, '');
        }

        var hargasat = parseFloat($(this).val().replace(/\./g, ''));

        $('#dec').val($(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));

    });

    function changeFormatNumber()
    {
        
        var ajan = document.getElementById('jan');
        ajan.value = ajan.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        var afeb = document.getElementById('feb');
        afeb.value = afeb.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        var amar = document.getElementById('mar');
        amar.value = amar.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        var aapr = document.getElementById('apr');
        aapr.value = aapr.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        var amei = document.getElementById('mei');
        amei.value = amei.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        var ajun = document.getElementById('jun');
        ajun.value = ajun.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        var ajul = document.getElementById('jul');
        ajul.value = ajul.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        var aaug = document.getElementById('aug');
        aaug.value = aaug.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        var asep = document.getElementById('sep');
        asep.value = asep.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        var aoct = document.getElementById('oct');
        aoct.value = aoct.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        var anov = document.getElementById('nov');
        anov.value = anov.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        var adec = document.getElementById('dec');
        adec.value = adec.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        hitung();
        
    }

    function hitung()
    {
        var getJan = document.getElementById('jan');
        var getFeb = document.getElementById('feb');
        var getMar = document.getElementById('mar');
        var getApr = document.getElementById('apr');
        var getMei = document.getElementById('mei');
        var getJun = document.getElementById('jun');
        var getJul = document.getElementById('jul');
        var getAug = document.getElementById('aug');
        var getSep = document.getElementById('sep');
        var getOct = document.getElementById('oct');
        var getNov = document.getElementById('nov');
        var getDec = document.getElementById('dec');
        
        var total = parseFloat(getJan.value.replace(/\D/g, '')) + parseFloat(getFeb.value.replace(/\D/g, '')) + + parseFloat(getMar.value.replace(/\D/g, ''))
                    + parseFloat(getApr.value.replace(/\D/g, '')) + parseFloat(getMei.value.replace(/\D/g, '')) + + parseFloat(getJun.value.replace(/\D/g, ''));
                    + parseFloat(getJul.value.replace(/\D/g, '')) + parseFloat(getAug.value.replace(/\D/g, '')) + + parseFloat(getSep.value.replace(/\D/g, ''));
                    + parseFloat(getOct.value.replace(/\D/g, '')) + parseFloat(getNov.value.replace(/\D/g, '')) + + parseFloat(getDec.value.replace(/\D/g, ''));
        $('#total').val(total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));

    }

    window.onload = changeFormatNumber();
});
