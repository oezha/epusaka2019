<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use CRUDBooster;

class RkaklController extends Controller
{
    public function load_rkakl()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(180);
        // delete rkakl

        DB::table('rkakl')->delete();

        $userid = CRUDBooster::MyId();
        $satkeruser = DB::table('satker_user')->where('user_id' , $userid)->first();
        $data_upload = DB::table('rkakl_upload')
                            ->where('satker_id' , $satkeruser->satker_id)
                           ->get();
        $parameter = DB::table('parameter')->where('nama' , 'Tahun Anggaran')->first();
        $data = [];
        $kode9 = '';$kode4 = '';$kode8 = '';$kode6 = '';$kode7 = '';$kode11 = '';$kode0 = '';
        $x = 0;$y=0;$z=0;
        $no = 1;
        foreach ($data_upload as $key => $value) {
            $nomak = '';$nomaksys = '';
            switch (strlen($value->kode)) {
                case 9:
                    $kode9 = trim($value->kode);
                    $nomak = $kode9;
                    $nomaksys = $kode9;
                    break;
                case 4:
                    $kode4 = trim($value->kode);
                    $nomak = $kode9 . '.' . $kode4;
                    $nomaksys = $kode9 . '.' . $kode4;
                    break;
                case 8:
                    $kode8 = trim($value->kode);
                    $nomak = $kode9 . '.' . $kode8;
                    $nomaksys = $kode9 . '.' . $kode8;
                    break;
                case 6:
                    $kode6 = trim($value->kode);
                    $nomak = $kode9 . '.' . $kode8 . '.' . $kode6;
                    $nomaksys = $kode9 . '.' . $kode8 . '.' . $kode6;
                    break;
                case 7:
                    $kode7 = trim($value->kode);
                    $nomak = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7;
                    $nomaksys = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7;
                    break;
                case 11:
                    $kode11 = trim($value->kode);
                    $x = 0;
                    $z = 0 ;
                    $nomak = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11;
                    $nomaksys = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11;
                    break;
                default:
                    
                    $tanda1 = trim(substr($value->uraian , 0 , 3));    
                    $tanda2 = trim(substr($tanda1 , 0 , 2));
                    if($tanda2 == ">")
                    {
                        $x = $x + 1;
                        $y = 0;
                        $z = 0;
                        $nomak = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11;
                        $nomaksys = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11 . '.' . $x;
                    }
                    elseif ($tanda2 == ">>") {
                        $y = $y + 1;
                        $z = 0;
                        $nomak = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11;
                        $nomaksys = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11 . '.' . $x . '.' . $y;
                    }
                    else
                    {
                        $z = $z + 1;
                        $nomak = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11;
                        $nomaksys = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11 . '.' . $x . '.' . $y . '.' . $z;
                    }



                    break;
            }

            $nomaksys = $nomaksys;

            $data['id']               = $no;
            $data['no_urut']               = $no;
            $data['satker_id']        = $satkeruser->satker_id;
            $data['thnang']           = $parameter->nilai;
            $data['no_mak']           = $nomak;
            $data['no_mak_sys']       = $nomaksys;
            $data['level']            = strlen($value->kode);
            $data['kode']             = TRIM($value->kode);
            $data['uraian']           = $value->uraian;
            $data['vol']              = $value->vol;
            $data['sat']              = $value->sat;
            $data['hargasat']         = $value->hargasat;
            $data['jumlah']           = $value->jumlah;
            $data['kdblokir']         = $value->kdblokir;
            $data['sdana']            = $value->sdana;

            if($data)
            {
                DB::table('rkakl')->insert($data);
            }


            $no = $no + 1;
        }

        
        

        $baseline_update = DB::table('rkakl')
                                ->where('uraian' , 'LIKE' , '%_x000D_[Base Line]%')
                                ->get();

        foreach ($baseline_update as $key => $value) {
            $geturaian = $value->uraian;
            $a = substr($geturaian , 0 , strlen($geturaian) - 18  );
            DB::table('rkakl')
                        ->where('id' , $value->id)
                        ->update(['uraian' => $a]);
        }
        
        DB::table('parameter')->where('nama','id_rkakl')->update(['nilai'=>$no]);

        $this->update_kode_7_m_rpk();
        
        return back();
        
    }

    public function update_kode_7_m_rpk()
    {
        $data_mRPK = DB::table('m_rpk')->get();
        
        foreach ($data_mRPK as $key => $value) {
            $data = DB::table('rkakl')->where('no_mak_sys' , $value->no_mak_7)->first();

            DB::table('m_rpk')->where('id' , $value->id)
                                ->update(['kode_7' => $data->id]);

            DB::table('detail_rpd')->where('no_mak_sys' , $value->no_mak_7)
                                ->update(['rkakl_id'=> $data->id]);
        }

        $this->update_detail_rpd();
    }

    public function update_detail_rpd()
    {
        $dataRPK = DB::table('m_rpk')->get();
        foreach ($dataRPK as $key => $value) {
            $detail_rpd = DB::table('detail_rpd')->where('no_mak_sys' , 'LIKE' , '%' . $value->no_mak_7 . '%')->get();
            foreach ($detail_rpd as $key2 => $value2) {
                $get_rkakl = DB::table('rkakl')->where('no_mak_sys' , 'LIKE' , '%' . $value2->no_mak_sys . '%')->first();

                DB::table('detail_rpd')
                            ->where('id' , $value2->id)
                            ->update(['rkakl_id' => $get_rkakl->id]);
            }
        }
    }

    public function cek_detail_rpd()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(180);
        DB::table('detail_rpd_temp')->delete();
        $detail_rpd = DB::table('detail_rpd')->where('level' , 6)->OrWhere('level' , 7)->get();

        foreach ($detail_rpd as $key => $value) {
            $detailRpd  = DB::table('detail_rpd')->where('no_mak_sys' , 'LIKE' , '%' . $value->no_mak_sys . '%')->Count();
            $Rkakl      = DB::table('rkakl')->where('no_mak_sys' , 'LIKE' , '%' . $value->no_mak_sys . '%')->Count();
            
            if($detailRpd != $Rkakl)
            {
                $insert = [];
                $insert2 = [];
                if($detailRpd > $rkakl)
                {
                    $data_detail = DB::table('detail_rpd')->where('no_mak_sys' , 'LIKE' , '%' . $value->no_mak_sys . '%')
                    ->OrderBy('no_mak_sys' , 'asc')
                    ->chunk(50 , function($details){
                        foreach ($details as $key2 => $value2) {
                            $insert['rkakl_id']         =   $value2->rkakl_id;
                            $insert['kode']             =   $value2->kode;
                            $insert['level']            =   $value2->level;
                            $insert['no_mak_sys']       =   $value2->no_mak_sys;
                            $insert['uraian']           =   $value2->uraian;
                            $insert['vol']              =   $value2->vol;
                            $insert['sat']              =   $value2->sat;
                            $insert['hargasat']         =   $value2->hargasat;
                            $insert['jumlah']           =   $value2->jumlah;

                            DB::table('detail_rpd_temp')->insert($insert);
                            
                        }
                    });

                    $data_rkakl = DB::table('rkakl')->where('no_mak_sys' , 'LIKE' , '%' . $value->no_mak_sys . '%')
                    ->OrderBy('no_mak_sys' , 'asc')
                    ->chunk(50 , function($rkakls){
                        foreach ($rkakls as $key3 => $value3) {

                            $cek = DB::table('detail_rpd_temp')->where('no_mak_sys' , $value3->no_mak_sys)->count();
                            if($cek > 0)
                            {
                                DB::table('detail_rpd_temp')->where('no_mak_sys' , $value3->no_mak_sys)
                                ->update([
                                    'rkakl_id_rkakl'    =>$value3->id,
                                    'kode_rkakl'        =>$value3->kode,
                                    'level_rkakl'       =>$value3->level,
                                    'no_mak_sys_rkakl'  =>$value3->no_mak_sys,
                                    'uraian_rkakl'      =>$value3->uraian,
                                    'vol_rkakl'         =>$value3->vol,
                                    'sat_rkakl'         =>$value3->sat,
                                    'hargasat_rkakl'    =>$value3->hargasat,
                                    'jumlah_rkakl'      =>$value3->jumlah
                                 ]);
                            }
                            else
                            {
                                
                                $insert['rkakl_id_rkakl']=$value3->id;
                                $insert['kode_rkakl']=$value3->kode;
                                $insert['level_rkakl']=$value3->level;
                                $insert['no_mak_sys_rkakl']=$value3->no_mak_sys;
                                $insert['uraian_rkakl']=$value3->uraian;
                                $insert['vol_rkakl']=$value3->vol;
                                $insert['sat_rkakl']=$value3->sat;
                                $insert['hargasat_rkakl']=$value3->hargasat;
                                $insert['jumlah_rkakl']=$value3->jumlah;

                                DB::table('detail_rpd_temp')->insert($insert);
                                
                                $insert2['id']=$value3->id;
                                $insert2['m_rpd_id']=$value->m_rpd_id;
                                $insert2['rkakl_id']=$value3->id;
                                $insert2['kode']=$value3->kode;
                                $insert2['level']=$value3->level;
                                $insert2['no_mak_sys']=$value3->no_mak_sys;
                                $insert2['uraian']=$value3->uraian;
                                $insert2['vol']=$value3->vol;
                                $insert2['sat']=$value3->sat;
                                $insert2['hargasat']=$value3->hargasat;
                                $insert2['jumlah']=$value3->jumlah;
                                $insert2['jan']=0;
                                $insert2['feb']=0;
                                $insert2['mar']=0;
                                $insert2['apr']=0;
                                $insert2['mei']=0;
                                $insert2['jun']=0;
                                $insert2['jul']=0;
                                $insert2['aug']=0;
                                $insert2['sep']=0;
                                $insert2['oct']=0;
                                $insert2['nov']=0;
                                $insert2['dec']=0;

                                DB::table('detail_rpd')->insert($insert2);

                            }


                           
                        }
                    });

                    


                }
                
                
                
            }
        }

        
        

        return 'OK';
    }
}
