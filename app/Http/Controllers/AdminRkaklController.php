<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminRkaklController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "100";
			$this->orderby = "id,Asc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = false;
			$this->button_delete = false;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "rkakl";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Kode","name"=>"kode"];
			$this->col[] = ["label"=>"Uraian","name"=>"uraian"];
			$this->col[] = ["label"=>"Vol","name"=>"vol"];
			$this->col[] = ["label"=>"Sat","name"=>"sat"];
			$this->col[] = ["label"=>"Hargasat","name"=>"hargasat"];
			$this->col[] = ["label"=>"Jumlah","name"=>"jumlah"];
			$this->col[] = ["label"=>"Kdblokir","name"=>"kdblokir"];
			$this->col[] = ["label"=>"Sdana","name"=>"sdana"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'No Mak','name'=>'no_mak','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'No Mak Sys','name'=>'no_mak_sys','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Level','name'=>'level','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Kode','name'=>'kode','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Uraian','name'=>'uraian','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Vol','name'=>'vol','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Sat','name'=>'sat','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Hargasat','name'=>'hargasat','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Jumlah','name'=>'jumlah','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Kdblokir','name'=>'kdblokir','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Sdana','name'=>'sdana','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Realisasi 1','name'=>'realisasi_1','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Realisasi 2','name'=>'realisasi_2','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Realisasi 3','name'=>'realisasi_3','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Jenis Transaksi Id','name'=>'jenis_transaksi_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Satker Id","name"=>"satker_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"satker,nama"];
			//$this->form[] = ["label"=>"Thnang","name"=>"thnang","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"No Mak","name"=>"no_mak","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"No Mak Sys","name"=>"no_mak_sys","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Level","name"=>"level","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Kode","name"=>"kode","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Uraian","name"=>"uraian","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			//$this->form[] = ["label"=>"Vol","name"=>"vol","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Sat","name"=>"sat","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Hargasat","name"=>"hargasat","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Jumlah","name"=>"jumlah","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Kdblokir","name"=>"kdblokir","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Sdana","name"=>"sdana","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Realisasi 1","name"=>"realisasi_1","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Realisasi 2","name"=>"realisasi_2","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Realisasi 3","name"=>"realisasi_3","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Jenis Transaksi Id","name"=>"jenis_transaksi_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"jenis_transaksi,id"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
			$this->index_button = array();
			$this->index_button[] = ['label'=>'Load RKAKL','url'=>'/rkakl/load-rkakl',"icon"=>"fa fa-spinner"];



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

		}
		
		public function laporan_realisasi()
		{
			if(!CRUDBooster::myId()) { 
				CRUDBooster::redirect(CRUDBooster::adminPath('login'),"Silahkan Login!");
			}


			$data = [];
			$data['page_title'] = 'Realisasi';
			$data['row'] = DB::table('rkakl')->get();
			

			//Please use cbView method instead view method from laravel
			$this->cbView('backend.laporan.realisasi',$data);
		}

		public function import_to_transaksi()
		{
			if(!CRUDBooster::myId()) { 
				CRUDBooster::redirect(CRUDBooster::adminPath('login'),"Silahkan Login!");
			}
			$this->transfer_kegiatan_to_transaksi();
			$this->transfer_perjadin_to_transaksi();
			CRUDBooster::redirect(CRUDBooster::adminPath('laporan/realisasi'),"Complete!");
			
		}

		public function hitung_transaksi_to_realisasi()
		{
			DB::table('rkakl')->update(['realisasi_1' => 0 , 'realisasi_2' => 0 , 'realisasi_3' => 0]);


			$transaksi = DB::table('transaksi')
						->select(DB::raw('SUM(jumlah) as summary') , 'status_id' , 'no_mak_sys')
						->groupby('no_mak_sys' , 'status_id')
						->OrderBy('no_mak_sys')
						->chunk(50 , function($data){
							foreach ($data as $key => $value) {
								if($value->status_id == 3 || $value->status_id == 4 ){
									DB::table('rkakl')->where('no_mak_sys' , $value->no_mak_sys)->update(['realisasi_1' => $value->summary]);
								}
								elseif($value->status_id == 5 || $value->status_id == 7 || $value->status_id == 8 || $value->status_id == 9 || $value->status_id == 11 )
								{
									DB::table('rkakl')->where('no_mak_sys' , $value->no_mak_sys)->update(['realisasi_2' => $value->summary]);
								}
								elseif($value->status_id == 10)
								{
									DB::table('rkakl')->where('no_mak_sys' , $value->no_mak_sys)->update(['realisasi_2' => $value->summary]);
								}
							}
						});
			return 'OK';

			$transaksi = DB::table('transaksi')
						->select(DB::raw('SUM(jumlah) as summary') , 'status_id' , DB::raw('SUBSTR(no_mak_sys,1,LENGTH(no_mak_sys) - 2) as no_mak_sys'))
						->groupby('SUBSTR(no_mak_sys,1,LENGTH(no_mak_sys) - 2)' , 'status_id')
						->OrderBy('SUBSTR(no_mak_sys,1,LENGTH(no_mak_sys) - 2)')
						->chunk(50 , function($data){

							foreach ($data as $key => $value) {
								if($value->status_id == 3 || $value->status_id == 4 ){
									DB::table('rkakl')->where('no_mak_sys' , $value->no_mak_sys)->update(['realisasi_1' => $value->summary]);
								}
								elseif($value->status_id == 5 || $value->status_id == 7 || $value->status_id == 8 || $value->status_id == 9 || $value->status_id == 11 )
								{
									DB::table('rkakl')->where('no_mak_sys' , $value->no_mak_sys)->update(['realisasi_2' => $value->summary]);
								}
								elseif($value->status_id == 10)
								{
									DB::table('rkakl')->where('no_mak_sys' , $value->no_mak_sys)->update(['realisasi_2' => $value->summary]);
								}
							}

						});

			
		}

		public function transfer_kegiatan_to_transaksi(){
			DB::table('transaksi')->where('keterangan' , 'Kegiatan')->delete();
			//Kegiatan
			DB::table('m_kegiatan')
			->orderby('id' , 'asc')
			->chunk(50 , function($data){
					foreach ($data as $key => $value) {

						$data_bagian = DB::table('bagian')->where('id' , $value->bagian_id)->first();
						$satkerid = $data_bagian->satker_id;
						$statusid = $value->status_id;
						$tanggal  = $value->tgl_pengajuan;
						DB::table('detail_kegiatan')
								->where('kegiatan_id' , $value->id)
								->where('level' , 0)
								->orderby('id' , 'asc')
								->chunk(50 , function($detail) use ($satkerid , $statusid){
									foreach ($detail as $key => $value2) {
										$insert['id_t'] 		= $value2->kegiatan_id;
										$insert['satker_id'] 	= $satkerid;
										$insert['keterangan'] 	= 'Kegiatan';
										$insert['no_mak_sys'] 	= $value2->no_mak_sys;
										$insert['jumlah'] 		= $value2->jumlah_pengajuan;
										$insert['status_id'] 	= $statusid;
										$insert['tanggal'] 		= $tanggal;
										DB::table('transaksi')->insert($insert);

									}
								});
					}
			});
		}

		public function transfer_perjadin_to_transaksi(){
			DB::table('transaksi')->where('keterangan' , 'Perjadin')->delete();
			//Kegiatan
			DB::table('Perjadin')
			->orderby('id' , 'asc')
			->chunk(50 , function($data){
					foreach ($data as $key => $value) {

						$data_bagian = DB::table('bagian')->where('id' , $value->bagian_id)->first();
						$satkerid = $data_bagian->satker_id;
						$statusid = $value->status_id;
						$tanggal  = $value->tgl_pengajuan;
						DB::table('detail_perjadin')
								->where('perjadin_id' , $value->id)
								->where('level' , 0)
								->orderby('id' , 'asc')
								->chunk(50 , function($detail) use ($satkerid , $statusid){
									foreach ($detail as $key => $value2) {
										$insert['id_t'] 		= $value2->perjadin_ids;
										$insert['satker_id'] 	= $satkerid;
										$insert['keterangan'] 	= 'Perjadin';
										$insert['no_mak_sys'] 	= $value2->no_mak_sys;
										$insert['jumlah'] 		= $value2->jumlah_pengajuan;
										$insert['status_id'] 	= $statusid;
										$insert['tanggal'] 		= $tanggal;
										DB::table('transaksi')->insert($insert);

									}
								});
					}
			});
		}




	    //By the way, you can still create your own method in here... :) 


	}