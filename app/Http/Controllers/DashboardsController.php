<?php namespace App\Http\Controllers;
use Session;
use Request;
use DB;
use CRUDBooster;
use Charts;
use Carbon\Carbon;


class DashboardsController extends \crocodicstudio\crudbooster\controllers\CBController {
 public function cbInit() {
# START CONFIGURATION DO NOT REMOVE THIS LINE
$this->title_field = "";
$this->limit = "20";
$this->orderby = "id,desc";
$this->global_privilege = false;
$this->button_table_action = true;
$this->button_bulk_action = true;
$this->button_action_style = "button_icon";
$this->button_add = true;
$this->button_edit = true;
$this->button_delete = true;
$this->button_detail = false;
$this->button_show = true;
$this->button_filter = true;
$this->button_import = false;
$this->button_export = true;
# END CONFIGURATION DO NOT REMOVE THIS LINE
# START COLUMNS DO NOT REMOVE THIS LINE
# END COLUMNS DO NOT REMOVE THIS LINE
# START FORM DO NOT REMOVE THIS LINE
# END FORM DO NOT REMOVE THIS LINE
/*
 | ----------------------------------------------------------------------
 | Sub Module
 | ----------------------------------------------------------------------
| @label = Label of action
| @path = Path of sub module
| @foreign_key = foreign key of sub table/module
| @button_color = Bootstrap Class (primary,success,warning,danger)
| @button_icon = Font Awesome Class
| @parent_columns = Sparate with comma, e.g : name,created_at
 |
 */
 $this->sub_module = array();
 /* 
 | ----------------------------------------------------------------------
 | Add More Action Button / Menu
 | ----------------------------------------------------------------------
 | @label = Label of action
 | @url = Target URL, you can use field alias. e.g : [id], [name], [title], etc
 | @icon = Font awesome class icon. e.g : fa fa-bars
 | @color = Default is primary. (primary, warning, succecss, info)
 | @showIf = If condition when action show. Use field alias. e.g : [id] == 1
 |
 */
 $this->addaction = array();
 /*
 | ----------------------------------------------------------------------
 | Add More Button Selected
 | ----------------------------------------------------------------------
 | @label = Label of action
 | @icon = Icon from fontawesome
 | @name = Name of button
 | Then about the action, you should code at actionButtonSelected method
 |
 */
 $this->button_selected = array();

 /*
 | ----------------------------------------------------------------------
 | Add alert message to this module at overheader
 | ----------------------------------------------------------------------
 | @message = Text of message
 | @type = warning,success,danger,info
 |
 */
 $this->alert = array();

 /*
 | ----------------------------------------------------------------------
 | Add more button to header button
 | ----------------------------------------------------------------------
 | @label = Name of button
 | @url = URL Target
 | @icon = Icon from Awesome.
 |
 */
 $this->index_button = array();
 /*
 | ----------------------------------------------------------------------
 | Customize Table Row Color
 | ----------------------------------------------------------------------
 | @condition = If condition. You may use field alias. E.g : [id] == 1
 | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
 |
 */
 $this->table_row_color = array();

 /*
 | ----------------------------------------------------------------------
 | You may use this bellow array to add statistic at dashboard
 | ----------------------------------------------------------------------
 | @label, @count, @icon, @color
 |
 */
 $this->index_statistic = array();
 
 /*
 | ----------------------------------------------------------------------
 | Add javascript at body
 | ----------------------------------------------------------------------
 | javascript code in the variable
 | $this->script_js = "function() { ... }";
 | $this->script_js = "";
 */
$this->script_js = "";
 /*
 | ----------------------------------------------------------------------
 | Include HTML Code before index table
 | ----------------------------------------------------------------------
 | html code to display it before index table
 | $this->pre_index_html = "<p>test</p>";
 |
 */
 $this->pre_index_html = null;


 /*
 | ----------------------------------------------------------------------
 | Include HTML Code after index table
 | ----------------------------------------------------------------------
 | html code to display it after index table
 | $this->post_index_html = "<p>test</p>";
 |
 */
 $this->post_index_html = null;
 

 /*
 | ----------------------------------------------------------------------
 | Include Javascript File
 | ----------------------------------------------------------------------
 | URL of your javascript each array
 | $this->load_js[] = asset("myfile.js");
 | $this->load_js = array();
 */
 $this->load_js = array();


 /*
 | ----------------------------------------------------------------------
 | Add css style at body
 | ----------------------------------------------------------------------
 | css code in the variable
 | $this->style_css = ".style{....}";
 | $this->style_css = "";
 */
 $this->style_css = "";


 /*
 | ----------------------------------------------------------------------
 | Include css File
 | ----------------------------------------------------------------------
 | URL of your css each array
 | $this->load_css[] = asset("myfile.css");
 |
 */
 $this->load_css = array();


 }

 /*
 | ----------------------------------------------------------------------
 | Hook for button selected
 | ----------------------------------------------------------------------
 | @id_selected = the id selected
 | @button_name = the name of button
 |
 */
 public function actionButtonSelected($id_selected,$button_name) {
 //Your code here

 }
 /*
 | ----------------------------------------------------------------------
 | Hook for manipulate query of index result
 | ----------------------------------------------------------------------
 | @query = current sql query
 |
 */
 public function hook_query_index(&$query) {
 //Your code here

 }
 /*
 | ----------------------------------------------------------------------
 | Hook for manipulate row of index table html
 | ----------------------------------------------------------------------
 |
 */
 public function hook_row_index($column_index,&$column_value) {
 //Your code here
 }
 /*
 | ----------------------------------------------------------------------
 | Hook for manipulate data input before add data is execute
 | ----------------------------------------------------------------------
 | @arr
 |
 */
 public function hook_before_add(&$postdata) {
 //Your code here
 }
 /*
 | ----------------------------------------------------------------------
 | Hook for execute command after add public static function called
 | ----------------------------------------------------------------------
 | @id = last insert id
 |
 */
 public function hook_after_add($id) {
 //Your code here
 }
 /*
 | ----------------------------------------------------------------------
 | Hook for manipulate data input before update data is execute
 | ----------------------------------------------------------------------
 | @postdata = input post data
 | @id = current id
 |
 */
 public function hook_before_edit(&$postdata,$id) {
 //Your code here
 }
 /*
 | ----------------------------------------------------------------------
 | Hook for execute command after edit public static function called
 | ----------------------------------------------------------------------
 | @id = current id
 |
 */
 public function hook_after_edit($id) {
 //Your code here
 }
 /*
 | ----------------------------------------------------------------------
 | Hook for execute command before delete public static function called
 | ----------------------------------------------------------------------
 | @id = current id
 |
 */
 public function hook_before_delete($id) {
 //Your code here
 }
 /*
 | ----------------------------------------------------------------------
 | Hook for execute command after delete public static function called
 | ----------------------------------------------------------------------
 | @id = current id
 |
 */
 public function hook_after_delete($id) {
 //Your code here
 }

 public function getIndex() {

    if(!CRUDBooster::myId()){ return redirect('/admin/login'); }
    

    // REALISASI //
        $this->realisasi();
        $data = $this->data_realisasi();
        $no2  =	 Charts::create('pie', 'highcharts')
                        ->title('Realisasi')
                        ->labels(['Alokasi Pagu', 'Pengajuan', 'SPM' , 'SP2D'])
                        ->values([$data->pagu , $data->realisasi_1 , $data->realisasi_2 , $data->realisasi_3])
                        ->dimensions(800,250)
                        ->responsive(true);
    


        return view('backend.dashboard.index',compact('no1' , 'no2' , 'chart2'));
}


public function realisasi()
{
            
    $user_data = DB::table('satker_user')->where('user_id' , CRUDBooster::myid())->first();
    $rkakl = DB::table('rkakl')->where('level' , 9)
    ->where('satker_id' , $user_data->satker_id)
    ->first();
    $data = DB::table('realisasi')->where('satker_id' , $user_data->satker_id)->delete();

    $insert['satker_id']    = $user_data->satker_id;
    $insert['pagu']         = $rkakl->jumlah;
    $insert['realisasi_1'] = $rkakl->realisasi_1;
    $insert['realisasi_2'] = $rkakl->realisasi_2;
    $insert['realisasi_3'] = $rkakl->realisasi_3;

    $data1 = DB::table('realisasi')->insert($insert);

    return redirect('/admin');
}

public function gen_to_transaksi($ket)
{
    // Kegiatan
    DB::table('transaksi')
    ->where('keterangan' , $ket)
    ->delete();
    $table          = "";
    $table_detail   = "";
    $field          = "";


    switch ($ket) {
        case 'kegiatan':
            $table = 'm_kegiatan';
            $field = 'kegiatan_id';
            $table_detail   = "detail_kegiatan";
            break;
        case 'perjadin':
            $table = 'perjadin';
            $field = 'perjadin_id';
            $table_detail   = "detail_perjadin";
            break;
        case 'honor':
            $table = 'honor';
            $field = 'honor_id';
            $table_detail   = "detail_honor";
            break;
        default:
            $table = 'perkantoran';
            $field = 'perkantoran_id';
            $table_detail   = "detail_perkantoran";
            break;
    }


    $status = ['3' , '4' , '5'];
    $insert = [];
    $data_user = DB::table('satker_user')->where('user_id' , CRUDBooster::myId())->first();
    $satker_id = $data_user->satker_id;
    $data = DB::table($table)->whereIn('status_id' , $status )
    ->orderBy('id' , 'Asc')
    ->chunk( 100 , function($kegiatan) use ($table , $table_detail , $field , $ket , $satker_id) {
        foreach ($kegiatan as $key => $value)  {
            $status = $value->status_id;
            $tanggal = $value->tgl_pengajuan;
            $detail = DB::table($table_detail)
                            ->where($field , $value->id)
                            ->where('level' , 0)
                            ->where('jumlah_pengajuan' , '!=' , 0) 
                            ->orderBy('id' , 'Asc')
                            ->chunk( 50 , function($detail) use ($table , $table_detail , $field , $ket , $satker_id , $status , $tanggal) {
                                foreach ($detail as $key_detail => $value_detail) {
                                    $no_mak = explode("." , $value_detail->no_mak_sys);

                                    $insert['id_t']         =   $value_detail->kegiatan_id;
                                    $insert['satker_id']    =   $satker_id;
                                    $insert['keterangan']   =   $ket;
                                    $insert['no_mak_sys']   =   $value_detail->no_mak_sys;
                                    $insert['jumlah']       =   $value_detail->jumlah_pengajuan;
                                    $insert['kode_9']       =   $no_mak[0] . "." . $no_mak[1] . "." . $no_mak[2];
                                    $insert['kode_4']       =   $no_mak[3];
                                    $insert['kode_8']       =   $no_mak[3] . "." . $no_mak[4];
                                    $insert['kode_6']       =   $no_mak[5];
                                    $insert['kode_7']       =   $no_mak[6] ;
                                    $insert['kode_11']      =   $no_mak[7];
                                    $insert['kode_0']       =   $no_mak[8] . "." . $no_mak[9] . "." . $no_mak[10];
                                    $insert['status_id']    =   $status;
                                    $insert['tanggal']      =   $tanggal;

                                    $cek = DB::table('transaksi')
                                                ->where('satker_id' , $satker_id)
                                                ->where('id_t' , $value->id)
                                                ->where('keterangan' , $ket)
                                                ->Count();
                                    if($cek == 0)
                                    {
                                        DB::table('transaksi')->insert($insert);
                                    }
                                }
                            });
        }
    });

}

public function hitung_realisasi($satker_id)
{
    $insert = [];
    $bulan = ['Jan' , 'Feb' , 'Mar' , 'Apr' , 'Mei' , 'Jun' , 'Jul' , 'Aug' , 'Sep' , 'Okt' , 'Nov' , 'Des'];
    DB::table('realisasi_per_bulan')->delete();
    for ($i=0; $i < 12 ; $i++) { 
        $insert['satker_id']            =   $satker_id;
        $insert['tahun']                =   CRUDBooster::myYear();
        $insert['bulan']                =   $bulan[$i];
        $insert['nilai_pengajuan']      =   0;
        $insert['nilai_dilaksanakan']   =   0;
        $insert['nilai_selesai']        =   0;

        
        DB::table('realisasi_per_bulan')->insert($insert);

    }
    
    $data_transaksi = DB::table('transaksi')
    ->select( 'status_id' , DB::raw('Month(tanggal) as bulan , SUM(jumlah) as nilai') )
    ->WhereYear('tanggal' , CRUDBooster::myYear())
    ->OrderBy('id' , 'Asc')
    ->GroupBy(DB::raw('Month(tanggal)'))
    ->GroupBy('status_id')
    ->chunk(10 , function($data) use ($bulan , $satker_id){
        foreach ($data as $key => $value) {
            switch ($value->status_id) {
                case 5:
                    DB::table('realisasi_per_bulan')
                    ->where('bulan' , $bulan[$value->bulan - 1])
                    ->where('satker_id' , $satker_id)
                    ->where('tahun' , CRUDBooster::myYear())
                    ->update(['nilai_dilaksanakan' => $value->nilai]);
                    break;
                
                default:
                    # code...
                    break;
            }
        }

    });

    return 'OK';
}

public function hitung_perbulan()
{
    $insert = [];
    $bulan = ['Jan' , 'Feb' , 'Mar' , 'Apr' , 'Mei' , 'Jun' , 'Jul' , 'Aug' , 'Sep' , 'Okt' , 'Nov' , 'Des'];
    DB::table('realisasi_bulanan')->delete();
    $userdata = DB::table('satker_user')->where('user_id' , CRUDBooster::myId())->first();
    
    $satkerdata = DB::table('satker')->where('id' , $userdata->satker_id)->first();
    $satker_id = $satkerdata->id;
    $satker_name=$satkerdata->nama;

    $insert['satker_id']        =   $satker_id;
    $insert['nama_satker']      =   $satker_name;
    $insert['tahun']            =   CRUDBooster::myYear();
    $insert['jan']              =   0;
    $insert['feb']              =   0;
    $insert['mar']              =   0;
    $insert['apr']              =   0;
    $insert['mei']              =   0;
    $insert['jun']              =   0;
    $insert['jul']              =   0;
    $insert['aug']              =   0;
    $insert['sep']              =   0;
    $insert['okt']              =   0;
    $insert['nov']              =   0;
    $insert['des']              =   0;

    DB::table('realisasi_bulanan')->insert($insert);

    $data_transaksi = DB::table('transaksi')
    ->select( 'status_id' , DB::raw('Month(tanggal) as bulan , SUM(jumlah) as nilai') )
    ->WhereYear('tanggal' , CRUDBooster::myYear())
    ->OrderBy('id' , 'Asc')
    ->GroupBy(DB::raw('Month(tanggal)'))
    ->GroupBy('status_id')
    ->chunk(10 , function($data) use ($bulan , $satker_id){
        $percentase = 0;
        foreach ($data as $key => $value) {
            
            $data_anggaran = DB::table('rkakl')->where('level' , 9)->first();
            $pagu = $data_anggaran->jumlah;

            $percentase = $percentase + ( ($value->nilai / $pagu) * 100 );
            
            
            
            
            DB::table('realisasi_bulanan')
            ->where('satker_id' , $satker_id)
            ->where('tahun' , CRUDBooster::myYear())
            ->update([
                $bulan[$value->bulan - 1] => round($percentase, 2)
                ]);
        }
        
    });

    $data_transaksi = DB::table('transaksi')
    ->select( 'status_id' , DB::raw('Month(tanggal) as bulan , SUM(jumlah) as nilai') )
    ->WhereYear('tanggal' , CRUDBooster::myYear())
    ->OrderBy('id' , 'Asc')
    ->GroupBy(DB::raw('Month(tanggal)'))
    ->GroupBy('status_id')
    ->get();

    return $data_transaksi;

    



    return 'OK';
}

public function hitung_jenis_belanja($satker_id)
{
    DB::table('realisasi_jenis_belanja')->delete();
    $data_jenis_belanja = DB::table('jenis_belanja')->get();
    $insert = [];
    $userdata = DB::table('satker_user')->where('user_id' , CRUDBooster::myId())->first();
    
    $satkerdata = DB::table('satker')->where('id' , $userdata->satker_id)->first();
    $satker_id = $satkerdata->id;
    $satker_name=$satkerdata->nama;

    
    foreach ($data_jenis_belanja as $key => $value) {
        $insert['tahun']            = CRUDBooster::myYear();
        $insert['satker_id']        = $satker_id;
        $insert['satker_name']      = $satker_name;
        $insert['kode_belanja']    = $value->code;
        $insert['jenis_belanja']    = $value->description;
        $insert['nilai']            = 0;

        DB::table('realisasi_jenis_belanja')->insert($insert);
    }

    

    $data_transaksi = DB::table('transaksi')
                            ->select(DB::raw('SUM(jumlah) as nilai , SUBSTRING(kode_11,1,2) as kode'))
                            ->GroupBy(DB::raw('SUBSTRING(kode_11,1,2)'))
                            ->get();
    foreach ($data_transaksi as $key => $value) {
        DB::table('realisasi_jenis_belanja')->where('tahun' , CRUDBooster::myYear())
                                            ->where('satker_id' , $satker_id)
                                            ->where('kode_belanja' , $value->kode)
                                            ->update(['nilai' => $value->nilai]);
    }

    
    
}

public function hitung_rm_pnbp($satker_id)
{
    $data =  DB::table('transaksi')
    ->select('kode_9' , 'kode_8' , 'kode_6' , 'kode_7' , 'kode_11' , 'status_id' , DB::raw('SUM(jumlah) as nilai'))
    ->GroupBy('kode_9' , 'kode_8' , 'kode_6' , 'kode_7' , 'kode_11' , 'status_id')
    ->get();

    foreach ($data as $key => $value) {
        $no_mak_sys = $value->kode_9 . "." . $value->kode_8 . "." . $value->kode_6 . "." . $value->kode_7 . "." . $value->kode_11;

        if($value->status_id == 3 || $value->status_id == 4)
        {
            DB::table('rkakl')->where('no_mak_sys' , $no_mak_sys)
                            ->update(['realisasi_1' => $value->nilai]);
        }

        if($value->status_id == 5)
        {
            DB::table('rkakl')->where('no_mak_sys' , $no_mak_sys)
                            ->update(['realisasi_2' => $value->nilai]);
        }
        


    }

    
}

public static function data_realisasi()
{
    $user_data = DB::table('satker_user')->where('user_id' , CRUDBooster::myid())->first();
    $data = DB::table('realisasi')
    ->where('satker_id' , $user_data->satker_id)
    ->first();
    return $data;
}

public static function data_perbulan()
{
    $data_per_bulan = DB::table('realisasi_bulanan')->where('tahun' , CRUDBooster::myYear())
    ->where('satker_id' , 1)
    ->get();
    
    
    foreach ($data_per_bulan as $key => $value) {
        $nilai[0] = $value->jan;
        if($value->feb != 0){$nilai[1] = $value->feb;}
        if($value->mar != 0){$nilai[2] = $value->mar;}
        if($value->apr != 0){$nilai[3] = $value->apr;}
        if($value->mei != 0){$nilai[4] = $value->mei;}
        if($value->jun != 0){$nilai[5] = $value->jun;}
        if($value->jul != 0){$nilai[6] = $value->jul;}
        if($value->aug != 0){$nilai[7] = $value->aug;}
        if($value->sep != 0){$nilai[8] = $value->sep;}
        if($value->okt != 0){$nilai[9] = $value->okt;}
        if($value->nov != 0){$nilai[10] = $value->nov;}
        if($value->des != 0){$nilai[11] = $value->des;}
    }

    return $nilai;
}

public static function data_jenis_belanja()
{
    $data = DB::table('realisasi_jenis_belanja')->get(['jenis_belanja' , 'nilai']);
   
    return $data; 
}

public static function data_rm_pnbp()
{
    $data = DB::table('rkakl')->where('level' , 11)
                            ->where('realisasi_1' , '!=' , 0 )
                            ->where('realisasi_2' , '!=' , 0 )
                            ->where('realisasi_3' , '!=' , 0 )
                            ->get();
    return $data;
}

 //By the way, you can still create your own method in here... :)
}