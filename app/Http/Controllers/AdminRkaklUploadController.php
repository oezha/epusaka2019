<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use CB;
	use Illuminate\Support\Facades\App;
	use Illuminate\Support\Facades\Cache;	
	use Illuminate\Support\Facades\Hash;
	use Illuminate\Support\Facades\PDF;
	use Illuminate\Support\Facades\Route;
	use Illuminate\Support\Facades\Storage;
	use Illuminate\Support\Facades\Validator;
	use Maatwebsite\Excel\Facades\Excel;
	use Schema;
	use File;


	class AdminRkaklUploadController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "500";
			$this->orderby = "id,asc";
			$this->global_privilege = false;
			$this->button_table_action = false;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = false;
			$this->button_delete = false;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = true;
			$this->button_export = true;
			$this->table = "rkakl_upload";
			$this->sidebar_mode		   = "collapse-mini";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Kode","name"=>"kode"];
			$this->col[] = ["label"=>"Uraian","name"=>"uraian"];
			$this->col[] = ["label"=>"Vol","name"=>"vol"];
			$this->col[] = ["label"=>"Sat","name"=>"sat"];
			$this->col[] = ["label"=>"Hargasat","name"=>"hargasat"];
			$this->col[] = ["label"=>"Jumlah","name"=>"jumlah"];
			$this->col[] = ["label"=>"Kdblokir","name"=>"kdblokir"];
			$this->col[] = ["label"=>"Sdana","name"=>"sdana"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Satker Id','name'=>'satker_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'satker,id'];
			$this->form[] = ['label'=>'Kode','name'=>'kode','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Uraian','name'=>'uraian','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Vol','name'=>'vol','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Sat','name'=>'sat','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Hargasat','name'=>'hargasat','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Jumlah','name'=>'jumlah','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Kdblokir','name'=>'kdblokir','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Sdana','name'=>'sdana','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Satker Id','name'=>'satker_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'satker,id'];
			//$this->form[] = ['label'=>'Kode','name'=>'kode','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Uraian','name'=>'uraian','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Vol','name'=>'vol','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Sat','name'=>'sat','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Hargasat','name'=>'hargasat','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Jumlah','name'=>'jumlah','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Kdblokir','name'=>'kdblokir','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Sdana','name'=>'sdana','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
			$this->load_js = array();
			$this->load_js[] = "http://malsup.github.com/jquery.form.js";
			$this->load_js[] = asset('js/referensi/rkakl/import.js');
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }



	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
			//Your code here
			$satkerid = DB::table('satker_user')->where('user_id', CRUDBooster::myId())->first();
	            $query->where('satker_id' , $satkerid->satker_id);
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

		}
		
		public function getImportData()
		{	

			if(!CRUDBooster::myId()){
				$to = CRUDBooster::adminPath() . '/login' ;
				$message = 'Silahkan Login';
				$type = 'warning';
				return CRUDBooster::Redirect($to , $message , $type);
			}
			$data = [];
			$data['page_title'] = 'Import RKAKL';

			$dt_uploadke = DB::table('parameter')->where('nama' , 'UploadKe')->first();
			$data['revisike'] = $dt_uploadke->nilai + 1;


			   
			$this->cbView('backend.referensi.rkakl.import' , $data);
		}

		public function getIndex(){

			if(!CRUDBooster::MyId()){
				$to 		= CRUDBooster::adminpath() . '/login';
				$message 	= 'Please Login First!';
				$type		= 'warning';
				CRUDBooster::redirect($to , $message , $type);
			}

			$data = [] ;
			$data['page_title'] = 'Import Rkakl';
			$satkerid = DB::table('satker_user')->where('user_id', CRUDBooster::myId())->first();
			
			$cek_uploadke = DB::table('parameter')->where('nama' , 'UploadKe')->Count();
			if($cek_uploadke == 0){
				$insert = [];
				$insert['id'] = '9999';
				$insert['nama'] = 'UploadKe';
				$insert['nilai'] = 0;
				DB::table('parameter')->insert($insert);
			}

			$dt_uploadke = DB::table('parameter')->where('nama' , 'UploadKe')->first();
			$UploadKe = $dt_uploadke->nilai;

			$data_menjadi = $UploadKe;
			$data_semula = $UploadKe - 1;

			
			$data['upload_rkakl'] = DB::table('rkakl_upload')
									->where('satker_id' , $satkerid->satker_id)
									->where('UploadKe' , $UploadKe)
									->paginate(100);

			
			$data['semula'] = $data_semula;
			$data['menjadi'] = $data_menjadi;
			$this->cbView('backend.referensi.rkakl.index' , $data);
		}
		
		public function fileUploadPost(Request $request){
			$request->validate([
				'file' => 'required',
			]);

			if($request->hasFile('file')){
				$extension = File::extension($request->file->getClientOriginalName());
				if ($extension != "xlsx") {
					$to 		= '/admin/rkakl_upload/import-data';
					$message 	= "File is a '.$extension.' file.!! Please upload a valid xlsx file..!!'";
					$type		= 'danger' ;

					CRUDBooster::redirect($to , $message , $type);
				}

				ini_set('max_execution_time', 180);
				$path = $request->file->getRealPath();
				$satkeruser = DB::table('satker_user')->where('user_id' , CRUDBooster::myid())->first();
				$data = Excel::load($path, function($reader) {})->get();
					$a = $request->input('uploadke');
					$UploadKe = $a;

					if($a != 0){
						$UploadKe = $request->input('revisike');
					}
					
					DB::table('rkakl_upload')->where('UploadKe' , $UploadKe)->delete();
					if(!empty($data) && $data->count()){
						$kode9 = '';$kode4 = '';$kode8 = '';$kode6 = '';$kode7 = '';$kode11 = '';$kode0 = '';
						$x = 0;$y=0;$z=0;
						$no = 1;
						foreach ($data as $key => $value) {
							switch (strlen($value->kode)) {
								case 9:
									$kode9 = trim($value->kode);
									$nomak = $kode9;
									$nomaksys = $kode9;
									break;
								case 4:
									$kode4 = trim($value->kode);
									$nomak = $kode9 . '.' . $kode4;
									$nomaksys = $kode9 . '.' . $kode4;
									break;
								case 8:
									$kode8 = trim($value->kode);
									$nomak = $kode9 . '.' . $kode8;
									$nomaksys = $kode9 . '.' . $kode8;
									break;
								case 6:
									$kode6 = trim($value->kode);
									$nomak = $kode9 . '.' . $kode8 . '.' . $kode6;
									$nomaksys = $kode9 . '.' . $kode8 . '.' . $kode6;
									break;
								case 7:
									$kode7 = trim($value->kode);
									$nomak = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7;
									$nomaksys = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7;
									break;
								case 11:
									$kode11 = trim($value->kode);
									$x = 0;
									$z = 0 ;
									$nomak = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11;
									$nomaksys = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11;
									break;
								default:
									
									$tanda1 = trim(substr($value->uraian , 0 , 3));    
									$tanda2 = trim(substr($tanda1 , 0 , 2));
									if($tanda2 == ">")
									{
										$x = $x + 1;
										$y = 0;
										$z = 0;
										$nomak = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11;
										$nomaksys = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11 . '.' . $x;
									}
									elseif ($tanda2 == ">>") {
										$y = $y + 1;
										$z = 0;
										$nomak = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11;
										$nomaksys = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11 . '.' . $x . '.' . $y;
									}
									else
									{
										$z = $z + 1;
										$nomak = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11;
										$nomaksys = $kode9 . '.' . $kode8 . '.' . $kode6 . '.' . $kode7 . '.' . $kode11 . '.' . $x . '.' . $y . '.' . $z;
									}
				
				
				
									break;
							}
							$nomaksys = $nomaksys;
							$insert[] = [
								'no_mak_sys'=> $nomaksys,
								'level'		=> strlen($value->kode),
								'UploadKe' 	=> $UploadKe,
								'satker_id' => $satkeruser->satker_id,
								'thnang'	=> CRUDBooster::myYear(),
								'kode' 		=> $value->kode,
								'uraian'	=> $value->uraian,
								'vol'		=> $value->vol,
								'sat'		=> $value->sat,
								'hargasat'	=> $value->hargasat,
								'jumlah'	=> $value->jumlah,
								'kdblokir'	=> $value->kdblokir,
								'sdana'		=> $value->sdana
							];
						}
						if(!empty($insert)){
							$fileName = time().'.'.request()->file->getClientOriginalExtension();
							request()->file->move(public_path('files'), $fileName);
							DB::table('rkakl_upload')->insert($insert);
							
							DB::table('parameter')->where('nama' , 'UploadKe')->Update(['nilai' => $UploadKe]);

							$to 	= "/admin/rkakl_upload";
							$pesan 	= "Your Data has successfully imported" ;
							$type	= "success";

							return CRUDBooster::redirect($to , $pesan , $type);
						}

						
					}

			}
		}

		public function get_semula($UploadKe){
			$this->semula_menjadi($UploadKe , $UploadKe + 1);
			$data['upload'] = DB::table('rkakl_sebelum')->where('UploadKe', $UploadKe)->get();
			return response()->json($data);
		}

		public function get_menjadi($UploadKe){
			if($UploadKe != 0){
				$data['upload'] = DB::table('rkakl_sesudah')->where('UploadKe', $UploadKe )->get();
			}
			else
			{
				$data['upload'] = [];
				$data['keterangan'] = "Tidak Ada Data";
			}
			return response()->json($data);
		}

		public function transfer_menjadi($UploadKe){
			$data = DB::table('rkakl_upload')->where('UploadKe' , $UploadKe)->get();
			if(!empty($data) && $data->count()){
				foreach ($data as $key => $value) {
					$insert[] = [
						'no_mak_sys'=> $value->no_mak_sys,
						'level'		=> strlen($value->kode),
						'UploadKe' 	=> $UploadKe,
						'satker_id' => $satkeruser->satker_id,
						'thnang'	=> CRUDBooster::myYear(),
						'kode' 		=> $value->kode,
						'uraian'	=> $value->uraian,
						'vol'		=> $value->vol,
						'sat'		=> $value->sat,
						'hargasat'	=> $value->hargasat,
						'jumlah'	=> $value->jumlah,
						'kdblokir'	=> $value->kdblokir,
						'sdana'		=> $value->sdana,
						'Ket_Upload' => 0,
						'pilih' => 0
					];
				};

				if(!empty($insert)){
					DB::table('rkakl_sesudah')->delete();
					DB::table('rkakl_sesudah')->insert($insert);
				};				
			};
		}

		public function transfer_semula($UploadKe){
			$data = DB::table('rkakl_upload')->where('UploadKe' , $UploadKe)->get();
			if(!empty($data) && $data->count()){
				foreach ($data as $key => $value) {
					$insert[] = [
						'no_mak_sys'=> $value->no_mak_sys,
						'level'		=> strlen($value->kode),
						'UploadKe' 	=> $UploadKe,
						'satker_id' => $satkeruser->satker_id,
						'thnang'	=> CRUDBooster::myYear(),
						'kode' 		=> $value->kode,
						'uraian'	=> $value->uraian,
						'vol'		=> $value->vol,
						'sat'		=> $value->sat,
						'hargasat'	=> $value->hargasat,
						'jumlah'	=> $value->jumlah,
						'kdblokir'	=> $value->kdblokir,
						'sdana'		=> $value->sdana,
						'Ket_Upload' => 0,
						'pilih' => 0
					];
				};

				if(!empty($insert)){
					DB::table('rkakl_sebelum')->delete();
					DB::table('rkakl_sebelum')->insert($insert);
				};				
			};
		}

		public function semula_menjadi($semula , $menjadi)
		{
			$this->transfer_semula($semula);
			$this->transfer_menjadi($menjadi);

			//==============================================
			// analisa nomaksys
			$data1 = DB::table('rkakl_upload')->where('UploadKe' , $menjadi)->get(['no_mak_sys']);
			$dt_nomaksys = [];
			foreach ($data1 as $key => $value) {
				$dt_nomaksys[] = $value->no_mak_sys;
			}
			
			DB::table('rkakl_sebelum')
			->where('UploadKe' , $semula)
			->whereNotIn('no_mak_sys' , $dt_nomaksys)
			->update(['analisa_no_mak_sys' => 1]);
			//===============================================

			// periksa perubaha uraian pada no_mak yg sama dengan sebelumnya
			$data = DB::table('rkakl_sebelum')->where('analisa_no_mak_sys' , 0)
			->OrderBy('id' , 'asc')
			->chunk(50 , function($datas){
				foreach ($datas as $key => $value) {
					$dt_sesudah = DB::table('rkakl_sesudah')
					->where('no_mak_sys' , $value->no_mak_sys)->first();

					if($dt_sesudah->uraian != $value->uraian){
						DB::table('rkakl_sesudah')->where('id' , $dt_sesudah->id)->update(['analisa_uraian' => 2]);
					}
					
				}
			});
			//===============================================================

			//======================================================================================
			// analisa no_mak_sys yg bertambah dari rkakl sebelum nya
			//======================================================================================
			$a = DB::table('rkakl_sebelum')->where('analisa_no_mak_sys' , 0)->get(['no_mak_sys']);
			foreach ($a as $key => $value) {
				$b[] = $value->no_mak_sys;
			}
			DB::table('rkakl_sesudah')
			->whereNotIn('no_mak_sys' , $b)
			->update(['analisa_no_mak_sys' => 3]);
			//=======================================================================================

			return response()->json('OK');
		}

		public function pagu_minus($UploadKe){
			DB::table('rkakl_revisi')->delete();
			$data = DB::table('rkakl_upload')->where('UploadKe' , $UploadKe)->get();
			$x = 1;
			foreach ($data as $key => $value) {
				$insert [] = [
					'no_urut'			=> $x,
					'satker_id'			=> $value->satker_id,
					'thnang'			=> $value->thnang,
					'no_mak_sys'		=> $value->no_mak_sys,
					'level'				=> $value->level,
					'kode'				=> $value->kode,
					'uraian'			=> $value->uraian,
					'vol'				=> $value->vol,
					'sat'				=> $value->sat,
					'hargasat'			=> $value->hargasat,
					'jumlah'			=> $value->jumlah,
					'kdblokir'			=> $value->kdblokir,
					'sdana'				=> $value->sdana,
					'realisasi_1'		=> 0,
					'realisasi_2'		=> 0,
					'realisasi_3'		=> 0,
					'jenis_transaksi_id'=> 0,
				];
				$x++;
			}
			if(!empty($insert)){
				DB::table('rkakl_revisi')->insert($insert);
			}
		}

		public function transfer_transaksi(){
			ini_set('max_execution_time', 180);
			$this->transfer_kegiatan_to_transaksi();
			$this->transfer_perjadin_to_transaksi();
			$this->transfer_honor_to_transaksi();	
			$this->transfer_perkantoran_to_transaksi();		
		}

		public function transfer_kegiatan_to_transaksi(){
			//===== Kegiatan ====
				//hapus transaksi kegiatan
				DB::table('transaksi')->where('keterangan' , 'kegiatan')->delete();
				//============================

				$data = DB::table('m_kegiatan')->whereNotIn('status_id' , [1 , 6])->get();
				foreach ($data as $key => $value) {
					$detail = DB::table('detail_kegiatan')
					->where('jumlah_pengajuan' , '!=' , 0)
					->where('kegiatan_id' , $value->id)
					->get();

					if(!empty($detail)){
						$insert = [];
					}

					foreach ($detail as $key2 => $value2) {
						$satkeruser = DB::table('satker_user')->where('user_id' , CRUDBooster::Myid())->first();
						$insert[] = [
							'id_t'			=> $value->id,
							'keterangan'	=> 'Kegiatan',
							'tanggal'		=> $value->tgl_pengajuan,
							'satker_id'		=> $satkeruser->satker_id,
							'no_mak_sys'	=> $value2->no_mak_sys,
							'jumlah'		=> $value2->jumlah_pengajuan,
							'status_id'		=> $value->status_id
						];
					}

					if(!empty($insert)){
						DB::table('transaksi')->insert($insert);
					}

					DB::table('m_kegiatan')->where('id' , $value->id)->update(['posting' => 1]);
				}
			//==================
		}

		public function transfer_perjadin_to_transaksi(){
			//=== Perjadin ===
				//hapus transaksi kegiatan
				DB::table('transaksi')->where('keterangan' , 'perjadin')->delete();
				//============================
				$data = DB::table('perjadin')->whereNotIn('status_id' , [1 , 6])->get();
				foreach ($data as $key => $value) {
					$detail = DB::table('detail_perjadin')
					->where('jumlah_pengajuan' , '!=' , 0)
					->where('perjadin_id' , $value->id)
					->get();

					if(!empty($detail)){
						$insert = [];
					}

					foreach ($detail as $key2 => $value2) {
						$satkeruser = DB::table('satker_user')->where('user_id' , CRUDBooster::Myid())->first();
						$insert[] = [
							'id_t'			=> $value->id,
							'keterangan'	=> 'Perjadin',
							'tanggal'		=> $value->tgl_pengajuan,
							'satker_id'		=> $satkeruser->satker_id,
							'no_mak_sys'	=> $value2->no_mak_sys,
							'jumlah'		=> $value2->jumlah_pengajuan,
							'status_id'		=> $value->status_id
						];
					}

					if(!empty($insert)){
						DB::table('transaksi')->insert($insert);
					}

					DB::table('perjadin')->where('id' , $value->id)->update(['posting' => 1]);
				}
			//================
		}

		public function transfer_honor_to_transaksi(){
			//=== Perjadin ===
				//hapus transaksi kegiatan
				DB::table('transaksi')->where('keterangan' , 'honor')->delete();
				//============================
				$data = DB::table('honor')->whereNotIn('status_id' , [1 , 6])->get();
				foreach ($data as $key => $value) {
					$detail = DB::table('detail_honor')
					->where('jumlah_pengajuan' , '!=' , 0)
					->where('honor_id' , $value->id)
					->get();

					if(!empty($detail)){
						$insert = [];
					}

					foreach ($detail as $key2 => $value2) {
						$satkeruser = DB::table('satker_user')->where('user_id' , CRUDBooster::Myid())->first();
						$insert[] = [
							'id_t'			=> $value->id,
							'keterangan'	=> 'Honor',
							'tanggal'		=> $value->tgl_pengajuan,
							'satker_id'		=> $satkeruser->satker_id,
							'no_mak_sys'	=> $value2->no_mak_sys,
							'jumlah'		=> $value2->jumlah_pengajuan,
							'status_id'		=> $value->status_id
						];
					}

					if(!empty($insert)){
						DB::table('transaksi')->insert($insert);
					}

					DB::table('honor')->where('id' , $value->id)->update(['posting' => 1]);
				}
			//================
		}

		public function transfer_perkantoran_to_transaksi(){
			//=== Perjadin ===
				//hapus transaksi kegiatan
				DB::table('transaksi')->where('keterangan' , 'perkantoran')->delete();
				//============================
				$data = DB::table('honor')->whereNotIn('status_id' , [1 , 6])->get();
				foreach ($data as $key => $value) {
					$detail = DB::table('detail_honor')
					->where('jumlah_pengajuan' , '!=' , 0)
					->where('honor_id' , $value->id)
					->get();

					if(!empty($detail)){
						$insert = [];
					}

					foreach ($detail as $key2 => $value2) {
						$satkeruser = DB::table('satker_user')->where('user_id' , CRUDBooster::Myid())->first();
						$insert[] = [
							'id_t'			=> $value->id,
							'keterangan'	=> 'Perkantoran',
							'tanggal'		=> $value->tgl_pengajuan,
							'satker_id'		=> $satkeruser->satker_id,
							'no_mak_sys'	=> $value2->no_mak_sys,
							'jumlah'		=> $value2->jumlah_pengajuan,
							'status_id'		=> $value->status_id
						];
					}

					if(!empty($insert)){
						DB::table('transaksi')->insert($insert);
					}

					DB::table('perkantoran')->where('id' , $value->id)->update(['posting' => 1]);
				}
			//================
		}

		public function posting_transaksi(){
			// $data = DB::table('transaksi')
			// 		->select(DB::raw('SUM(jumlah) as jumlah') , 'no_mak_sys' , 'status_id')
			// 		->groupby('no_mak_sys' , 'status_id')
			// 		->get();

			// foreach ($data as $key => $value) {
			// 	$b = DB::table('rkakl')->where('no_mak_sys' ,  $value->no_mak_sys)->Count();

			// 	if($b != 0){
			// 		DB::table('transaksi')->where('no_mak_sys' ,  $value->no_mak_sys)->update(['posting' => 1]);
			// 		$a = DB::table('status')->where('id' , $value->status_id)->first();
			// 		switch ($a->kode_realisasi) {
			// 			case 'RL02':
			// 				DB::table('rkakl')->where('no_mak_sys' , $value->no_mak_sys)
			// 				->update(['realisasi_2' => $value->jumlah]);
			// 				break;
			// 			case 'RL03':
			// 				DB::table('rkakl')->where('no_mak_sys' , $value->no_mak_sys)
			// 				->update(['realisasi_3' => $value->jumlah]);
			// 				break;
			// 			default:
			// 				DB::table('rkakl')->where('no_mak_sys' , $value->no_mak_sys)
			// 				->update(['realisasi_1' => $value->jumlah]);
			// 				break;
			// 		}
			// 	}
			// }
			
			$data2 = DB::table('rkakl')
					->select(DB::raw('SUBSTR(no_mak_sys , 1 , 33) as nomaksys') , DB::raw('SUM(realisasi_1) as R1')  , DB::raw('SUM(realisasi_2) as R2') , DB::raw('SUM(realisasi_3) as R3'))
					->where('level' , 0)
					->groupby('nomaksys')
					->get();
			
			foreach ($data2 as $key => $value) {
				DB::table('rkakl')->where('no_mak_sys' , $value->nomaksys)
									->update([
										'realisasi_1' => $value->R1,
										'realisasi_2' => $value->R2,
										'realisasi_3' => $value->R3
										]);
			}

			$data2 = DB::table('rkakl')
					->select(DB::raw('SUBSTR(no_mak_sys , 1 , 31) as nomaksys') , DB::raw('SUM(realisasi_1) as R1')  , DB::raw('SUM(realisasi_2) as R2') , DB::raw('SUM(realisasi_3) as R3'))
					->where('level' , 0)
					->groupby('nomaksys')
					->get();
			
			foreach ($data2 as $key => $value) {
				DB::table('rkakl')->where('no_mak_sys' , $value->nomaksys)
									->update([
										'realisasi_1' => $value->R1,
										'realisasi_2' => $value->R2,
										'realisasi_3' => $value->R3
										]);
			}

			return response()->json('Posting OK');
		}


}