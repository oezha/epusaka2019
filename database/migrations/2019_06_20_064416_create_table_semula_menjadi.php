<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSemulaMenjadi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('semula_menjadi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_Sebelum');
            $table->Integer('UploadKe_Sebelum');
            $table->integer('satker_id_Sebelum');
            $table->integer('thnang_Sebelum');
            $table->string('kode_Sebelum');
            $table->string('uraian_Sebelum');
            $table->integer('vol_Sebelum');
            $table->string('sat_Sebelum');
            $table->double('hargasat_Sebelum');
            $table->double('jumlah_Sebelum');
            $table->string('kdblokir_Sebelum');
            $table->string('sdana_Sebelum');
            $table->integer('id_Sesudah');
            $table->Integer('UploadKe_Sesudah');
            $table->integer('satker_id_Sesudah');
            $table->integer('thnang_Sesudah');
            $table->string('kode_Sesudah');
            $table->string('uraian_Sesudah');
            $table->integer('vol_Sesudah');
            $table->string('sat_Sesudah');
            $table->double('hargasat_Sesudah');
            $table->double('jumlah_Sesudah');
            $table->string('kdblokir_Sesudah');
            $table->string('sdana_Sesudah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('semula_menjadi');
    }
}
